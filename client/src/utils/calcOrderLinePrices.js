import * as R from 'ramda'

const calcOrderLinePrices = (orderLineValues) => {
  const { pricelistData, order_line, tax_rate, discount_rate } = orderLineValues
  const { pricelist } = pricelistData
  let newOrderLine = []
  let newSubtotal = 0
  let newTax = 0
  let newDiscount = 0
  let newBalance = 0

  // console.log('pricelist', pricelist)

  // eslint-disable-next-line array-callback-return
  order_line.map((product) => {
    let newTotal = 0
    let newUnitPrice = 0
    let stitch_price = 0
    let min_price = 0

    // console.log('product', product)

    let { category, stitches, quantity, emb_type, unit_price } = product

    const pricelistFiltered = R.filter(R.where({ embroidery_type: R.includes(emb_type) }))(pricelist)
    // console.log('pricelistFiltered', pricelistFiltered)
    const pricelistSorted = R.sort(R.ascend(R.compose(R.prop('max_qty'))), pricelistFiltered)
    // console.log('pricelistSorted', pricelistSorted)
    const pricelistToApply = R.filter(R.where({ max_qty: R.lte(quantity) }))(pricelistSorted)[0]
    // console.log('last', emb_type, quantity, pricelistToApply)
    if (category === 'emb_logo') {
      min_price = parseFloat(pricelistToApply.min_price)
      stitch_price = (parseFloat(stitches) / 1000) * parseFloat(pricelistToApply.price_per_thus_stitches)
      newUnitPrice = min_price > stitch_price ? min_price : stitch_price
      newTotal = newUnitPrice * parseFloat(quantity)

      newSubtotal = newSubtotal + newTotal
      newOrderLine.push({ ...product, unit_price: newUnitPrice, total: newTotal })
    } else {
      newTotal = unit_price * parseFloat(quantity)
      newSubtotal = newSubtotal + newTotal
      newTotal = unit_price * parseFloat(quantity)
      newSubtotal = newSubtotal + newTotal
      newOrderLine.push({ ...product, unit_price, total: newTotal })
    }
  })
  newTax = (parseInt(tax_rate) / 100) * newSubtotal
  // console.log(`newTax: ${newTax}, tax_rate: ${tax_rate}, newSubtotal: ${newSubtotal}`)
  newDiscount = (parseInt(discount_rate) / 100) * newSubtotal
  newBalance = newSubtotal - newTax - newDiscount
  return { order_line: newOrderLine, balance: newBalance, tax: newTax, discount: newDiscount, sub_total: newSubtotal }
}

export default calcOrderLinePrices
