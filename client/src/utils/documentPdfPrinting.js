import axios from 'axios'

export const documentPdfPrinting = async (ids, accountsStatus) => {
  console.log('ids', ids)
  console.log('accountsStatus', accountsStatus)

  if (ids.length >= 1 && ids.length < 5) {
    ids.forEach(async (id) => {
      // console.log('id', id)
      try {
        const response = await axios.post(
          '/api/services/invoice/pdf',
          { id, accountsStatus },
          {
            responseType: 'arraybuffer',
            headers: {
              Accept: 'application/pdf',
            },
          }
        )

        const file = new Blob([response.data], { type: 'application/pdf' })

        const fileURL = URL.createObjectURL(file)

        const pdfWindow = window.open()
        pdfWindow.location.href = fileURL

        // const myBlob = new Blob([processedStr], { type: 'text/csv' })
        // dllink.href = window.URL.createObjectURL(myBlob)
        // dllink.setAttribute('download', 'custom_name.csv') // Added Line
        // dllink.click()

        // create <a> tag dinamically
        // var fileLink = document.createElement('a')
        // fileLink.href = fileURL

        // // it forces the name of the downloaded file
        // fileLink.download = 'pdf_name'

        // // triggers the click event
        // fileLink.click()
      } catch (err) {
        console.log(err.message)
      }
      return
    })
  }

  // console.log('You should select at least one document')
}

export const receiptPdfPrinting = async (ids) => {
  console.log('ids', ids)

  if (ids.length >= 1 && ids.length < 5) {
    ids.forEach(async (id) => {
      try {
        const response = await axios.post(
          '/api/services/receipt/pdf',
          { id },
          {
            responseType: 'arraybuffer',
            headers: {
              Accept: 'application/pdf',
            },
          }
        )

        const file = new Blob([response.data], { type: 'application/pdf' })

        const fileURL = URL.createObjectURL(file)

        const pdfWindow = window.open()
        pdfWindow.location.href = fileURL
      } catch (err) {
        console.log(err.message)
      }
      return
    })
  }

  // console.log('You should select at least one document')
}
