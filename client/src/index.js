import '@fontsource/roboto/300.css'
import '@fontsource/roboto/400.css'
import '@fontsource/roboto/500.css'
import '@fontsource/roboto/700.css'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import App from './App'
import socket, { SocketContext } from './services/socket'
import store from './store/store'
// import theme from './theme'
import { createTheme, ThemeProvider } from '@mui/material/styles'

/**
 * TODO: Create custom theme
 */
const theme = createTheme({})

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <SocketContext.Provider value={socket}>
        <ThemeProvider theme={theme}>
          <App />
        </ThemeProvider>
      </SocketContext.Provider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)
