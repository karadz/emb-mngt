import React from 'react'
import { io } from 'socket.io-client'

const socket = io('http://localhost:4000', { transports: ['websocket'], upgrade: false })

export default socket
export const SocketContext = React.createContext()
