import React, { useContext, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
/**
 * TODO:  convert form to use react-hook-form and to use local storage
 *
 */
// Nav components
import Nav from './components/Nav/Nav'
// Main page components
import Header from './components/Header/Header'
// Customers components
import Customers from './components/Main/Customers/Customers'
// const Customers = React.lazy(() => import('./components/Main/Customers/Customers'))
import CustomerCreate from './components/Main/Customers/CustomerCreate'
import CustomerEdit from './components/Main/Customers/CustomerEdit'
import CustomerView from './components/Main/Customers/CustomerView'
// Dashboard components
import Dashboard from './components/Main/Dashboard/Dashboard'
// Error components
import Error from './components/Main/Error'
// Messages components
import Messages from './components/Main/Messages/Messages'
// Production components
import Production from './components/Main/Production/Production'
// Products components
import Products from './components/Main/Products/Products'
import ProductCreate from './components/Main/Products/ProductCreate'
import ProductView from './components/Main/Products/ProductView'
import ProductEdit from './components/Main/Products/ProductEdit'
// Sales components
import Sales from './components/Main/Sales/Sales'
import SalesOrders from './components/Main/Sales/SalesOrders/SalesOrders'
import SalesOrderCreate from './components/Main/Sales/SalesOrders/SalesOrderCreate'
import SalesOrderView from './components/Main/Sales/SalesOrders/SalesOrderView'
import SalesOrderEdit from './components/Main/Sales/SalesOrders/SalesOrderEdit'
import Quotations from './components/Main/Sales/Quotations/Quotations'
import QuotationCreate from './components/Main/Sales/Quotations/QuotationCreate'
import QuotationView from './components/Main/Sales/Quotations/QuotationView'
import QuotationEdit from './components/Main/Sales/Quotations/QuotationEdit'
import Invoice from './components/Main/Sales/invoice/Invoice'
import InvoiceCreate from './components/Main/Sales/invoice/InvoiceCreate'
import InvoiceView from './components/Main/Sales/invoice/InvoiceView'

// Settings components
import Pricelist from './components/Main/Settings/Pricelists/Pricelist'
import PricelistCreate from './components/Main/Settings/Pricelists/PricelistCreate'
import PricelistEdit from './components/Main/Settings/Pricelists/PricelistEdit'
import PricelistView from './components/Main/Settings/Pricelists/PricelistView'
import CustomEmums from './components/Main/Settings/EmbControls/CustomEmums'
import Settings from './components/Main/Settings/Settings'
import Users from './components/Main/Users/Users'
import UserCreate from './components/Main/Users/UserCreate'
import UserEdit from './components/Main/Users/UserEdit'
import UserView from './components/Main/Users/UserView'

import './sass/main.scss'
import { SocketContext } from './services/socket'
import { setAuthUser } from './store/features/auth/authSlice'
import { getAllCustomers } from './store/features/customers/customersSlice'
import { getAllPricelists } from './store/features/pricelists/pricelistsSlice'
import { getAllProducts } from './store/features/products/productsSlice'
import { getAllCustomEnums } from './store/features/settings/embControls/customEnumsSlice'
import { getCurrentUiState } from './store/features/ui/uiSlice'
import { getAllUsers } from './store/features/users/usersSlice'
import { getAllQuotations } from './store/features/sales/quotationsSlice'
import { useLocalStorage } from './utils/useLocalStorage'

// const Customers = React.lazy(() => import('./components/Main/Customers/Customers'))

export let loadData = null

const App = () => {
  const dispatch = useDispatch()
  const socket = useContext(SocketContext)
  const [bigNav, setBigNav] = useState(true)

  const STORAGE_NAME = 'appData'

  const defaultValues = { bigNav: true }

  const [storage, setStorage] = useLocalStorage(STORAGE_NAME, defaultValues)

  loadData = () => {
    /**
     * Load user UI elements
     * TODO: to use local storage to save the state of the UI
     */

    dispatch(setAuthUser())

    dispatch(getCurrentUiState(localStorage.getItem('uiStates') ? JSON.parse(localStorage.getItem('uiStates')) : '/'))
    /**
     * Initialize the store state
     */
    dispatch(getAllProducts())

    dispatch(getAllUsers())

    dispatch(getAllCustomers())

    dispatch(getAllPricelists())

    dispatch(getAllCustomEnums())

    dispatch(getAllQuotations())
  }

  useEffect(() => {
    /**
     * Test new socket io connection
     */

    socket.on('connection')

    loadData()

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleClick = (e) => {
    // if (bigNav === true) {
    //   setBigNav(false)
    // } else {
    //   setBigNav(true)
    // }
    setBigNav(!bigNav)
    setStorage({ bigNav: !storage.bigNav })
  }

  return (
    <Router>
      <main className='App bg' style={storage.bigNav ? { gridTemplateColumns: '200px 1fr' } : { gridTemplateColumns: '40px 1fr' }}>
        <Header handleClick={handleClick} bigNav={storage.bigNav} />
        <Nav bigNav={storage.bigNav} />
        <Switch>
          {/* Login Routes */}
          {/* <Route exact path='/login' children={<Login />} /> */}
          {/* Dashboard Routes */}
          <Route exact path='/' children={<Dashboard />} />
          {/* Messages Routes */}
          <Route exact path='/messages' children={<Messages />} />
          {/* Customers Routes */}
          <Route exact path='/customers' children={<Customers />} />
          <Route exact path='/customer/create' children={<CustomerCreate />} />
          <Route exact path='/customer/view/:id' children={<CustomerView />} />
          <Route exact path='/customer/edit/:id' children={<CustomerEdit />} />
          {/* Sales Routes */}
          <Route exact path='/sales' children={<Sales />} />

          <Route exact path='/sales/quotation' children={<Quotations />} />
          <Route exact path='/sales/quotation/create' children={<QuotationCreate />} />
          <Route exact path='/sales/quotation/view/:id' children={<QuotationView />} />
          <Route exact path='/sales/quotation/edit/:id' children={<QuotationEdit />} />

          <Route exact path='/sales/sales_order' children={<SalesOrders />} />
          <Route exact path='/sales/sales_order/create' children={<SalesOrderCreate />} />
          <Route exact path='/sales/sales_order/view/:id' children={<SalesOrderView />} />
          <Route exact path='/sales/sales_order/Edit/:id' children={<SalesOrderEdit />} />

          <Route exact path='/sales/invoices' children={<Invoice />} />
          <Route exact path='/sales/invoices/create' children={<InvoiceCreate />} />
          <Route exact path='/sales/invoices/view/:id' children={<InvoiceView />} />

          {/* Products Routes */}
          <Route exact path='/products' children={<Products />} />
          <Route exact path='/product/create' children={<ProductCreate />} />
          <Route exact path='/product/view/:id' children={<ProductView />} />
          <Route exact path='/product/edit/:id' children={<ProductEdit />} />
          {/* Production Routes */}
          <Route exact path='/production' children={<Production />} />
          {/* Settings Routes */}
          <Route exact path='/settings' children={<Settings />} />
          <Route exact path='/settings/pricelists' children={<Pricelist />} />
          <Route exact path='/settings/pricelists/create' children={<PricelistCreate />} />
          <Route exact path='/settings/pricelists/edit/:id' children={<PricelistEdit />} />
          <Route exact path='/settings/pricelists/view/:id' children={<PricelistView />} />
          <Route exact path='/settings/controls' children={<CustomEmums />} />
          {/* Users Routes */}
          <Route exact path='/settings/users' children={<Users />} />
          <Route exact path='/settings/user/create' children={<UserCreate />} />
          <Route exact path='/settings/user/view/:id' children={<UserView />} />
          <Route exact path='/settings/user/edit/:id' children={<UserEdit />} />
          {/* Error Route */}
          <Route exact path='*' children={<Error />} />
        </Switch>
      </main>
    </Router>
  )
}

export default App
