import { combineReducers } from '@reduxjs/toolkit'
import customersReducer from '../features/customers/customersSlice'
import messagesReducer from '../features/messages/messagesSlice'
import pricelistsReducer from '../features/pricelists/pricelistsSlice'
import productsReducer from '../features/products/productsSlice'
import quotationsReducer from '../features/sales/quotationsSlice'
import customEnumsReducer from '../features/settings/embControls/customEnumsSlice'
import settingsReducer from '../features/settings/settingsSlice'
import usersReducer from '../features/users/usersSlice'

export default combineReducers({
  products: productsReducer,
  customers: customersReducer,
  users: usersReducer,
  messages: messagesReducer,
  quotations: quotationsReducer,
  settings: settingsReducer,
  pricelists: pricelistsReducer,
  customEnums: customEnumsReducer,
})
