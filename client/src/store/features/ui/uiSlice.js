import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { uiStates } from '../../../components/Main/PageHeader/pageUiData'

const dayjs = require('dayjs')
dayjs().format()

const date = dayjs().toISOString().slice(0, 16)

export const getAllUiStates = createAsyncThunk('uiStates/getAllUiStates', async () => {
  try {
    return uiStates
  } catch (err) {
    return err.message
  }
})

export const uiSlice = createSlice({
  name: 'uiStates',
  initialState: {
    uiStates: uiStates,
    currentUI: uiStates[localStorage.getItem('uiStates') ? JSON.parse(localStorage.getItem('uiStates')) : '/'],
    showListItems: true,
    loading: false,
    error: false,
    success: false,
  },

  reducers: {
    getCurrentUiState: (state, { payload }) => {
      // console.log('payload Slice', payload)
      state.currentUI = state.uiStates[payload]
    },
    // Change from List View to Card View
    changeShowListItem: (state, { payload }) => {
      state.showListItems = payload
    },
  },
  extraReducers: {
    [getAllUiStates.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getAllUiStates.fulfilled]: (state, { payload }) => {
      state.uiStates = payload
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
      state.currentUI = {}
    },
    [getAllUiStates.rejected]: (state, { payload }) => {
      // state.uiStates = {}
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
  },
})

export const { changeShowListItem, getCurrentUiState } = uiSlice.actions

export default uiSlice.reducer
