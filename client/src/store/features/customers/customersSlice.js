import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios'
import * as R from 'ramda'
import { createAlert } from '../alerts/alertsSlice'

const dayjs = require('dayjs')
dayjs().format()

const date = dayjs().toISOString().slice(0, 16)

export const getAllCustomers = createAsyncThunk('customers/getAllCustomers', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.get('/api/customers')

    dispatch(createAlert({ errors: [{ msg: 'Customers fetched', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const getCustomerById = createAsyncThunk('customers/getCustomerById', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.get(`/api/customers/${payload.id}`)

    dispatch(createAlert({ errors: [{ msg: 'Customer fetched', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const createCustomer = createAsyncThunk('customers/createCustomer', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.post('/api/customers', payload.customer)

    dispatch(createAlert({ errors: [{ msg: 'Customer created', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const editCustomer = createAsyncThunk('customers/editCustomer', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.put(`/api/customers/${payload.id}`, payload.customer)

    dispatch(createAlert({ errors: [{ msg: 'Customer updated', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const deleteCustomer = createAsyncThunk('customers/deleteCustomer', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.delete(`/api/customers/${payload.id}`)

    dispatch(createAlert({ errors: [{ msg: 'Customer updated', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const customersSlice = createSlice({
  name: 'customers',
  initialState: {
    customers: [],
    loading: false,
    lastFetch: null,
    error: false,
    success: false,
  },

  reducers: {
    // TODO: Implement upload using createAsyncThunk()
    uploadCustomers: (state, { payload }) => {
      state.customers = state.customers.filter((alert) => {
        return payload.id !== alert.id
      })
    },
  },
  extraReducers: {
    [getAllCustomers.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getAllCustomers.fulfilled]: (state, { payload }) => {
      state.customers = payload.payload
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getAllCustomers.rejected]: (state, { payload }) => {
      state.customers = []
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [getCustomerById.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getCustomerById.fulfilled]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getCustomerById.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [createCustomer.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [createCustomer.fulfilled]: (state, { payload }) => {
      state.customers.push(payload.payload)
      state.loading = false
      state.lastFetch = null
      state.error = false
      state.success = true
    },
    [createCustomer.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [editCustomer.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [editCustomer.fulfilled]: (state, { payload }) => {
      state.customers = R.map((customer) => (customer._id === payload.payload._id ? payload.payload : customer))(state.customers)
      state.loading = false
      state.lastFetch = null
      state.error = false
      state.success = true
    },
    [editCustomer.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [deleteCustomer.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [deleteCustomer.fulfilled]: (state, { payload }) => {
      // state.customers = R.reject(R.where({ tags: R.includes(payload._id) }), state.customers)
      state.customers = state.customers.filter((item) => item._id !== payload.payload._id)
      state.loading = false
      state.lastFetch = null
      state.error = false
      state.success = true
    },
    [deleteCustomer.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
  },
})

export const { uploadCustomers } = customersSlice.actions

export default customersSlice.reducer
