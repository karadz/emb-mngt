import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios'
import * as R from 'ramda'
import { createAlert } from '../alerts/alertsSlice'

const dayjs = require('dayjs')
dayjs().format()

const date = dayjs().toISOString().slice(0, 16)

// TODO: Pricelist Upload

export const getAllPricelists = createAsyncThunk('pricelists/getAllPricelists', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.get('/api/settings/pricelists')

    dispatch(createAlert({ errors: [{ msg: 'Pricelist fetched', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const getPricelistsById = createAsyncThunk(
  'pricelists/getPricelistsById',
  async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
    try {
      const response = await axios.get(`/api/settings/pricelists/${payload.id}`)

      dispatch(createAlert({ errors: [{ msg: 'Pricelist fetched', type: 'success' }] }))

      return fulfillWithValue({ payload: response.data })
    } catch (err) {
      if (!err.response) {
        throw Error(err.message)
      }
      dispatch(createAlert(err.response.data))

      return rejectWithValue({})
    }
  }
)

export const createPricelist = createAsyncThunk('pricelists/createPricelist', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.post('/api/settings/pricelists', payload.pricelist)

    dispatch(createAlert({ errors: [{ msg: 'Pricelist created', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const editPricelist = createAsyncThunk('pricelists/editPricelist', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.put(`/api/settings/pricelists/${payload.id}`, payload.pricelist)

    dispatch(createAlert({ errors: [{ msg: 'Pricelist edited', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const deletePricelist = createAsyncThunk('pricelists/deletePricelist', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.delete(`/api/settings/pricelists/${payload.id}`)

    dispatch(createAlert({ errors: [{ msg: 'Pricelist deleted', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const pricelistsSlice = createSlice({
  name: 'pricelists',
  initialState: {
    pricelists: [],
    loading: false,
    lastFetch: null,
    error: false,
    success: false,
  },
  reducers: {},
  extraReducers: {
    [createPricelist.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [createPricelist.fulfilled]: (state, { payload }) => {
      state.pricelists.push(payload.payload)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [createPricelist.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [getAllPricelists.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getAllPricelists.fulfilled]: (state, { payload }) => {
      state.pricelists = payload.payload
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getAllPricelists.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [getPricelistsById.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getPricelistsById.fulfilled]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getPricelistsById.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [editPricelist.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [editPricelist.fulfilled]: (state, { payload }) => {
      state.pricelists = R.map((pricelist) => (pricelist._id === payload.payload._id ? payload.payload : pricelist))(state.pricelists)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [editPricelist.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [deletePricelist.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [deletePricelist.fulfilled]: (state, { payload }) => {
      // state.pricelists = R.reject(R.where({ tags: R.includes(payload._id) }), state.pricelists)
      state.pricelists = state.pricelists.filter((item) => item._id !== payload.payload._id)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [deletePricelist.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
  },
})

export default pricelistsSlice.reducer
