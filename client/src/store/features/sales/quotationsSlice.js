import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios'
import * as R from 'ramda'
import { createAlert } from '../alerts/alertsSlice'

const dayjs = require('dayjs')
dayjs().format()

const date = dayjs().toISOString().slice(0, 16)

export const getAllQuotations = createAsyncThunk('quotations/getAllQuotations', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.get('/api/sales/quotations')

    dispatch(createAlert({ errors: [{ msg: 'Quotations fetched', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const getQuotationById = createAsyncThunk('quotations/getQuotationById', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.get(`/api/sales/quotations/${payload.id}`)

    dispatch(createAlert({ errors: [{ msg: 'Quotation fetched', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))
    return rejectWithValue({})
  }
})

export const createQuotation = createAsyncThunk('quotations/createQuotation', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    // console.log(payload)
    const response = await axios.post('/api/sales/quotations', payload.data)

    dispatch(createAlert({ errors: [{ msg: 'Quotation created', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    // console.log(err.response.data)

    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const editQuotation = createAsyncThunk('quotations/editQuotation', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.put(`/api/sales/quotations/${payload.id}`, payload.data)

    dispatch(createAlert({ errors: [{ msg: 'Quotation updated', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const deleteQuotation = createAsyncThunk('quotations/deleteQuotation', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.delete(`/api/sales/quotations/${payload.id}`)

    dispatch(createAlert({ errors: [{ msg: 'Quotation updated', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const quotationsSlice = createSlice({
  name: 'quotations',
  initialState: {
    quotations: [],
    loading: false,
    lastFetch: null,
    error: false,
    success: false,
  },

  reducers: {},
  extraReducers: {
    [getAllQuotations.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getAllQuotations.fulfilled]: (state, { payload }) => {
      state.quotations = payload.payload
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getAllQuotations.rejected]: (state, { payload }) => {
      state.quotations = []
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [getQuotationById.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getQuotationById.fulfilled]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getQuotationById.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [createQuotation.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [createQuotation.fulfilled]: (state, { payload }) => {
      state.quotations.push(payload.payload)
      state.loading = false
      state.lastFetch = null
      state.error = false
      state.success = true
    },
    [createQuotation.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [editQuotation.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [editQuotation.fulfilled]: (state, { payload }) => {
      state.quotations = R.map((quotation) => (quotation._id === payload.payload._id ? payload.payload : quotation))(state.quotations)
      state.loading = false
      state.lastFetch = null
      state.error = false
      state.success = true
    },
    [editQuotation.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [deleteQuotation.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [deleteQuotation.fulfilled]: (state, { payload }) => {
      // state.quotations = R.reject(R.where({ tags: R.includes(payload._id) }), state.quotations)
      state.quotations = state.quotations.filter((item) => item._id !== payload.payload._id)
      state.loading = false
      state.lastFetch = null
      state.error = false
      state.success = true
    },
    [deleteQuotation.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
  },
})

export default quotationsSlice.reducer
