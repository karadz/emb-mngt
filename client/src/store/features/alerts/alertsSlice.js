import { createSlice, nanoid } from '@reduxjs/toolkit'

export const alertsSlice = createSlice({
  name: 'alerts',
  initialState: {
    alerts: [],
  },

  reducers: {
    /**
     * payload should be of the form {errors: [{msg: "", type: ""}]}
     * msg => Any message
     * type => error, warning, info, success
     */
    createAlert: (state, { payload }) => {
      payload.errors.forEach((error) => {
        let { msg, type } = error
        if (!type) {
          type = 'error'
        }
        const id = nanoid()
        state.alerts.push({ id, msg, type })
      })
    },
    deleteAlert: (state, { payload }) => {
      state.alerts = state.alerts.filter((alert) => {
        return payload.id !== alert.id
      })
    },
  },
})

export const { createAlert, deleteAlert } = alertsSlice.actions

export default alertsSlice.reducer
