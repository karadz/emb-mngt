import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios'
import * as R from 'ramda'
import { createAlert } from '../alerts/alertsSlice'

const dayjs = require('dayjs')
dayjs().format()

const date = dayjs().toISOString().slice(0, 16)

// Get all users thunk
export const getAllUsers = createAsyncThunk('users/getAllUsers', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.get('/api/users')

    dispatch(createAlert({ errors: [{ msg: 'Users fetched', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})
// Create user thunk
export const createUser = createAsyncThunk('users/createUser', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.post('/api/users', payload.user)

    dispatch(createAlert({ errors: [{ msg: 'User created', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})
// Edit user thunk
export const editUser = createAsyncThunk('users/editUser', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.put(`/api/users/${payload.id}`, payload.user)

    dispatch(createAlert({ errors: [{ msg: 'User edited', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})
// Delete user thunk
export const deleteUser = createAsyncThunk('users/deleteUser', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.delete(`/api/users/${payload.id}`)

    dispatch(createAlert({ errors: [{ msg: 'User deleted', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const usersSlice = createSlice({
  name: 'users',
  initialState: {
    users: [],
    loading: false,
    lastFetch: null,
    error: false,
    success: false,
  },

  reducers: {},
  extraReducers: {
    [getAllUsers.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getAllUsers.fulfilled]: (state, { payload }) => {
      state.users = payload.payload
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getAllUsers.rejected]: (state, { payload }) => {
      state.users = []
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [createUser.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [createUser.fulfilled]: (state, { payload }) => {
      state.users.push(payload.payload)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [createUser.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [editUser.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [editUser.fulfilled]: (state, { payload }) => {
      state.users = R.map((user) => (user._id === payload.payload._id ? payload.payload : user))(state.users)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [editUser.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [deleteUser.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [deleteUser.fulfilled]: (state, { payload }) => {
      // user are not filtered
      // state.users = R.reject(R.where({ tags: R.includes(payload._id) }), state.users)
      // state.users = state.users.filter((item) => item._id !== payload.payload._id)
      state.loading = false
      state.lastFetch = null
      state.error = false
      state.success = true
    },
    [deleteUser.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
  },
})

// export const {} = usersSlice.actions

export default usersSlice.reducer
