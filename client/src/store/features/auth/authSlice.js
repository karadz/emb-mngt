import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios'
import setAuthToken from '../../../utils/setAuthToken'
import { createAlert } from '../alerts/alertsSlice'

const dayjs = require('dayjs')
dayjs().format()

const date = dayjs().toISOString().slice(0, 16)

export const authUser = createAsyncThunk('user/authUser', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.post('/api/auth', payload)
    // console.log(response.data.token)
    // Set axios auth token global header
    setAuthToken(response.data.token)
    // Add token to local storage
    localStorage.setItem('token', response.data.token)

    dispatch(createAlert({ errors: [{ msg: 'User authenticated', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    localStorage.removeItem('token')
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const setAuthUser = createAsyncThunk('user/setAuthUser', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const token = localStorage.getItem('token')
    setAuthToken(token)
    const response = await axios.get('/api/auth')

    dispatch(createAlert({ errors: [{ msg: 'User loaded', type: 'success' }] }))

    return fulfillWithValue({ user: response.data, token })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    localStorage.removeItem('token')
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const logout = createAsyncThunk('user/logout', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    dispatch(createAlert({ errors: [{ msg: 'User logged out', type: 'success' }] }))
    localStorage.removeItem('token')
    return fulfillWithValue({ payload })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    user: null,
    token: null,
    isLoggedIn: false,

    loading: false,
    lastFetch: null,
    error: false,
    success: false,
  },

  reducers: {},

  extraReducers: {
    [authUser.pending]: (state, { payload }) => {
      state.token = null
      state.user = null
      state.isLoggedIn = false

      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [authUser.fulfilled]: (state, { payload }) => {
      state.token = payload.payload.token
      state.user = payload.payload.user
      state.isLoggedIn = true

      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [authUser.rejected]: (state, { payload }) => {
      state.token = null
      state.user = null
      state.isLoggedIn = false

      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [setAuthUser.pending]: (state, { payload }) => {
      state.token = null
      state.user = null
      state.isLoggedIn = false

      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [setAuthUser.fulfilled]: (state, { payload }) => {
      state.token = payload.token
      state.user = payload.user
      state.isLoggedIn = true

      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [setAuthUser.rejected]: (state, { payload }) => {
      state.token = null
      state.user = null
      state.isLoggedIn = false

      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [logout.pending]: (state, { payload }) => {
      state.token = null
      state.user = null
      state.isLoggedIn = false

      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [logout.fulfilled]: (state, { payload }) => {
      state.token = null
      state.user = null
      state.isLoggedIn = false

      state.loading = false
      state.lastFetch = null
      state.error = false
      state.success = true
    },
    [logout.rejected]: (state, { payload }) => {
      state.token = null
      state.user = null
      state.isLoggedIn = false

      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
  },
})

export default authSlice.reducer
