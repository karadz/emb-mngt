import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios'
import * as R from 'ramda'
import { createAlert } from '../../alerts/alertsSlice'

const dayjs = require('dayjs')
dayjs().format()

const date = dayjs().toISOString().slice(0, 16)

export const getAllCustomEnums = createAsyncThunk('settings/getAllCustomEnums', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.get('/api/settings/custom_enum')
    dispatch(createAlert({ errors: [{ msg: 'Custom enums fetched', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const getCustomEnumsById = createAsyncThunk(
  'settings/getCustomEnumsById',
  async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
    try {
      const response = await axios.get(`/api/settings/custom_enum/${payload.id}`)
      dispatch(createAlert({ errors: [{ msg: 'Custom enum fetched', type: 'success' }] }))

      return fulfillWithValue({ payload: response.data })
    } catch (err) {
      if (!err.response) {
        throw Error(err.message)
      }
      dispatch(createAlert(err.response.data))

      return rejectWithValue({})
    }
  }
)

export const createCustomEnums = createAsyncThunk('settings/createCustomEnums', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    // console.log(payload)
    const response = await axios.post('/api/settings/custom_enum', payload)

    dispatch(createAlert({ errors: [{ msg: 'Custom enum created', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const editCustomEnums = createAsyncThunk('settings/editCustomEnums', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  // console.log('Slice: ', payload)
  try {
    const response = await axios.put(`/api/settings/custom_enum/${payload.id}`, payload.enum)

    dispatch(createAlert({ errors: [{ msg: 'Custom enum edited', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const deleteCustomEnums = createAsyncThunk('settings/deleteCustomEnums', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  // console.log(payload)
  try {
    const response = await axios.delete(`/api/settings/custom_enum/${payload.id}`)

    dispatch(createAlert({ errors: [{ msg: 'Custom enum deleted', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const customEnumsSlice = createSlice({
  name: 'enums',
  initialState: {
    enums: [],
    loading: false, // to change to false
    lastFetch: null,
    error: false,
    success: false,
  },

  reducers: {},
  extraReducers: {
    [getAllCustomEnums.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getAllCustomEnums.fulfilled]: (state, { payload }) => {
      state.enums = payload.payload
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getAllCustomEnums.rejected]: (state, { payload }) => {
      state.enums = []
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [getCustomEnumsById.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getCustomEnumsById.fulfilled]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getCustomEnumsById.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [createCustomEnums.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [createCustomEnums.fulfilled]: (state, { payload }) => {
      state.enums.push(payload.payload)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [createCustomEnums.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [editCustomEnums.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [editCustomEnums.fulfilled]: (state, { payload }) => {
      state.enums = R.map((customEnum) => (customEnum._id === payload.payload._id ? payload.payload : customEnum))(state.enums)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [editCustomEnums.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [deleteCustomEnums.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [deleteCustomEnums.fulfilled]: (state, { payload }) => {
      // console.log(R.reject(R.where({ tags: R.includes(payload.payload.id) }), [...JSON.parse(JSON.stringify(state.enums))]))
      state.enums = state.enums.filter((item) => item._id !== payload.payload._id)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [deleteCustomEnums.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
  },
})

export default customEnumsSlice.reducer
