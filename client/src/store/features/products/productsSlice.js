import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios'
import * as R from 'ramda'
import { createAlert } from '../alerts/alertsSlice'

const dayjs = require('dayjs')
dayjs().format()

const date = dayjs().toISOString().slice(0, 16)

export const getAllProducts = createAsyncThunk('products/getAllProducts', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.get('/api/products')
    dispatch(createAlert({ errors: [{ msg: 'Products fetched', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const getProductById = createAsyncThunk('products/getProductById', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.get(`/api/products/${payload.id}`)
    dispatch(createAlert({ errors: [{ msg: 'Product fetched', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const createProduct = createAsyncThunk('products/createProduct', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.post('/api/products', payload.product)

    dispatch(createAlert({ errors: [{ msg: 'Product created', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const editProduct = createAsyncThunk('products/editProduct', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.put(`/api/products/${payload.id}`, payload.product)

    dispatch(createAlert({ errors: [{ msg: 'Product edited', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const deleteProduct = createAsyncThunk('products/deleteProduct', async (payload, { rejectWithValue, fulfillWithValue, dispatch }) => {
  try {
    const response = await axios.delete(`/api/products/${payload.id}`)

    dispatch(createAlert({ errors: [{ msg: 'Product deleted', type: 'success' }] }))

    return fulfillWithValue({ payload: response.data })
  } catch (err) {
    if (!err.response) {
      throw Error(err.message)
    }
    dispatch(createAlert(err.response.data))

    return rejectWithValue({})
  }
})

export const productsSlice = createSlice({
  name: 'products',
  initialState: {
    products: [],
    loading: false, // to change to false
    lastFetch: null,
    error: false,
    success: false,
  },

  reducers: {
    // TODO: product upload using createAsyncThunk()
    uploadProducts: (state, { payload }) => {
      state.products = state.products.filter((alert) => {
        return payload.id !== alert.id
      })
    },
  },
  extraReducers: {
    [getAllProducts.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getAllProducts.fulfilled]: (state, { payload }) => {
      state.products = payload.payload
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getAllProducts.rejected]: (state, { payload }) => {
      state.products = []
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [getProductById.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [getProductById.fulfilled]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [getProductById.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [createProduct.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [createProduct.fulfilled]: (state, { payload }) => {
      state.products.push(payload.payload)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [createProduct.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [editProduct.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [editProduct.fulfilled]: (state, { payload }) => {
      state.products = R.map((product) => (product._id === payload.payload._id ? payload.payload : product))(state.products)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [editProduct.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
    [deleteProduct.pending]: (state, { payload }) => {
      state.loading = true
      state.lastFetch = null
      state.error = false
      state.success = false
    },
    [deleteProduct.fulfilled]: (state, { payload }) => {
      state.products = R.reject(R.where({ tags: R.includes(payload._id) }), state.products)
      state.loading = false
      state.lastFetch = date
      state.error = false
      state.success = true
    },
    [deleteProduct.rejected]: (state, { payload }) => {
      state.loading = false
      state.lastFetch = null
      state.error = true
      state.success = false
    },
  },
})

export const { uploadProducts } = productsSlice.actions

export default productsSlice.reducer
