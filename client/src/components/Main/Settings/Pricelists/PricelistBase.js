// import './App.css'
import { Button, Grid, LinearProgress, List, ListItem } from '@mui/material'
import { useDispatch, useSelector } from 'react-redux'
import * as R from 'ramda'
import { useForm, useFieldArray } from 'react-hook-form'
import FormTextField from '../../../form-components/FormTextField'
import FormTextFieldDropdown from '../../../form-components/FormTextFieldDropdown'
import FormSwitch from '../../../form-components/FormSwitch'
import MainPageBase from '../../MainPageBase'
import { makeStyles } from '@mui/styles'

import { useLocalStorage } from '../../../../utils/useLocalStorage'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { useHistory } from 'react-router-dom'
import { useParams } from 'react-router-dom'
import calcOrderLinePrices from '../../../../utils/calcOrderLinePrices'

// let renderCount = 0

export const pricelistSchema = yup.object().shape({
  name: yup.string().required(),
  isDefault: yup.boolean().required(),
  pricelist: yup.array().of(
    yup.object().shape({
      embroidery_type: yup.string().required('Required'),
      max_qty: yup.number().required('Required'),
      price_per_thus_stitches: yup.number().required('Required'),
      min_price: yup.number().required('Required'),
    })
  ),
})
const testSchema = yup.object().shape({
  testStitches: yup.number().required(),
  testQuantity: yup.number().required(),
  testTotal: yup.number().required(),
  test_embroidery_type: yup.string().required('Required'),
})

const useStyles = makeStyles((theme) => ({
  root: {
    // flexGrow: 1,
    // display: 'flex',
    flexDirection: 'column',
    '& .MuiFormControl-root': {
      width: '100%',
      margin: '0',
    },
    '& .MuiButton-root': {
      width: '95%',
      margin: '3px',
    },
    '& .MuiGrid-root': {
      width: '100%',
      margin: '0',
    },
    '& .MuiGrid-item': {
      padding: '0',
      margin: '0',
    },
    '& .MuiList-root': {
      padding: '6px',
      margin: '6px',
    },
    '& .MuiListItem-root': {
      padding: '6px',
      margin: '0',
    },
  },
}))

export default function PricelistBase(props) {
  const history = useHistory()
  const classes = useStyles()
  const dispatch = useDispatch()
  const { id } = useParams()

  let defaultValues = props.defaultValues
  const STORAGE_NAME = props.STORAGE_NAME
  const readOnly = props.readOnly
  let defaultPricelist = { embroidery_type: '', max_qty: '', price_per_thus_stitches: '', min_price: '' }
  let defaultEmbTypeValue = ''

  const allEnums = useSelector((state) => state.entities.customEnums.enums)

  const isLoading = useSelector((state) => state.entities.customEnums.loading)

  const filteredEnums = R.filter(R.where({ type: R.includes('embTypes') }))(allEnums)

  const defaultEnum = R.filter(R.where({ isDefault: R.equals(true) }))(filteredEnums)[0]

  // console.log('filteredEnums: ', filteredEnums)
  // console.log('defaultEnum: ', defaultEnum)

  function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
  }

  const embTypes = filteredEnums.map((user) => {
    return { value: user.value, label: capitalize(user.value) }
  })

  const changeDefault = () => {
    try {
      if (defaultEnum && defaultValues) {
        if (!defaultValues.pricelist[0].embroidery_type) {
          defaultValues = {
            pricelist: [{ embroidery_type: defaultEnum.value, max_qty: '', price_per_thus_stitches: '', min_price: '' }],
            name: '',
            isDefault: false,
          }
        }
        defaultPricelist = { embroidery_type: defaultEnum.value, max_qty: '', price_per_thus_stitches: '', min_price: '' }
        defaultEmbTypeValue = defaultEnum.value
      }
    } catch (error) {}
  }

  changeDefault()

  // console.log('defaultValues: ', defaultValues)
  // console.log('defaultPricelist: ', defaultPricelist)

  const [storage, setStorage, removeStorage] = useLocalStorage(STORAGE_NAME, defaultValues)

  const { control, handleSubmit, reset, getValues } = useForm({ defaultValues: storage, resolver: yupResolver(pricelistSchema) })

  const { fields, append, prepend, remove, swap, move, insert, replace } = useFieldArray({
    control,
    name: 'pricelist',
  })

  // const onSubmit = (data) => console.log('data', data)

  const onSubmit = (data) => {
    const payload = { id, pricelist: data }

    dispatch(props.pricelistData(payload))
    // console.log('data: ', data)

    handleReset()
  }

  const handleReset = () => {
    removeStorage()
    reset({ pricelist: [defaultPricelist], name: '', isDefault: false })
  }

  const storeLocalData = ({ storage, setStorage, getValues, data }) => {
    setStorage({ ...storage, ...getValues(), ...data })
  }

  const noChange = (data) => {
    try {
      if (data.target.type) {
        console.log('data.target.type: ', data.target.type)
        return
      }
    } catch (err) {
      // console.log(err)
    }
    // console.log('noChangeData data: ', data)
    storeLocalData({ storage, setStorage, getValues, data })
  }

  const handleRedirect = (e) => {
    e.preventDefault()
    history.push('/settings/pricelists')
  }

  // const defaultValuesTest = {}

  // console.log('defaultValuesTest: ', defaultValuesTest)
  const {
    control: controlTest,
    getValues: getValuesTest,
    setValue: setValueTest,
  } = useForm({
    mode: 'onChange',
    defaultValues: { testStitches: 0, testQuantity: 0, testUnitPrice: 0, testTotal: 0, test_embroidery_type: defaultEmbTypeValue },
    resolver: yupResolver(testSchema),
  })

  const noChangeTest = (data) => {
    let { testStitches = 0, testQuantity = 0, test_embroidery_type = defaultEmbTypeValue } = getValuesTest()
    // console.log('getValuesTest data: ', getValuesTest())

    const tax_rate = 0
    const discount_rate = 0
    try {
      if (typeof data === 'object') {
        if (data.hasOwnProperty('testStitches')) {
          // console.log('object.......................')
          // if (!data.testStitches.isInteger()) throw new Error('Whoops!')
          testStitches = parseInt(data.testStitches)
        }
        if (data.hasOwnProperty('testQuantity')) {
          // if (!data.testQuantity.isInteger()) throw new Error('Whoops!')
          testQuantity = parseInt(data.testQuantity)
        }
        if (data.hasOwnProperty('test_embroidery_type')) {
          test_embroidery_type = data.test_embroidery_type
        }
      }
    } catch (error) {
      // console.log('it throw......................')
    }
    const orderline = [
      {
        category: 'emb_logo',
        stitches: testStitches,
        quantity: testQuantity,
        unit_price: 1,
        emb_type: test_embroidery_type,
        _id: '612512bf89b726135dfed68as4',
        productID: '100-000-0006',
        name: 'Pricelist Test',
        total: 1,
        garment_positions: 'Front Left',
      },
    ]
    let orderLineValues = { pricelistData: { pricelist: getValues().pricelist }, order_line: orderline, discount_rate, tax_rate }
    // console.log('orderLineValues data: ', orderLineValues)
    // console.log('calcOrderLinePrices: ', calcOrderLinePrices(orderLineValues))

    const testResults = calcOrderLinePrices(orderLineValues)
    // console.log('test data: ', data)
    // console.log('getValuesTest data: ', getValuesTest())
    // console.log('getValues : ', getValues())
    setValueTest('testUnitPrice', testResults.order_line[0].unit_price.toFixed(2))
    setValueTest('testTotal', testResults.sub_total.toFixed(2))
  }

  // renderCount++

  // console.log('renderCount: ', renderCount)

  if (isLoading) {
    return (
      <MainPageBase>
        <LinearProgress color='primary' />
      </MainPageBase>
    )
  }

  return (
    <MainPageBase>
      <Grid container direction='row' justifyContent='center' spacing={2}>
        <Grid item xs={1}></Grid>
        <Grid item xs={6}>
          <form className={classes.root} onSubmit={handleSubmit(onSubmit)}>
            <Grid container direction='row' justifyContent='center' alignItems='center' spacing={2}>
              <Grid item xs={12}>
                <h1>Pricelist </h1>
              </Grid>

              <Grid item xs={6}>
                <FormTextField control={control} name='name' label='Pricelist Name' noChangeData={noChange} readOnly={readOnly} />
              </Grid>
              <Grid item xs={6}>
                <FormSwitch control={control} name='isDefault' label='Default Pricelist' noChangeData={noChange} readOnly={readOnly} />
              </Grid>

              <List sx={{ bgcolor: 'background.paper', width: '100%' }}>
                {/* <Grid container direction='row' spacing={2}> */}
                {fields.map((item, index) => {
                  return (
                    <ListItem key={item.id}>
                      <Grid item xs={1}>
                        <p>{index}</p>
                      </Grid>
                      <Grid item>
                        <FormTextFieldDropdown
                          control={control}
                          name={`pricelist.${index}.embroidery_type`}
                          label='Embroidery Type'
                          dropdownList={embTypes}
                          noChangeData={noChange}
                          readOnly={readOnly}
                        />
                      </Grid>
                      <Grid item>
                        <FormTextField
                          control={control}
                          name={`pricelist.${index}.max_qty`}
                          label='Max Quantity'
                          noChangeData={noChange}
                          readOnly={readOnly}
                        />
                      </Grid>
                      <Grid item>
                        <FormTextField
                          control={control}
                          name={`pricelist.${index}.price_per_thus_stitches`}
                          label='Price/1000st'
                          noChangeData={noChange}
                          readOnly={readOnly}
                        />
                      </Grid>
                      <Grid item>
                        <FormTextField
                          control={control}
                          name={`pricelist.${index}.min_price`}
                          label='Min Price'
                          noChangeData={noChange}
                          readOnly={readOnly}
                        />
                      </Grid>
                      <Grid item>
                        <Button disabled={readOnly} variant='contained' size='large' type='button' onClick={() => remove(index)}>
                          Delete
                        </Button>
                      </Grid>
                    </ListItem>
                  )
                })}
              </List>
              <Grid container direction='row' spacing={2}>
                <Grid item xs={3}>
                  <Button
                    disabled={readOnly}
                    variant='contained'
                    type='button'
                    onClick={() => {
                      append(defaultPricelist)
                    }}
                  >
                    append
                  </Button>
                </Grid>
                <Grid item xs={3}>
                  <Button disabled={readOnly} variant='contained' type='button' onClick={() => prepend(defaultPricelist)}>
                    prepend
                  </Button>
                </Grid>
                <Grid item xs={3}>
                  <Button disabled={readOnly} variant='contained' type='button' onClick={() => insert(parseInt(2, 10), defaultPricelist)}>
                    insert at
                  </Button>
                </Grid>
                <Grid item xs={3}>
                  <Button disabled={readOnly} variant='contained' type='button' onClick={() => swap(1, 2)}>
                    swap
                  </Button>
                </Grid>
                <Grid item xs={3}>
                  <Button disabled={readOnly} variant='contained' type=' Button' onClick={() => move(1, 2)}>
                    move
                  </Button>
                </Grid>
                <Grid item xs={3}>
                  <Button disabled={readOnly} variant='contained' type='button' onClick={() => replace([defaultPricelist])}>
                    replace
                  </Button>
                </Grid>
                <Grid item xs={3}>
                  <Button disabled={readOnly} variant='contained' type='button' onClick={() => remove(1)}>
                    remove at
                  </Button>
                </Grid>
                <Grid item xs={3}>
                  <Button disabled={readOnly} variant='contained' type='button' onClick={handleReset}>
                    reset
                  </Button>
                </Grid>
              </Grid>
              <Grid item xs={6}>
                <Button disabled={readOnly} sx={{ width: '97.8% !important' }} variant='contained' type='submit'>
                  Save
                </Button>
              </Grid>
              <Grid item xs={6}>
                <Button sx={{ width: '97.8% !important' }} variant='contained' onClick={handleRedirect}>
                  return
                </Button>
              </Grid>
            </Grid>
          </form>
        </Grid>
        <Grid item xs={1}></Grid>
        <Grid item xs={3}>
          <form className={classes.root}>
            <Grid container>
              <Grid item xs={12}>
                <h1>Test </h1>
              </Grid>
              <Grid item>
                <FormTextField control={controlTest} name='testStitches' label='Stitches' noChangeData={noChangeTest} />
              </Grid>
              <Grid item>
                <FormTextField control={controlTest} name='testQuantity' label='Quantity' noChangeData={noChangeTest} />
              </Grid>
              <Grid item>
                <FormTextFieldDropdown
                  control={controlTest}
                  name='test_embroidery_type'
                  label='Embroidery Type'
                  dropdownList={embTypes}
                  noChangeData={noChangeTest}
                />
              </Grid>
              <Grid item>
                <FormTextField control={controlTest} name='testUnitPrice' label='Unit Price' noChangeData={noChangeTest} readOnly={true} />
              </Grid>
              <Grid item>
                <FormTextField control={controlTest} name='testTotal' label='Total' noChangeData={noChangeTest} readOnly={true} />
              </Grid>
            </Grid>
          </form>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </MainPageBase>
  )
}
