import { useParams } from 'react-router-dom'

import { useSelector } from 'react-redux'

import { editPricelist } from '../../../../store/features/pricelists/pricelistsSlice'
import PricelistBase from './PricelistBase'

const readOnly = false
const STORAGE_NAME = 'pricelistData'

export default function PricelistEdit(props) {
  const { id } = useParams()

  const pricelist = useSelector((state) => state.entities.pricelists.pricelists.filter((pricelist) => pricelist._id === id))[0]

  console.log('pricelist: ', pricelist)

  return <PricelistBase STORAGE_NAME={STORAGE_NAME} defaultValues={pricelist} pricelistData={editPricelist} readOnly={readOnly} />
}
