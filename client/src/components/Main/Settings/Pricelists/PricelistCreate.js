// import './App.css'
import { createPricelist } from '../../../../store/features/pricelists/pricelistsSlice'
import PricelistBase from './PricelistBase'

const readOnly = false
let defaultValues = {
  pricelist: [{ embroidery_type: '', max_qty: '', price_per_thus_stitches: '', min_price: '' }],
  name: '',
  isDefault: false,
}
const STORAGE_NAME = 'pricelistData'

export default function PricelistTest(props) {
  return <PricelistBase STORAGE_NAME={STORAGE_NAME} defaultValues={defaultValues} pricelistData={createPricelist} readOnly={readOnly} />
}
