import {
  Button,
  Checkbox,
  Divider,
  Grid,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  Paper,
  Tooltip,
  Typography,
} from '@mui/material'
import makeStyles from '@mui/styles/makeStyles'
import CheckIcon from '@mui/icons-material/Check'
import CloseIcon from '@mui/icons-material/Close'
import DeleteIcon from '@mui/icons-material/Delete'
import EditIcon from '@mui/icons-material/Edit'
import ToggleButton from '@mui/material/ToggleButton'
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup'
import * as R from 'ramda'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { createCustomEnums, deleteCustomEnums, editCustomEnums } from '../../../../store/features/settings/embControls/customEnumsSlice'
import MainPageBase from '../../MainPageBase'
import Notification from '../../../Notification/Notification'

import FormTextField from '../../../form-components/FormTextField'
import FormSwitch from '../../../form-components/FormSwitch'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'

const Schema = yup.object().shape({
  type: yup.string().required(),
  isDeleted: yup.boolean().required(),
  isDefault: yup.boolean().required(),
  value: yup.string().when('type', {
    is: (value) => value === 'taxRate',
    then: yup
      .string()
      .required()
      .matches(/^(?:[1-9]\d*|0)?(?:\.\d+)?$/, 'Must be only digits'),
    otherwise: yup
      .string()
      .required()
      .matches(/^[a-zA-Z]([0-9a-zA-Z_-\s])*$/, 'Must start with a character'),
  }),
})

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    margin: theme.spacing(2, 0, 2),
  },
  text: {
    width: '40%',
    paddingTop: '2px',
  },
  switch: {
    width: '20%',
  },
  btn: {
    width: '30%',
  },
  btn2: {
    width: '90%',
  },
  paper: {
    padding: theme.spacing(1), //grid padding
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}))

function ProductsCategories() {
  const dispatch = useDispatch()
  const classes = useStyles()

  const readOnly = false

  const allEnums = useSelector((state) => state.entities.customEnums.enums)

  const [componentVariables, setComponentVariables] = useState({ CATEGORIES: 'userRoles', HEADING: 'User Roles', LABEL: 'Enter user role' }) // Leave alone

  const [enumType, setEnumType] = useState('userRoles')

  const initEnums = { type: 'userRoles', isDeleted: false, isDefault: false, value: '' }

  const { handleSubmit, control, getValues, reset, setValue } = useForm({ defaultValues: initEnums, resolver: yupResolver(Schema) })

  // console.log('componentVariables: ', componentVariables)

  const editInit = { edit: false, id: '' }

  /**
   * To Add more enums and in toggleButtonValues
   */
  const toggleButtonValues = [
    { categories: 'userRoles', heading: 'User Roles', label: 'Enter user role' },
    { categories: 'customerTypes', heading: 'Customer Types', label: 'Enter customer type' },
    { categories: 'productCategories', heading: 'Products Categories', label: 'Enter product category' },
    { categories: 'garmentPositions', heading: 'Garment Positions', label: 'Enter garment position' },
    { categories: 'embTypes', heading: 'Embroidery Types', label: 'Enter embroidery type' },
    { categories: 'taxRate', heading: 'Tax Rate', label: 'Enter tax rate' },
    { categories: 'accountsStatus', heading: 'Accounts Status', label: 'Enter accounts status' },
    { categories: 'manufacturingStatus', heading: 'Manufacturing Status', label: 'Enter Mfg status' },
  ]

  const handleTabChange = (e, newEnumType) => {
    e.preventDefault()
    console.log('newEnumType', newEnumType)
    setEnumType(newEnumType)
    setValue('type', newEnumType)
    setEdit(editInit)
    toggleButtonValues.filter((row) => {
      const { categories, heading, label } = row
      if (newEnumType === categories) {
        setComponentVariables({ CATEGORIES: categories, HEADING: heading, LABEL: label })
      }
      return ''
    })
  }

  /**
   * Product categories
   */
  // const isLoading = useSelector((state) => state.entities.customEnums.loading)

  const filteredEnums = R.filter(R.where({ type: R.includes(componentVariables.CATEGORIES) }))(allEnums)

  console.log('filteredEnums', filteredEnums)
  // console.log(componentVariables)

  // console.log('categoryText', categoryText)

  const [categoryList, setCategoryList] = useState(filteredEnums)

  const [edit, setEdit] = useState(editInit)

  const noChange = (data) => console.log(data)

  const onSubmit = (data) => {
    console.log(data)
    if (edit.edit) {
      setEdit({ edit: false, id: '' })
      reset({ ...data, isDeleted: false, isDefault: false, value: '' })
      dispatch(editCustomEnums({ id: edit.id, enum: data }))
      return ''
    }
    dispatch(createCustomEnums(data))
    reset({ ...data, isDeleted: false, isDefault: false, value: '' })
  }

  const handleEnumDelete = (e, _id) => {
    e.preventDefault()
    dispatch(deleteCustomEnums({ id: _id }))
  }

  const handleEnumEdit = (e, _id) => {
    e.preventDefault()
    const editEnum = R.find(R.propEq('_id', _id))(filteredEnums)
    // console.log('Inside  handleEnumEdit: ', editEnum)
    const pickFields = R.pick(['type', 'isDeleted', 'isDefault', 'value'], editEnum)
    // console.log('pickFields: ', pickFields)
    R.forEachObjIndexed((value, key) => {
      // console.log(`key: ${key}, value: ${value}`)
      setValue(key, value)
      return ''
    })(pickFields)
    const newEnumList = R.reject(R.where({ _id: R.includes(_id) }), filteredEnums)
    setCategoryList(newEnumList)
    setEdit({ edit: true, id: editEnum._id })
  }

  useEffect(() => {
    setCategoryList(filteredEnums)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [allEnums, enumType])

  return (
    <MainPageBase>
      <Typography variant='h4' className={classes.title}>
        Embroidery Control Values
      </Typography>
      <Notification />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Paper square>
            <ToggleButtonGroup size='small' value={getValues('type')} exclusive onChange={handleTabChange}>
              {toggleButtonValues.map((row) => {
                const { categories, heading } = row
                return (
                  <ToggleButton key={categories} value={categories}>
                    {getValues('type') === { categories } ? <CheckIcon /> : <CloseIcon />} {heading}
                  </ToggleButton>
                )
              })}
            </ToggleButtonGroup>
          </Paper>
        </Grid>
        <Grid item xs={4}></Grid>
        <Grid item xs={4}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Paper variant='outlined' className={classes.paper}>
              <Typography variant='h6' className={classes.title}>
                {componentVariables.HEADING}
              </Typography>
              <Grid container className={classes.container}>
                {false ? <FormTextField control={control} name='type' label='Name' noChangeData={noChange} readOnly={readOnly} /> : null}
                <FormTextField control={control} name='value' label='Name' noChangeData={noChange} readOnly={readOnly} />

                {false ? <FormSwitch control={control} name='isDeleted' label='isDeleted' noChangeData={noChange} readOnly={readOnly} /> : null}

                <FormSwitch control={control} name='isDefault' label='Default' noChangeData={noChange} readOnly={readOnly} />

                <Button variant='contained' className={classes.btn} type='submit'>
                  {edit.edit ? 'Update Edit' : 'Add'}
                </Button>
              </Grid>
              <Divider
                sx={{
                  padding: '2px',
                  margin: '15px 0',
                }}
              />
              <Grid item xs={12}>
                <List>
                  {categoryList.map((item) => {
                    const { _id, value, isDefault } = item
                    return (
                      <ListItem
                        button
                        key={_id}
                        sx={{
                          background: '#eee',
                        }}
                      >
                        <ListItemText primary={value} />
                        <Tooltip title='Default Value'>
                          <ListItemIcon sx={{ marginRight: '16px' }}>
                            <Checkbox disabled edge='start' checked={isDefault} />
                          </ListItemIcon>
                        </Tooltip>
                        <ListItemSecondaryAction>
                          <Tooltip title='Edit'>
                            <IconButton edge='end' aria-label='delete' onClick={(e) => handleEnumEdit(e, _id)} size='large'>
                              <EditIcon />
                            </IconButton>
                          </Tooltip>

                          <Tooltip title='Delete'>
                            <IconButton edge='end' aria-label='delete' onClick={(e) => handleEnumDelete(e, _id)} size='large'>
                              <DeleteIcon />
                            </IconButton>
                          </Tooltip>
                        </ListItemSecondaryAction>
                      </ListItem>
                    )
                  })}
                </List>
              </Grid>
            </Paper>
          </form>
        </Grid>
      </Grid>
    </MainPageBase>
  )
}

export default ProductsCategories
