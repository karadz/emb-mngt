import { createQuotation } from '../../../../store/features/sales/quotationsSlice'

import SalesOrderBase from '../SalesOrderBase'
import { useSelector } from 'react-redux'
import * as R from 'ramda'
import dayjs from 'dayjs'

let date = dayjs().toISOString().slice(0, 16)
let oneWeek = dayjs().add(7, 'day').toISOString().slice(0, 16)

const STORAGE_NAME = 'quotationCreate'
const readOnly = false
const orderType = 'Quotation'

dayjs().format()

function QuotationCreate() {
  const allEnums = useSelector((state) => state.entities.customEnums.enums)

  // console.log('allEnums ', allEnums)
  const pricelist = useSelector((state) => state.entities.pricelists.pricelists)

  let getTaxRate = R.filter(R.where({ type: R.includes('taxRate') }))(allEnums)
  getTaxRate = getTaxRate && getTaxRate.length ? parseFloat(getTaxRate[0].value) : undefined

  let defaultValues = {
    customer: null,
    pricelistData: R.filter((obj) => obj.isDefault === true, pricelist)[0],
    orderID: '',
    comments: '',
    order_date: date,
    quote_expiry_date: oneWeek,
    required_date: oneWeek,
    sub_total: 0,
    balance: 0,
    discount: 0,
    discount_rate: 0,
    tax: 0,
    tax_rate: getTaxRate,
    order_line: [],
  }
  console.log('Create Quotation')
  return (
    <SalesOrderBase
      STORAGE_NAME={STORAGE_NAME}
      defaultValues={defaultValues}
      orderSlice={createQuotation}
      readOnly={readOnly}
      orderType={orderType}
    />
  )
}

export default QuotationCreate
