import React, { useState } from 'react'
import { Checkbox, IconButton, LinearProgress } from '@mui/material'
import { useSelector } from 'react-redux'
import { DataGrid } from '@mui/x-data-grid'
import { NavLink, useLocation } from 'react-router-dom'
import { Edit as EditIcon, Visibility as VisibilityIcon } from '@mui/icons-material'
// import { deleteUser } from '../../../store/features/users/usersSlice'
import MainPageBase from '../../MainPageBase'
import DeleteIcon from '@mui/icons-material/Delete'
import * as R from 'ramda'

const debug = false

function Quotations() {
  let location = useLocation().pathname

  if (debug) console.log(location)

  const [selectionModel, setSelectionModel] = useState([])
  // console.log('selectionModel', selectionModel, selectionModel.length)

  const handleDelete = (id) => {
    // e.preventDefault()
    // dispatch(deleteUser({ id }))
    // dispatch(getAllUsers())
  }

  const rows = useSelector((state) => state.entities.quotations.quotations)

  const filteredRows = R.filter(R.where({ accountsStatus: R.includes('Quotation') }))(rows)

  // console.log(rows)

  const isLoading = useSelector((state) => state.entities.quotations.loading)

  const columns = [
    {
      field: '_id',
      headerName: 'ID',
      width: 150,
      editable: false,
      hide: true,
    },
    {
      field: 'orderID',
      headerName: 'Order ID',
      width: 250,
      editable: false,
    },
    {
      field: 'customer',
      headerName: 'Customer',
      width: 300,
      editable: false,
      valueFormatter: (params) => params.value.name,
    },

    {
      field: 'order_date',
      headerName: 'Order Date',
      width: 300,
      editable: false,
    },
    {
      field: 'balance',
      headerName: 'Balance',
      width: 120,
      editable: false,
    },
    {
      field: 'accountsStatus',
      headerName: 'Status',
      width: 120,
      editable: false,
    },
    {
      field: 'isDeleted',
      headerName: 'Is Deleted',
      width: 120,
      renderCell: (props) => {
        return <Checkbox color='primary' disabled checked={props.row.isDeleted} />
      },
    },
    {
      field: 'view',
      headerName: 'View',
      sortable: false,
      width: 120,
      disableClickEventBubbling: true,
      renderCell: (params) => {
        return (
          <div sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <IconButton size='small'>
              <NavLink exact to={`/sales/quotation/view/${params.id}`}>
                <VisibilityIcon /> View
              </NavLink>
            </IconButton>
          </div>
        )
      },
    },
    {
      field: 'edit',
      headerName: 'Edit',
      sortable: false,
      width: 120,
      disableClickEventBubbling: true,
      renderCell: (params) => {
        return (
          <IconButton size='small'>
            <NavLink exact to={`/sales/quotation/edit/${params.id}`}>
              <EditIcon fontSize='small' /> Edit
            </NavLink>
          </IconButton>
        )
      },
    },
    {
      field: 'delete',
      headerName: 'Delete',
      sortable: false,
      width: 120,
      disableClickEventBubbling: true,
      renderCell: (params) => {
        return (
          <IconButton size='small' onClick={(e) => handleDelete(params.id)}>
            <NavLink to={`#`}>
              <DeleteIcon fontSize='small' /> Cancel
            </NavLink>
          </IconButton>
        )
      },
    },
  ]

  if (isLoading) {
    return (
      <MainPageBase>
        <LinearProgress color='primary' />
      </MainPageBase>
    )
  }
  return (
    <MainPageBase selectionModel={selectionModel} accountsStatus='Quotation'>
      <DataGrid
        getRowId={(row) => row._id}
        rows={filteredRows}
        columns={columns}
        autoPageSize={true}
        onSelectionModelChange={(newSelectionModel) => {
          setSelectionModel(newSelectionModel)
        }}
        selectionModel={selectionModel}
        checkboxSelection
        disableSelectionOnClick
      />
    </MainPageBase>
  )
}

export default Quotations
