import { editQuotation } from '../../../../store/features/sales/quotationsSlice'

import SalesOrderBase from '../SalesOrderBase'
import { useSelector } from 'react-redux'
import * as R from 'ramda'
import dayjs from 'dayjs'
import { useParams } from 'react-router-dom'

const STORAGE_NAME = 'salesOrderEdit'
const readOnly = false
const orderType = 'Sales Order'

dayjs().format()

function QuotationEdit() {
  const { id } = useParams()

  let pricelistData

  const pricelist = useSelector((state) => state.entities.pricelists.pricelists)

  let quotation = useSelector((state) => state.entities.quotations.quotations.filter((quotation) => quotation._id === id))

  if (quotation && quotation.length) {
    quotation = quotation[0]
    pricelistData = R.filter((obj) => obj._id === quotation.pricelist_id, pricelist)
  }

  pricelistData = pricelistData && pricelistData.length ? pricelistData[0] : undefined

  console.log('pricelistData: ', pricelistData)
  const quotationFinal = { ...quotation, pricelistData }
  console.log('quotationFinal: ', quotationFinal)

  return (
    <SalesOrderBase STORAGE_NAME={STORAGE_NAME} defaultValues={quotationFinal} orderSlice={editQuotation} readOnly={readOnly} orderType={orderType} />
  )
}

export default QuotationEdit
