import {
  Button,
  FormControl,
  Grid,
  InputLabel,
  LinearProgress,
  NativeSelect,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@mui/material'

import { makeStyles } from '@mui/styles'

import * as R from 'ramda'
import React, { useEffect, useState, createContext } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import Notification from '../../Notification/Notification'
import MainPageBase from '../MainPageBase'
import calcOrderLinePrices from '../../../utils/calcOrderLinePrices'
import FormAutoComplete from '../../form-components/FormAutoComplete'
import FormDate from '../../form-components/FormDate'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { useLocalStorage } from '../../../utils/useLocalStorage'
import { useParams } from 'react-router-dom'

export const MainPageContext = createContext()

const Schema = yup
  .object()
  .shape({
    customer: yup
      .object()
      .shape({
        isCompany: yup.string().required(),
        rating: yup.number(),
        balance: yup.number(),
        name: yup.string().required(),
        vatOrBpNo: yup.string(),
        email: yup.string().email(),
        notes: yup.string(),
        address: yup.string(),
      })
      .required(),
    // pricelistData: pricelistSchema,
    orderID: yup.string(),
    comments: yup.string(),
    order_date: yup.date().required(),
    quote_expiry_date: yup.date().required(),
    required_date: yup.date().required(),
    sub_total: yup.number(),
    balance: yup.number(),
    order_line: yup.array().min(1, 'At least one product is required.'),
  })
  .required()

const useStyles = makeStyles((theme) => ({
  root: {
    // display: 'flex',
    // flexDirection: 'column',
    '& .MuiFormControl-root': {
      width: '100%',
      margin: '0',
    },
    '& .MuiButton-root': {
      width: '100%',
      margin: theme.spacing(1),
    },
    // '& .MuiGrid-item': {
    //   padding: '0',
    // },
  },

  table: {
    minWidth: 700,
  },

  input: {
    // /* text-align: right; */
    flexDirection: 'row-reverse',
    padding: '0',
  },

  soNumber: {
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}))

// let oldQuantity = []

function SalesOrderBase(props) {
  const history = useHistory()
  const classes = useStyles()
  const dispatch = useDispatch()

  const { id } = useParams()

  let defaultValues = props.defaultValues
  const STORAGE_NAME = props.STORAGE_NAME
  const readOnly = props.readOnly
  // const STORAGE_NAME = 'salesOrderData'
  // const readOnly = false

  const CUSTOMERS = useSelector((state) => state.entities.customers.customers)
  const isLoadingCustomer = useSelector((state) => state.entities.customers.loading)
  // if (debug) console.log(CUSTOMERS)

  const pricelist = useSelector((state) => state.entities.pricelists.pricelists)
  const isLoadingPricelist = useSelector((state) => state.entities.pricelists.loading)

  const allEnums = useSelector((state) => state.entities.customEnums.enums)
  const isLoadingAllEnums = useSelector((state) => state.entities.customEnums.loading)

  // console.log('allEnums ', allEnums)

  const [storage, setStorage, removeStorage] = useLocalStorage(STORAGE_NAME, defaultValues)

  // console.log('storage: ', storage)

  const [orderLine, setOrderLine] = useState(storage ? storage.order_line : [])

  const { handleSubmit, control, getValues, reset, setValue } = useForm({ defaultValues: storage, resolver: yupResolver(Schema) })

  // console.log('errors ', errors)

  const storeLocalData = ({ setStorage, getValues, data }) => {
    setStorage({ ...getValues(), ...data })
  }

  const noChange = (data) => {
    if (data.hasOwnProperty('order_line')) {
      addFieldsToSelectedProduct(data.order_line)
    }
    // console.log('noChangeData data: ', data)
    storeLocalData({ setStorage, getValues, data })
  }

  const onSubmit = (data) => {
    const newData = {
      ...data,
      pricelist_id: data.pricelistData._id,
      accountsStatus: props.orderType,
      manufacturingStatus: 'Receiving',
    }
    const payload = { id, data: newData }

    // console.log('Submit Entered ')
    // console.log('submit data: ', newData)
    dispatch(props.orderSlice(payload))

    // setTimeout(() => {
    // }, 4000)
    handleReset()
  }

  const handleRedirect = (e) => {
    e.preventDefault()
    history.push('sales/sales_order')
  }

  // console.log('orderLine: ', orderLine)

  const [oldQuantity, setOldQuantity] = useState([])

  /**
   *  get only values of quantity
   *  And make sure that useEffect only runs once when oldQuantity changes (prevent loop)
   */
  const currentQuantity = R.map((obj) => R.prop('quantity', obj))(orderLine)
  // console.log('oldQuantity: ', oldQuantity)
  // console.log('currentQuantity: ', currentQuantity)

  let quantityEqual = R.equals(oldQuantity, currentQuantity)

  if (!quantityEqual) {
    setOldQuantity(currentQuantity)
  }

  const [oldEmbType, setOldEmbType] = useState([])

  /**
   *  get only values of quantity
   *  And make sure that useEffect only runs once when oldQuantity changes (prevent loop)
   */
  const currentEmbType = R.map((obj) => R.prop('emb_type', obj))(orderLine)

  let embTypeEqual = R.equals(oldEmbType, currentEmbType)

  if (!embTypeEqual) {
    setOldEmbType(currentEmbType)
  }

  useEffect(() => {
    if (getValues('pricelistData')) {
      const orderLineCalculations = calcOrderLinePrices({
        pricelistData: getValues('pricelistData'),
        order_line: orderLine,
        tax_rate: getValues('tax_rate'),
        discount_rate: getValues('discount_rate'),
      })
      // console.log('orderLineCalculations', orderLineCalculations)
      setOrderLine(orderLineCalculations.order_line)
      const changeValues = ['order_line', 'sub_total', 'balance', 'discount', 'tax']
      changeValues.forEach((field) => setValue(field, orderLineCalculations[field]))
      setStorage({ ...storage, ...orderLineCalculations })
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [oldQuantity, oldEmbType])

  // const isLoadingEnums = useSelector((state) => state.entities.customEnums.loading)
  // console.log('Enums: ', allEnums)

  let products = useSelector((state) => state.entities.products.products)
  let isLoadingProducts = useSelector((state) => state.entities.products.loading)

  const garmentPositions = R.filter(R.where({ type: R.includes('garmentPositions') }))(allEnums)
  // console.log('garmentPositions: ', garmentPositions)

  const garmentPositionsDefault = R.filter((obj) => obj.isDefault === true, garmentPositions)
  // console.log('garmentPositionsDefault: ', garmentPositionsDefault[0].value)

  const garmentPositionsValues = R.map((obj) => R.prop('value', obj))(garmentPositions)

  const embTypes = R.filter(R.where({ type: R.includes('embTypes') }))(allEnums)
  // console.log('embTypes: ', embTypes)

  const embTypesDefault = R.filter((obj) => obj.isDefault === true, embTypes)
  // console.log('embTypesDefault: ', embTypesDefault[0].value)

  const embTypesValues = R.map((obj) => R.prop('value', obj))(embTypes)

  /**
   * Business Logic for order Line
   */
  const addFields = (product) => {
    return {
      ...product,
      unit_price: 0,
      total: 0,
      quantity: 1,
      emb_type: embTypesDefault[0].value,
      garment_positions: garmentPositionsDefault[0].value,
    }
  }

  function ccyFormat(num) {
    if (!num) {
      return '$ 0.00'
    }
    return `$ ${num.toFixed(2)}`
  }

  const handleQtyChange = async ({ id, e }) => {
    let { name, value } = e.target

    // console.log(`name: ${name} value: ${value}`)

    setOrderLine(
      orderLine.map((product) => {
        // if the product matches the id and is being changed
        value = value || 0
        if (product._id === id) {
          return { ...product, [name]: parseInt(value) }
        }
        // if the product is not being changed

        return product
      })
    )
  }

  const handleEnumChange = async ({ id, e, name, value }) => {
    // const { value } = e.target

    // console.log(`name: ${name} value: ${value}`)

    setOrderLine(
      orderLine.map((product) => {
        // if the product matches the id and is being changed
        if (product._id === id) {
          return { ...product, [name]: value }
        }
        // if the product is not being changed
        return product
      })
    )
  }

  /**
   * End Business Logic for order Line
   */

  const addFieldsToSelectedProduct = (value) => {
    // console.log('addFieldsToSelectedProduct: ', value)
    value = value.map((v) => (R.has('unit_price')(v) ? v : addFields(v)))
    setOrderLine(value)
  }

  const handleReset = () => {
    // console.log('Entered Start Reset')
    setOrderLine([])
    removeStorage()
    reset(defaultValues)
    // console.log('Entered End Reset')
  }

  if (isLoadingProducts || isLoadingCustomer || isLoadingAllEnums || isLoadingPricelist) {
    return (
      <MainPageBase>
        <LinearProgress color='primary' />
      </MainPageBase>
    )
  }

  console.log('accountStatus sales', storage.accountsStatus)
  console.log('storage sales', storage)

  return (
    <MainPageBase accountsStatus={storage.accountsStatus}>
      <form className={classes.root} onSubmit={handleSubmit(onSubmit)}>
        <Grid container direction='row' justifyContent='center' alignItems='center' spacing={2}>
          <Notification />
          <Grid item xs={6}>
            <Typography className={classes.soNumber} variant='h4' component='h4'>
              {props.orderType} {getValues('orderID') ? getValues('orderID') : 'New'}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <FormDate control={control} name='order_date' label='Order Date' noChangeData={noChange} readOnly={readOnly} />
          </Grid>
          <Grid item xs={6}>
            <FormAutoComplete
              control={control}
              name='customer'
              label='Customers'
              noChangeData={noChange}
              readOnly={readOnly}
              dropdownList={CUSTOMERS}
            />
          </Grid>
          <Grid item xs={6}>
            <FormDate control={control} name='quote_expiry_date' label='Expiry Date' noChangeData={noChange} readOnly={readOnly} />
          </Grid>
          <Grid item xs={6}>
            <FormAutoComplete
              control={control}
              name='pricelistData'
              label='Pricelist'
              noChangeData={noChange}
              readOnly={readOnly}
              dropdownList={pricelist}
            />
          </Grid>

          <Grid item xs={6}>
            <FormDate control={control} name='required_date' label='Required Date' noChangeData={noChange} readOnly={readOnly} />
          </Grid>
          <Grid item xs={12}>
            <FormAutoComplete
              control={control}
              name='order_line'
              label='Products'
              dropdownList={products}
              placeholder='Please select product'
              stitchesOptions={true}
              readOnly={readOnly}
              multiple={true}
              noChangeData={noChange}
            />
            <Grid item xs={12}>
              <TableContainer component={Paper} sx={{ marginTop: '16px' }}>
                <Table className={classes.table} aria-label='spanning table' size='small'>
                  <TableHead>
                    <TableRow>
                      <TableCell align='center' colSpan={7}>
                        Details
                      </TableCell>
                      <TableCell align='center'>Price</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Product ID</TableCell>
                      <TableCell>Name</TableCell>
                      <TableCell>Qty.</TableCell>
                      <TableCell>Stitches</TableCell>
                      <TableCell>Garment Position</TableCell>
                      <TableCell>Emb Type</TableCell>
                      <TableCell align='right'>Unit Price</TableCell>
                      <TableCell align='center'>Sum</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {orderLine.map((row) => {
                      const { _id, name, stitches, productID, quantity, total, unit_price } = row

                      // if (debug) console.log(quantity)
                      return (
                        <TableRow key={_id}>
                          <TableCell>{productID}</TableCell>
                          <TableCell>{name}</TableCell>
                          <TableCell sx={{ padding: '2px 16px', margin: '0px' }}>
                            <TextField
                              sx={{ padding: '0 16px!important', margin: '0!important' }}
                              size='small'
                              variant='standard'
                              disabled={readOnly}
                              inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }}
                              value={quantity}
                              className={classes.input}
                              style={{ width: 100 }}
                              name='quantity'
                              onFocus={(e) => e.target.select()}
                              onChange={(e) => handleQtyChange({ id: _id, e })}
                            />
                            {/* </FormControl> */}
                          </TableCell>
                          <TableCell sx={{ padding: '0 16px', margin: '0' }}>{stitches}</TableCell>
                          <TableCell sx={{ padding: '0 16px', margin: '0' }}>
                            <FormControl sx={{ padding: '0 16px', margin: '0!important' }} fullWidth disabled={readOnly}>
                              <InputLabel
                                sx={{ padding: '0 16px', margin: '0!important' }}
                                variant='standard'
                                htmlFor='garment_positions'
                              ></InputLabel>
                              <NativeSelect
                                size='small'
                                sx={{ padding: '0 16px', margin: '0!important' }}
                                value={orderLine.garment_positions}
                                onChange={(e) => handleEnumChange({ e, id: _id, name: 'garment_positions', value: e.target.value })}
                                inputProps={{
                                  name: 'name',
                                  id: 'garment_positions',
                                }}
                              >
                                {garmentPositionsValues.map((name) => (
                                  <option key={name} value={name}>
                                    {name}
                                  </option>
                                ))}
                              </NativeSelect>
                            </FormControl>
                          </TableCell>
                          <TableCell sx={{ padding: '0 16px', margin: '0' }}>
                            <FormControl sx={{ padding: '0 16px', margin: '0!important' }} fullWidth disabled={readOnly}>
                              <InputLabel
                                size='small'
                                sx={{ padding: '0 16px', margin: '0!important' }}
                                variant='standard'
                                htmlFor='emb_type'
                              ></InputLabel>
                              <NativeSelect
                                sx={{ padding: '0 16px', margin: '0!important' }}
                                size='small'
                                value={orderLine.emb_type}
                                onChange={(e) => handleEnumChange({ e, id: _id, name: 'emb_type', value: e.target.value })}
                                inputProps={{
                                  name: 'name',
                                  id: 'emb_type',
                                }}
                              >
                                {embTypesValues.map((name) => (
                                  <option key={name} value={name}>
                                    {name}
                                  </option>
                                ))}
                              </NativeSelect>
                            </FormControl>
                          </TableCell>
                          <TableCell sx={{ padding: '0 16px', margin: '0' }} align='right'>
                            {ccyFormat(unit_price)}
                          </TableCell>
                          <TableCell sx={{ padding: '0 16px', margin: '0' }} align='center'>
                            {ccyFormat(total)}
                          </TableCell>
                        </TableRow>
                      )
                    })}
                    <TableRow>
                      <TableCell rowSpan={3} colSpan={5}></TableCell>
                      <TableCell colSpan={2}>Subtotal</TableCell>
                      <TableCell align='center'>{ccyFormat(getValues('sub_total'))}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell>Tax</TableCell>
                      <TableCell align='right'>{`${(getValues('tax_rate') * 100).toFixed(0)} %`}</TableCell>
                      <TableCell align='center'>{ccyFormat(getValues('tax'))}</TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell colSpan={2}>Balance</TableCell>
                      <TableCell align='center'>{ccyFormat(getValues('balance'))}</TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
            <Grid container direction='row' spacing={2}>
              <Grid item xs={4}>
                <Button disabled={readOnly} variant='contained' type='submit'>
                  Save
                </Button>
              </Grid>
              <Grid item xs={4}>
                <Button disabled={readOnly} variant='contained' onClick={handleReset}>
                  Reset
                </Button>
              </Grid>
              <Grid item xs={4}>
                <Button variant='contained' onClick={handleRedirect}>
                  return
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </form>
    </MainPageBase>
  )
}

export default SalesOrderBase
