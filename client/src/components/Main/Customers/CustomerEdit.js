import { useParams } from 'react-router-dom'

import { useSelector } from 'react-redux'

import { editCustomer } from '../../../store/features/customers/customersSlice'
import CustomerBase from './CustomerBase'

const STORAGE_NAME = 'customerEdit'
const readOnly = false

function CustomerEdit(props) {
  const { id } = useParams()

  const user = useSelector((state) => state.entities.customers.customers.filter((customer) => customer._id === id))[0]

  return <CustomerBase STORAGE_NAME={STORAGE_NAME} defaultValues={user} customerData={editCustomer} readOnly={readOnly} />
}

export default CustomerEdit
