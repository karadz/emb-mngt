import { createCustomer } from '../../../store/features/customers/customersSlice'

import CustomerBase from './CustomerBase'

const defaultValues = {
  isCompany: 'Individual',
  rating: 0,
  balance: 0,
  name: '',
  vatOrBpNo: '',
  email: '',
  phone: '',
  notes: '',
  address: '',
}

const STORAGE_NAME = 'customerCreate'
const readOnly = false

function CustomerCreate(props) {
  return <CustomerBase STORAGE_NAME={STORAGE_NAME} defaultValues={defaultValues} customerData={createCustomer} readOnly={readOnly} />
}

export default CustomerCreate
