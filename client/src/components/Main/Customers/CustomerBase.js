import { Grid, Avatar, Button, LinearProgress } from '@mui/material'

import { makeStyles } from '@mui/styles'
import { deepOrange, lightBlue } from '@mui/material/colors'
import avatarImage from '../../../assets/avatar.png'
import MainPageBase from '../MainPageBase'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import Notification from '../../Notification/Notification'
import * as R from 'ramda'

import { useLocalStorage } from '../../../utils/useLocalStorage'
import { useParams } from 'react-router-dom'
import { useForm } from 'react-hook-form'

import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import FormTextFieldDropdown from '../../form-components/FormTextFieldDropdown'
import FormTextField from '../../form-components/FormTextField'

export const customerSchema = yup.object().shape({
  isCompany: yup.string().required(),
  rating: yup.number(),
  balance: yup.number(),
  name: yup.string().required(),
  vatOrBpNo: yup.string(),
  email: yup.string().email(),
  phone: yup.string().required('Phone is required'),
  notes: yup.string(),
  address: yup.string(),
})

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    // display: 'flex',
    flexDirection: 'column',
    '& .MuiFormControl-root': {
      width: '100%',
      margin: theme.spacing(1),
    },
    '& .MuiButton-root': {
      width: '100%',
      margin: theme.spacing(1),
    },
    '& .MuiGrid-root': {
      width: '100%',
    },
  },

  large: {
    width: theme.spacing(10),
    height: theme.spacing(10),
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
  orange: {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: lightBlue[500],
  },
}))

function CustomerBase(props) {
  const history = useHistory()
  const classes = useStyles()
  const dispatch = useDispatch()
  const { id } = useParams()

  let defaultValues = props.defaultValues
  const STORAGE_NAME = props.STORAGE_NAME
  const readOnly = props.readOnly

  const allEnums = useSelector((state) => state.entities.customEnums.enums)

  const isLoading = useSelector((state) => state.entities.customEnums.loading)

  const filteredEnums = R.filter(R.where({ type: R.includes('customerTypes') }))(allEnums)

  function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
  }

  const customerTypes = filteredEnums.map((user) => {
    return { value: user.value, label: capitalize(user.value) }
  })

  const [storage, setStorage, removeStorage] = useLocalStorage(STORAGE_NAME, defaultValues)

  const { handleSubmit, control, getValues, reset } = useForm({ defaultValues: storage, resolver: yupResolver(customerSchema) })

  const onSubmit = (data) => {
    const payload = { id, customer: data }

    dispatch(props.customerData(payload))
    console.log('data: ', data)

    handleReset()
  }

  const handleReset = () => {
    removeStorage()
    reset(defaultValues)
  }

  const handleRedirect = (e) => {
    e.preventDefault()
    history.push('/customers')
  }

  const storeLocalData = ({ storage, setStorage, getValues, data }) => {
    setStorage({ ...storage, ...getValues(), ...data })
  }

  const noChange = (data) => {
    try {
      if (data.target.type) {
        return
      }
    } catch (err) {}
    console.log('noChangeData data: ', data)
    storeLocalData({ storage, setStorage, getValues, data })
  }

  if (isLoading) {
    return (
      <MainPageBase>
        <LinearProgress color='primary' />
      </MainPageBase>
    )
  }

  return (
    <MainPageBase sx={{ paddingRight: '12px' }}>
      <form className={classes.root} onSubmit={handleSubmit(onSubmit)}>
        <Grid container justifyContent='flex-start' alignItems='center' spacing={2}>
          <Notification />

          <Grid item xs={3}>
            <Avatar alt='Brian Karadz' src={avatarImage} className={`${classes.orange} ${classes.large}`} />
          </Grid>
          <Grid item xs={3}>
            <FormTextFieldDropdown
              control={control}
              name='isCompany'
              label='Customer Type'
              dropdownList={customerTypes}
              noChangeData={noChange}
              readOnly={readOnly}
            />
          </Grid>
          <Grid item xs={3}>
            <FormTextField control={control} name='rating' label='Rating' noChangeData={noChange} readOnly={true} />
          </Grid>
          <Grid item xs={3}>
            <FormTextField control={control} name='balance' label='Balance' noChangeData={noChange} readOnly={true} />
          </Grid>
          <Grid item xs={6}>
            <FormTextField control={control} name='name' label='Name' noChangeData={noChange} readOnly={readOnly} />
          </Grid>
          <Grid item xs={6}>
            <FormTextField control={control} name='vatOrBpNo' label='Vat or Bp Number' noChangeData={noChange} readOnly={readOnly} />
          </Grid>
          <Grid item xs={6}>
            <FormTextField control={control} name='email' label='Email' noChangeData={noChange} readOnly={readOnly} />
          </Grid>
          <Grid item xs={6}>
            <FormTextField control={control} name='phone' label='Phone' noChangeData={noChange} readOnly={readOnly} />
          </Grid>
          <Grid item xs={6}>
            <FormTextField control={control} name='notes' label='Notes' noChangeData={noChange} multi={true} row={3} readOnly={readOnly} />
          </Grid>
          <Grid item xs={6}>
            <FormTextField control={control} name='address' label='Address' noChangeData={noChange} multi={true} row={3} readOnly={readOnly} />
          </Grid>
          <Grid item xs={4}>
            <Button variant='contained' type='submit' color='primary' disabled={readOnly}>
              Save
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button variant='contained' onClick={handleReset} disabled={readOnly}>
              Reset
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button variant='contained' onClick={handleRedirect}>
              Return
            </Button>
          </Grid>
        </Grid>
      </form>
    </MainPageBase>
  )
}

export default CustomerBase
