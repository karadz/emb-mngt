import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'

import { getCustomerById } from '../../../store/features/customers/customersSlice' // Redundant
import CustomerBase from './CustomerBase'

const STORAGE_NAME = 'customerView'
const readOnly = true // disable all except return

function ContactView(props) {
  const { id } = useParams()

  const user = useSelector((state) => state.entities.customers.customers.filter((customer) => customer._id === id))[0]

  return <CustomerBase STORAGE_NAME={STORAGE_NAME} defaultValues={user} customerData={getCustomerById} readOnly={readOnly} />
}

export default ContactView
