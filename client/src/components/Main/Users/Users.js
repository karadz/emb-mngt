import React from 'react'
import { Checkbox, IconButton, LinearProgress } from '@mui/material'
import { useSelector, useDispatch } from 'react-redux'
import { DataGrid } from '@mui/x-data-grid'
import { NavLink, useLocation } from 'react-router-dom'
import { Edit as EditIcon } from '@mui/icons-material'
import { deleteUser } from '../../../store/features/users/usersSlice'
import MainPageBase from '../MainPageBase'
import DeleteIcon from '@mui/icons-material/Delete'
import VisibilityIcon from '@mui/icons-material/Visibility'

const debug = false

function Users() {
  const dispatch = useDispatch()

  let location = useLocation().pathname

  if (debug) console.log(location)

  // useEffect(() => {
  //   dispatch(getAllUsers())
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [])

  const handleDelete = (id) => {
    // e.preventDefault()
    dispatch(deleteUser({ id }))
    // dispatch(getAllUsers())
  }

  const rows = useSelector((state) => state.entities.users.users)

  // console.log(rows)

  const isLoading = useSelector((state) => state.entities.users.loading)

  const columns = [
    {
      field: '_id',
      headerName: 'ID',
      width: 150,
      editable: false,
      hide: true,
    },
    {
      field: 'name',
      headerName: 'Name',
      width: 300,
      editable: false,
    },
    {
      field: 'role',
      headerName: 'Role',
      width: 150,
      editable: false,
    },
    {
      field: 'email',
      headerName: 'Email',
      width: 250,
      editable: false,
    },
    {
      field: 'mobile',
      headerName: 'Mobile',
      width: 250,
      editable: false,
    },
    {
      field: 'isDeleted',
      headerName: 'Is Deleted',
      width: 150,
      renderCell: (props) => {
        return <Checkbox color='primary' disabled checked={props.row.isDeleted} />
      },
    },
    {
      field: 'view',
      headerName: 'View',
      sortable: false,
      width: 80,
      disableClickEventBubbling: true,
      renderCell: (params) => {
        return (
          <IconButton size='small'>
            <NavLink exact to={`/settings/user/view/${params.id}`}>
              <VisibilityIcon fontSize='small' /> View
            </NavLink>
          </IconButton>
        )
      },
    },

    {
      field: 'edit',
      headerName: 'Edit',
      sortable: false,
      width: 80,
      disableClickEventBubbling: true,
      renderCell: (params) => {
        return (
          <IconButton size='small'>
            <NavLink exact to={`/settings/user/edit/${params.id}`}>
              <EditIcon fontSize='small' /> Edit
            </NavLink>
          </IconButton>
        )
      },
    },
    {
      field: 'delete',
      headerName: 'Delete',
      sortable: false,
      width: 120,
      disableClickEventBubbling: true,
      renderCell: (params) => {
        // console.log(params.id)
        return (
          <IconButton size='small' onClick={(e) => handleDelete(params.id)}>
            <DeleteIcon fontSize='small' /> Delete
          </IconButton>
        )
      },
    },
  ]

  if (isLoading) {
    return (
      <MainPageBase>
        <LinearProgress color='primary' />
      </MainPageBase>
    )
  }

  return (
    <MainPageBase>
      <DataGrid getRowId={(row) => row._id} rows={rows} columns={columns} autoPageSize={true} checkboxSelection disableSelectionOnClick />
    </MainPageBase>
  )
}

export default Users
