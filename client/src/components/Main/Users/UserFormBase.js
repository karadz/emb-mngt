import { Button, Grid, LinearProgress } from '@mui/material'
import makeStyles from '@mui/styles/makeStyles'
import { deepOrange, lightBlue } from '@mui/material/colors'
import MainPageBase from '../MainPageBase'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import * as R from 'ramda'
// import Notification from '../../Notification/Notification'
import { useForm } from 'react-hook-form'
import { useLocalStorage } from '../../../utils/useLocalStorage'
import { useParams } from 'react-router-dom'

import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import FormTextField from '../../form-components/FormTextField'
import FormTextFieldDropdown from '../../form-components/FormTextFieldDropdown'

const Schema = yup.object().shape({
  name: yup.string().required(),
  email: yup.string().email().required(),
  role: yup.string().required('Role is required'),
  mobile: yup.string().required('Phone is required'),
  password: yup.string().min(6).matches(/\d/, 'Password must contain at least one number').required('Password is required'),
  passwordConfirmation: yup
    .string()
    .oneOf([yup.ref('password'), null], "Passwords don't match")
    .required('Confirm Password is required'),
})

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,

    flexDirection: 'column',

    '& .MuiFormControl-root': {
      width: '100%',
      margin: theme.spacing(1),
    },
    '& .MuiButton-root': {
      width: '98%',
      margin: theme.spacing(1),
    },
  },
  buttonCentre: {
    display: 'flex',

    justifyContent: 'space-between',
  },
  large: {
    width: theme.spacing(10),
    height: theme.spacing(10),
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
  },
  orange: {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: lightBlue[500],
  },
}))

function UserFormBase(props) {
  const classes = useStyles()
  const dispatch = useDispatch()
  const history = useHistory()
  const { id } = useParams()

  const STORAGE_NAME = props.STORAGE_NAME
  const readOnly = props.readOnly

  const allEnums = useSelector((state) => state.entities.customEnums.enums)

  const isLoading = useSelector((state) => state.entities.customEnums.loading)

  const filteredEnums = R.filter(R.where({ type: R.includes('userRoles') }))(allEnums)

  function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
  }

  const userRoles = filteredEnums.map((user) => {
    return { value: user.value, label: capitalize(user.value) }
  })

  // console.log('userRoles: ', userRoles)

  let defaultValues = props.defaultValues

  // const [toReset, setToReset] = useState(false)
  const [storage, setStorage, removeStorage] = useLocalStorage(STORAGE_NAME, defaultValues)

  const { handleSubmit, control, getValues, reset } = useForm({ defaultValues: storage, resolver: yupResolver(Schema) })

  const onSubmit = (data) => {
    const payload = { id, user: data }

    dispatch(props.userData(payload))
    // console.log('data: ', data)

    handleReset()
  }

  const handleReset = () => {
    removeStorage()
    reset(defaultValues)
    // setToReset(true)
  }

  const storeLocalData = ({ storage, setStorage, getValues, data }) => {
    setStorage({ ...storage, ...getValues(), ...data })
  }

  const noChange = (data) => {
    try {
      if (data.target.type) {
        return
      }
    } catch (err) {
      // console.log(err)
    }
    // console.log('noChangeData data: ', data)
    storeLocalData({ storage, setStorage, getValues, data })
  }

  const handleRedirect = (e) => {
    e.preventDefault()
    history.push('/settings/users')
  }

  if (isLoading) {
    return (
      <MainPageBase>
        <LinearProgress color='primary' />
      </MainPageBase>
    )
  }

  return (
    <MainPageBase>
      <form className={classes.root} onSubmit={handleSubmit(onSubmit)}>
        <Grid container>
          {/* <Notification /> */}
          <FormTextField control={control} name='name' label='Name' noChangeData={noChange} readOnly={readOnly} />

          <FormTextField control={control} name='email' label='Email' noChangeData={noChange} readOnly={readOnly} />

          <FormTextFieldDropdown control={control} name='role' label='Role' dropdownList={userRoles} noChangeData={noChange} readOnly={readOnly} />

          <FormTextField control={control} name='mobile' label='Mobile' noChangeData={noChange} readOnly={readOnly} />

          <FormTextField control={control} name='password' label='Password' noChangeData={noChange} readOnly={readOnly} />

          <FormTextField control={control} name='passwordConfirmation' label='Confirmation Password' noChangeData={noChange} readOnly={readOnly} />

          <Grid className={classes.buttonCentre} container align='center'>
            <Grid item xs={4}>
              <Button disabled={readOnly} variant='contained' type='submit' color='primary'>
                Save
              </Button>
            </Grid>
            <Grid item xs={4}>
              <Button disabled={readOnly} variant='contained' type='reset' color='primary' onClick={handleReset}>
                Reset
              </Button>
            </Grid>
            <Grid item xs={4}>
              <Button variant='contained' onClick={handleRedirect}>
                Return
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </form>
    </MainPageBase>
  )
}

export default UserFormBase
