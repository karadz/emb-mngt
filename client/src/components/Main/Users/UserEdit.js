import UserFormBase from './UserFormBase'
import { useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { editUser } from '../../../store/features/users/usersSlice'

const STORAGE_NAME = 'editUserData'

function UserEdit() {
  const { id } = useParams()
  // console.log(id)

  const { name, email, role, mobile } = useSelector((state) => state.entities.users.users.filter((user) => user._id === id))[0]
  const selectedUser = { name, email, role, mobile: mobile.join(), password: '', passwordConfirmation: '' }
  // console.log(selectedUser)

  return <UserFormBase STORAGE_NAME={STORAGE_NAME} defaultValues={selectedUser} userData={editUser} />
}

export default UserEdit
