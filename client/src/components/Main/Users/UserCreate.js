import UserFormBase from './UserFormBase'
import { createUser } from '../../../store/features/users/usersSlice'

const STORAGE_NAME = 'createUserData'
const defaultValues = {
  name: '',
  email: '',
  role: 'user',
  mobile: '',
  password: '',
  passwordConfirmation: '',
}

function UserCreate() {
  return <UserFormBase STORAGE_NAME={STORAGE_NAME} defaultValues={defaultValues} userData={createUser} />
}

export default UserCreate
