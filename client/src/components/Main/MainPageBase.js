import PageHeader from './PageHeader/PageHeader'

import { Grid, Paper } from '@mui/material'
import { makeStyles } from '@mui/styles'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,

    height: '100%',
    flexDirection: 'column',
    backgroundColor: 'hsl(198, 17%, 33%)',
  },
  paper: {
    padding: theme.spacing(2),
    // margin: theme.spacing(0, 1.8),
    margin: theme.spacing(0.5),

    backgroundColor: '#eeeeee',
    width: '100%',
    elevation: 3,
  },
  grid: {
    height: 'calc(100% - 54px)',
  },
}))

function MainPageBase(props) {
  const classes = useStyles()

  // console.log('selectionModel mainBase', props.selectionModel)

  return (
    <Grid container className={`${classes.root} main`}>
      <PageHeader selectionModel={props.selectionModel} accountsStatus={props.accountsStatus} />
      <Grid container alignItems='stretch' className={classes.grid}>
        <Paper className={classes.paper}>{props.children}</Paper>
      </Grid>
    </Grid>
  )
}

export default MainPageBase
