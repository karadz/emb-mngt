import Notification from '../../Notification/Notification'

import Button from '@mui/material/Button'
import { createAlert } from '../../../store/features/alerts/alertsSlice'

import { useDispatch } from 'react-redux'
import MainPageBase from '../MainPageBase'

import { CNY, USD } from '@dinero.js/currencies'
import { dinero, add, toUnit, down, multiply, toSnapshot, greaterThanOrEqual, convert } from 'dinero.js'
import axios from 'axios'

const Dashboard = (props) => {
  const dispatch = useDispatch()

  const clickHandler = () => {
    dispatch(createAlert({ errors: [{ msg: 'This is an error alert', type: 'error' }] }))
    // Create base currency object
    const d1 = dinero({ amount: 35, currency: CNY })
    const d2 = dinero({ amount: 200, currency: CNY })
    // Compare money with units
    const hasFreeShipping = greaterThanOrEqual(d2, d1)
    console.log('hasFreeShipping: ', hasFreeShipping)
    // Format money amount  0.35
    console.log('Format money amount: ', toUnit(d1, { digits: 2, round: down }))
    // Calculate the amount of money  35 + 200 = 235
    console.log('Calculate the amount of money: ', add(d1, d2).toJSON())
    // Calculation  5.5%  VAT rate
    const tax = multiply(d2, 0.055)
    const total = add(d2, tax)
    console.log('Calculation  5.5%  VAT rate: ', toSnapshot(total))
    // Currency conversion
    const rates = { USD: { amount: 1549, scale: 4 } }
    console.log('Currency conversion: ', convert(d2, USD, rates).toJSON())
    const rawData = toSnapshot(d1)
    console.log('rowData: ', rawData)
  }

  const heavyImage = new Image()

  heavyImage.src = './logo.png'

  const pdfClickHandler = async () => {
    console.time('Page')

    try {
      const response = await axios.post(
        '/api/services/invoice/pdf',
        { id: '6166f042bab13ccb80d79f82' },
        {
          responseType: 'arraybuffer',
          headers: {
            Accept: 'application/pdf',
          },
        }
      )

      const file = new Blob([response.data], { type: 'application/pdf' })

      const fileURL = URL.createObjectURL(file)

      const pdfWindow = window.open()
      pdfWindow.location.href = fileURL

      console.timeEnd('Page')

      // const myBlob = new Blob([processedStr], { type: 'text/csv' })
      // dllink.href = window.URL.createObjectURL(myBlob)
      // dllink.setAttribute('download', 'custom_name.csv') // Added Line
      // dllink.click()

      // create <a> tag dinamically
      // var fileLink = document.createElement('a')
      // fileLink.href = fileURL

      // // it forces the name of the downloaded file
      // fileLink.download = 'pdf_name'

      // // triggers the click event
      // fileLink.click()
    } catch (err) {
      console.log(err.messaes)
    }
  }

  const receiptPdfClickHandler = async () => {
    console.time('Page')

    try {
      const response = await axios.post(
        '/api/services/receipt/pdf',
        { id: '6166f042bab13ccb80d79f82' },
        {
          responseType: 'arraybuffer',
          headers: {
            Accept: 'application/pdf',
          },
        }
      )

      const file = new Blob([response.data], { type: 'application/pdf' })

      const fileURL = URL.createObjectURL(file)

      const pdfWindow = window.open()
      pdfWindow.location.href = fileURL

      console.timeEnd('Page')
    } catch (err) {
      console.log(err.messaes)
    }
  }

  return (
    <MainPageBase>
      <h1>Dashboard</h1>
      <Notification />

      <Button variant='contained' color='secondary' onClick={clickHandler} sx={{ margin: '12px' }}>
        Notification
      </Button>
      <Button variant='contained' onClick={pdfClickHandler} sx={{ margin: '12px' }}>
        Invoice PDF
      </Button>
      <Button variant='contained' onClick={receiptPdfClickHandler} sx={{ margin: '12px' }}>
        Receipt PDF
      </Button>
    </MainPageBase>
  )
}

export default Dashboard
