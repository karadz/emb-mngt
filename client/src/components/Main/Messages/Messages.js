import React, { useEffect, useState } from 'react'
import FormDate from '../../form-components/FormDate'
import FormAutoComplete from '../../form-components/FormAutoComplete'
import FormTextFieldDropdown from '../../form-components/FormTextFieldDropdown'
import FormRadio from '../../form-components/FormRadio'
import FormSwitch from '../../form-components/FormSwitch'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { useLocalStorage } from '../../../utils/useLocalStorage'
import { useForm } from 'react-hook-form'
import { Button, Grid, LinearProgress, ButtonGroup } from '@mui/material'
import * as R from 'ramda'
import MainPageBase from '../MainPageBase'
import { useSelector } from 'react-redux'
import ArrowButton from './ArrowButton'

const Schema = yup.object().shape({
  date: yup.date().required(),
})

const LIST = [
  {
    value: 'USD',
    label: '$',
  },
  {
    value: 'EUR',
    label: '€',
  },
  {
    value: 'BTC',
    label: '฿',
  },
  {
    value: 'JPY',
    label: '¥',
  },
]

function Messages() {
  let products = useSelector((state) => state.entities.products.products)
  let isLoadingProducts = useSelector((state) => state.entities.products.loading)

  const multiple = false

  let defaultValues = {
    date: '2017-05-24T10:30',
    products: multiple ? [] : null,
    list: 'USD',
    radio: 'USD',
    switch: true,
  }

  const [toReset, setToReset] = useState(false)
  const [storage, setStorage] = useLocalStorage('tempData', defaultValues)

  // console.log('storage: ', storage)
  // console.log('defaultValues: ', defaultValues)

  if (!R.equals(defaultValues, storage)) {
    defaultValues = storage
  }

  useEffect(() => {
    if (toReset) {
      setToReset(false)
      reset()
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [toReset])

  const { handleSubmit, control, getValues, reset, register } = useForm({ defaultValues, resolver: yupResolver(Schema) })

  const onSubmit = (data) => {
    // e.preventDefault()
    // console.log('data: ', data)
    localStorage.removeItem('tempData')
    reset({
      date: '2017-05-24T10:30',
      products: multiple ? [] : null,
      list: 'USD',
      radio: 'USD',
      switch: true,
    })
    setToReset(true)
  }

  const onChange = (data) => {
    console.log('Data: ', data)
    // console.log('getValues(): ', getValues())
    try {
      // console.log('newData: ', data.target.type)
      if (data.target.type === 'checkbox' || data.target.type === 'radio') {
        return setStorage({ ...storage, ...getValues() })
      }
    } catch (err) {
      // console.log(err)
    }

    setStorage({ ...storage, ...getValues(), ...data })
  }

  if (isLoadingProducts) {
    return (
      <MainPageBase>
        <LinearProgress color='primary' />
      </MainPageBase>
    )
  }

  return (
    <MainPageBase>
      <Grid container spacing={2} direction='column' justifyContent='center' alignItems='stretch'>
        <form onSubmit={handleSubmit(onSubmit)} onChange={onChange}>
          <Grid sx={{ margin: '24px' }}></Grid>
          <Grid sx={{ margin: '24px' }}>
            <FormDate control={control} name='date' label='Date' noChangeData={onChange} />
          </Grid>
          <Grid sx={{ margin: '24px' }}>
            <FormAutoComplete
              control={control}
              name='products'
              label='Products'
              dropdownList={products}
              placeholder='Please select product'
              stitchesOptions={true}
              multiple={multiple}
              noChangeData={onChange}
            />
          </Grid>

          <Grid sx={{ margin: '24px' }}>
            <FormTextFieldDropdown control={control} name='list' label='List' dropdownList={LIST} noChangeData={onChange} />
          </Grid>

          <Grid sx={{ margin: '24px' }}>
            <FormSwitch control={control} name='switch' label='Switch' register={register} noChangeData={onChange} />
          </Grid>
          <Grid sx={{ margin: '24px' }}>
            <FormRadio control={control} name='radio' label='Radio' List={LIST} />
          </Grid>

          <Grid sx={{ margin: '24px' }}>
            <ArrowButton label='Test button' />
          </Grid>

          <Grid sx={{ margin: '24px' }}>
            <ButtonGroup aria-label='outlined primary button group'>
              <ArrowButton label='Test One' disabled={true} />
              <ArrowButton label='Test Two' />
              <ArrowButton label='Test Three' />
              {/* <Button>One</Button>
              <Button>Two</Button>
              <Button>Three</Button> */}
            </ButtonGroup>
          </Grid>
          <Grid sx={{ margin: '24px' }}>
            <Button variant='contained' type='submit' color='primary' disabled>
              Save
            </Button>
          </Grid>
        </form>
      </Grid>
    </MainPageBase>
  )
}

export default Messages
