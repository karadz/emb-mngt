import { Button } from '@mui/material'

function ArrowButton(props) {
  return (
    <Button
      variant='contained'
      disabled={props.disabled}
      sx={{
        // border: '0px',
        display: 'inline-block',
        position: 'relative',
        zIndex: '1',
        border: '0px solid #000000',
        backgroundColor: 'transparent',
        color: '#ffffff',
        fontSize: '13px',
        padding: '4px 20px',
        marginLeft: '5px',
        boxShadow: 'none',

        '&:hover': {
          background: 'transparent',
          boxShadow: 'none',
        },
        '&:disabled': {
          background: 'transparent',
          boxShadow: 'none',
        },

        '&:before': {
          content: "''",
          color: 'transparent',
          position: 'absolute',
          width: '100%',
          height: '50%',
          left: '0px',
          top: '0px',
          zIndex: '-1',
          // borderRadius: ' 3px',
          backgroundColor: '#1976d2',
          transform: 'skew(40deg)',
          boxShadow: '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
        },

        '&:hover:before': {
          backgroundColor: '#1565c0',
        },
        '&:disabled:before': {
          backgroundColor: '#d1d1d1',
        },

        '&:after': {
          content: "''",
          color: 'transparent',
          position: 'absolute',
          width: '100%',
          height: '50%',
          left: '0px',
          bottom: '0px',
          zIndex: '-1',
          // borderRadius: ' 3px',
          backgroundColor: '#1976d2',
          transform: 'skew(-40deg)',
          boxShadow: '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
        },
        '&:hover:after': {
          backgroundColor: '#1565c0',
        },
        '&:disabled:after': {
          backgroundColor: '#d1d1d1',
        },
      }}
    >
      {props.label}
    </Button>
  )
}

export default ArrowButton
