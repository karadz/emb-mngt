import { createProduct } from '../../../store/features/products/productsSlice'

import ProductBase from './ProductBase'

const STORAGE_NAME = 'productCreate'
const readOnly = false
const defaultValues = {
  name: '',
  productID: '',
  title: '',
  description: '',
  image: '',
  rating: 0,
  category: 'emb_logo', // TODO: Add categories from customEnums
  unit_price: 0,
  quantity: 0,
  stitches: 0,
}

function ProductCreate(props) {
  return <ProductBase STORAGE_NAME={STORAGE_NAME} defaultValues={defaultValues} productData={createProduct} readOnly={readOnly} />
}

export default ProductCreate
