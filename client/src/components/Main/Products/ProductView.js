import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'

import { getProductById } from '../../../store/features/products/productsSlice'

import ProductBase from './ProductBase'
const STORAGE_NAME = 'productCreate'
const readOnly = true

function ProductView() {
  const { id } = useParams()

  const product = useSelector((state) => state.entities.products.products.filter((product) => product._id === id))[0]

  return <ProductBase STORAGE_NAME={STORAGE_NAME} defaultValues={product} productData={getProductById} readOnly={readOnly} />
}

export default ProductView
