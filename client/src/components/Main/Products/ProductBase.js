import { Avatar, Button, Grid, LinearProgress } from '@mui/material'
import { deepOrange, lightBlue } from '@mui/material/colors'
import { makeStyles } from '@mui/styles'
import MainPageBase from '../MainPageBase'
// import Notification from '../../Notification/Notification'
import avatarImage from '../../../assets/avatar.png'
import { useDispatch, useSelector } from 'react-redux'
import * as R from 'ramda'

import { useLocalStorage } from '../../../utils/useLocalStorage'
import { useParams } from 'react-router-dom'
import { useForm } from 'react-hook-form'

import { useHistory } from 'react-router-dom'

import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import FormTextField from '../../form-components/FormTextField'
import FormTextFieldDropdown from '../../form-components/FormTextFieldDropdown'

const Schema = yup.object().shape({
  name: yup.string().required(),
  productID: yup.string(),
  title: yup.string(),
  description: yup.string(),
  image: yup.string(),
  rating: yup.number(),
  category: yup.string(),
  unit_price: yup.number('Unit price must be a number.'),
  quantity: yup.number('Quantity must be a number.'),
  stitches: yup.number('Stitches must be a number.'),
})

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    // display: 'flex',
    flexDirection: 'column',
    '& .MuiFormControl-root': {
      width: '100%',
      margin: theme.spacing(1),
    },
    '& .MuiButton-root': {
      width: '100%',
      margin: theme.spacing(1),
    },
    '& .MuiGrid-root': {
      width: '100%',
    },
  },

  large: {
    width: theme.spacing(15),
    height: theme.spacing(15),
    // padding: theme.spacing(7),
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
    marginBottom: theme.spacing(0.5),
    // borderColor: deepOrange[500],
  },
  orange: {
    color: theme.palette.getContrastText(deepOrange[500]),
    backgroundColor: lightBlue[500],
  },
}))

function ProductBase(props) {
  const history = useHistory()
  const classes = useStyles()
  const dispatch = useDispatch()
  const { id } = useParams()

  let defaultValues = props.defaultValues
  const STORAGE_NAME = props.STORAGE_NAME
  const readOnly = props.readOnly

  const allEnums = useSelector((state) => state.entities.customEnums.enums)

  const isLoading = useSelector((state) => state.entities.customEnums.loading)

  const filteredEnums = R.filter(R.where({ type: R.includes('productCategories') }))(allEnums)

  function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
  }

  const productCategories = filteredEnums.map((user) => {
    return { value: user.value, label: capitalize(user.value) }
  })

  // const [toReset, setToReset] = useState(false)
  const [storage, setStorage, removeStorage] = useLocalStorage(STORAGE_NAME, defaultValues)

  // if (!R.equals(defaultValues, storage)) {
  //   defaultValues = storage
  // }

  const { handleSubmit, control, getValues, reset } = useForm({ defaultValues: storage, resolver: yupResolver(Schema) })

  const onSubmit = (data) => {
    // e.preventDefault()

    const payload = { id, product: data }

    dispatch(props.productData(payload))
    console.log('data: ', data)

    handleReset()
  }

  // useEffect(() => {
  //   if (toReset) {
  //     setToReset(false)
  //     reset()
  //   }

  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [toReset])

  const handleReset = () => {
    // e.preventDefault()
    removeStorage()
    reset(defaultValues)
    // setToReset(true)
  }

  const handleRedirect = (e) => {
    e.preventDefault()
    history.push('/products')
  }

  const storeLocalData = ({ storage, setStorage, getValues, data }) => {
    setStorage({ ...storage, ...getValues(), ...data })
  }

  const noChange = (data) => {
    try {
      if (data.target.type) {
        return
      }
    } catch (err) {
      // console.log(err)
    }
    console.log('noChangeData data: ', data)
    storeLocalData({ storage, setStorage, getValues, data })
  }

  if (isLoading) {
    return (
      <MainPageBase>
        <LinearProgress color='primary' />
      </MainPageBase>
    )
  }

  return (
    <MainPageBase>
      <form className={classes.root} onSubmit={handleSubmit(onSubmit)}>
        <Grid container justifyContent='flex-start' alignItems='center' spacing={2}>
          {/* <Notification /> */}

          <Grid container justifyContent='flex-start' alignItems='center' item xs={6} spacing={2}>
            <Grid item xs={2}>
              <Avatar variant='rounded' alt='Brian Karadz' src={avatarImage} className={`${classes.orange} ${classes.large}`} />
            </Grid>

            <Grid item xs={5}>
              <FormTextField control={control} name='productID' label='Product ID' noChangeData={noChange} readOnly={true} />
            </Grid>
            <Grid item xs={5}>
              <FormTextField control={control} name='rating' label='Rating' noChangeData={noChange} readOnly={true} />
            </Grid>
          </Grid>

          <Grid item xs={6}>
            <FormTextField
              control={control}
              name='description'
              multi={true}
              row={3}
              label='Description'
              noChangeData={noChange}
              readOnly={readOnly}
            />
          </Grid>

          <Grid item xs={6}>
            <FormTextField control={control} name='name' label='Name' noChangeData={noChange} readOnly={readOnly} />
          </Grid>
          <Grid item xs={6}>
            <FormTextFieldDropdown
              control={control}
              name='category'
              label='Category'
              dropdownList={productCategories}
              noChangeData={noChange}
              readOnly={readOnly}
            />
          </Grid>
          <Grid item xs={6}>
            <FormTextField control={control} name='title' label='Title' noChangeData={noChange} readOnly={readOnly} />
          </Grid>
          <Grid item xs={6}>
            <FormTextField control={control} name='stitches' label='Stitches' noChangeData={noChange} readOnly={readOnly} />
          </Grid>

          <Grid item xs={6}>
            <FormTextField control={control} name='unit_price' label='Unit Price' noChangeData={noChange} readOnly={readOnly} />
          </Grid>

          <Grid item xs={6}>
            <FormTextField control={control} name='quantity' label='Quantity' noChangeData={noChange} readOnly={readOnly} />
          </Grid>

          <Grid item xs={4}>
            <Button type='submit' variant='contained' disabled={readOnly}>
              Save
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button variant='contained' onClick={handleReset} disabled={readOnly}>
              Reset
            </Button>
          </Grid>
          <Grid item xs={4}>
            <Button variant='contained' onClick={handleRedirect}>
              Return
            </Button>
          </Grid>
        </Grid>
      </form>
    </MainPageBase>
  )
}

export default ProductBase
