import { useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'

import { editProduct } from '../../../store/features/products/productsSlice'

import ProductBase from './ProductBase'

const STORAGE_NAME = 'productEdit'
const readOnly = false

function ProductEdit() {
  const { id } = useParams()

  const product = useSelector((state) => state.entities.products.products.filter((product) => product._id === id))[0]

  return <ProductBase STORAGE_NAME={STORAGE_NAME} defaultValues={product} productData={editProduct} readOnly={readOnly} />
}

export default ProductEdit
