import { NavLink, useLocation, useParams, useHistory } from 'react-router-dom'
import { emphasize } from '@mui/material/styles'
import withStyles from '@mui/styles/withStyles'
import makeStyles from '@mui/styles/makeStyles'
import { Paper, Chip, Button, ButtonGroup, Box, Breadcrumbs, Grid } from '@mui/material'
import { Home } from '@mui/icons-material'
import { useDispatch, useSelector } from 'react-redux'
import { getCurrentUiState } from '../../../store/features/ui/uiSlice'
import { useEffect } from 'react'
import { documentPdfPrinting, receiptPdfPrinting } from '../../../utils/documentPdfPrinting'

import FormArrowButton from '../../form-components/FormArrowButton'

// import { useLocalStorage } from '../../../utils/useLocalStorage'
// import { uiStates } from './pageUiData'

const debug = false

const StyledBreadcrumb = withStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.grey[100],
    color: theme.palette.grey[800],
    fontWeight: theme.typography.fontWeightRegular,
    '&:hover, &:focus': {
      backgroundColor: theme.palette.grey[300],
    },
    '&:active': {
      boxShadow: theme.shadows[1],
      backgroundColor: emphasize(theme.palette.grey[300], 0.12),
    },
  },
}))(Chip)

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paper: {
    background: theme.palette.grey[300],
    padding: theme.spacing(0.2),
    height: theme.spacing(6.5),
    display: 'flex',
    width: '100%',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  breadcrumbs: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
  },
}))

const PageHeader = ({ selectionModel = [], accountsStatus = 'Quotation' }) => {
  const history = useHistory()
  const classes = useStyles()
  const { id } = useParams()
  const dispatch = useDispatch()
  let location = useLocation().pathname.replace(`/${id}`, '')

  let accountsList = [{ label: 'Quotation' }, { label: 'Sales Order' }, { label: 'Invoice' }, { label: 'Canceled' }]

  console.log('accountsStatus2', accountsStatus)
  // console.log('test', test)
  if (accountsStatus) {
    let disabled = true
    accountsList = accountsList.map((row) => {
      if (row.label.toLowerCase() === accountsStatus.toLowerCase()) {
        disabled = false
        return { ...row, active: true }
      }
      return { ...row, disabled: disabled }
    })
  }
  if (location.includes('edit')) {
    accountsList = accountsList.map((row) =>
      row.label.toLowerCase() === accountsStatus.toLowerCase() ? { ...row, active: true } : { ...row, disabled: true }
    )
  }

  console.log('accountsList', accountsList)

  if (debug) console.log('props', selectionModel)

  const STORAGE_NAME = 'uiStates'

  // if (debug) console.log(location.replace(`/${id}`, ''))
  if (debug) console.log(location)
  // console.log('location', location)

  useEffect(() => {
    dispatch(getCurrentUiState(location))
    // console.log('useEffect location', location)
    localStorage.setItem(STORAGE_NAME, JSON.stringify(location))
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location])

  // console.log('allStates', allStates)

  const crumbLinks = useSelector((state) => state.ui.ui.currentUI.breadcrumbs)
  const buttons = useSelector((state) => state.ui.ui.currentUI.buttons)
  const showButton = useSelector((state) => state.ui.ui.currentUI.buttonShow)
  const showStatus = useSelector((state) => state.ui.ui.currentUI.showStatus)
  // const showList = false
  // const showStatus = true

  if (debug) console.log(showButton)
  // if (debug) console.log(showList)
  // if (debug) console.log(crumbLinks)

  const handleClick = ({ link, name }) => {
    if (debug) console.log(link)
    if (debug) console.log(name)
    // console.log('name', name)
    if (name === 'Create') {
      history.push(link)
    }
    if (selectionModel.length === 0 && !id) {
      if (debug) console.log('Error id must exist')
      return
    }
    if (name === 'Print Quotation' && selectionModel.length >= 1) {
      documentPdfPrinting(selectionModel, accountsStatus)
      return
    }
    if (name === 'Print Sales Order' && selectionModel.length >= 1) {
      documentPdfPrinting(selectionModel, accountsStatus)
      return
    }
    if (name === 'Print Invoice' && selectionModel.length >= 1) {
      documentPdfPrinting(selectionModel, accountsStatus)
      return
    }
    if (name === 'Print Receipt' && selectionModel.length >= 1) {
      receiptPdfPrinting(selectionModel, accountsStatus)
      return
    }
    if (name === 'Print Quotation' && id) {
      documentPdfPrinting([id], accountsStatus)
      return
    }
    if (name === 'Print Sales Order' && id) {
      documentPdfPrinting([id], accountsStatus)
      return
    }
    if (name === 'Print Invoice' && id) {
      documentPdfPrinting([id], accountsStatus)
      return
    }
    if (name === 'Print Receipt' && id) {
      receiptPdfPrinting([id], accountsStatus)
      return
    }
  }

  return (
    <Paper className={classes.paper}>
      <Grid container sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
        <Grid item xs={4}>
          <div className={classes.breadcrumbs}>
            <Breadcrumbs aria-label='breadcrumb'>
              {crumbLinks.map((crumb, index) => {
                const { title, url } = crumb
                return (
                  <NavLink key={index} exact to={url}>
                    {!index ? (
                      <StyledBreadcrumb label={title} onClick={handleClick} icon={<Home fontSize='small' />} />
                    ) : (
                      <StyledBreadcrumb label={title} onClick={handleClick} />
                    )}
                  </NavLink>
                )
              })}
            </Breadcrumbs>
          </div>
        </Grid>
        <Grid item xs={4}>
          <div>
            {showButton ? (
              <Box sx={{ flexDirection: 'row', display: 'flex', justifyContent: 'center' }}>
                <ButtonGroup variant='contained' size='small' aria-label='small contained primary button group'>
                  {buttons.map((btn, index) => {
                    const { name, link } = btn
                    let disabled = false
                    if (
                      (selectionModel.length === 0 && name.toLowerCase() === 'print quotation') ||
                      (selectionModel.length === 0 && name.toLowerCase() === 'print receipt') ||
                      (selectionModel.length === 0 && name.toLowerCase() === 'print invoice') ||
                      (selectionModel.length === 0 && name.toLowerCase() === 'print sales order') ||
                      (selectionModel.length > 5 && name.toLowerCase() === 'print quotation') ||
                      (selectionModel.length > 5 && name.toLowerCase() === 'print receipt') ||
                      (selectionModel.length > 5 && name.toLowerCase() === 'print invoice') ||
                      (selectionModel.length > 5 && name.toLowerCase() === 'print sales order')
                    ) {
                      disabled = true
                    }
                    if (id && location.includes('view')) disabled = false
                    return (
                      <Button key={index} onClick={(e) => handleClick({ e, link, name })} disabled={disabled}>
                        {name}
                      </Button>
                    )
                  })}
                </ButtonGroup>
              </Box>
            ) : (
              ``
            )}
          </div>
        </Grid>
        <Grid item xs={4}>
          <div>
            {/* {showList ? (
              <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end' }}>
                <div>
                  <IconButton onClick={() => dispatch(changeShowListItem(true))} size='large'>
                    <TableViewIcon />
                  </IconButton>
                </div>
                <div>
                  <IconButton onClick={() => dispatch(changeShowListItem(false))} size='large' sx={{ margin: '0 40px 0 0' }}>
                    <ArtTrackIcon />
                  </IconButton>
                </div>
              </Box>
            ) : (
              ``
            )} */}
            {showStatus ? (
              <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-start' }}>
                <FormArrowButton List={accountsList} accountsStatus={accountsStatus} />
              </Box>
            ) : (
              ``
            )}
          </div>
        </Grid>
      </Grid>
    </Paper>
  )
}

export default PageHeader
