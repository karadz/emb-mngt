import React, { useState } from 'react'
import { AccountCircle, Mail as MailIcon, MenuOpen as MenuOpenIcon, Notifications as NotificationsIcon, Menu as MenuIcon } from '@mui/icons-material'
import { IconButton, Menu, MenuItem, Badge, Typography } from '@mui/material'
import Login from '../Auth/Login'
import { useSelector, useDispatch } from 'react-redux'
import { logout } from '../../store/features/auth/authSlice'
// import { useLocation, useParams } from 'react-router-dom'

// const debug = false

const Header = ({ handleClick, bigNav }) => {
  const dispatch = useDispatch()
  // const { id } = useParams()
  // let location = useLocation().pathname
  // location = location.replace(`/${id}`, '')
  const [anchorEl, setAnchorEl] = useState(null)
  // if (debug) console.log(location)

  let title = useSelector((state) => state.ui.ui.currentUI.headerTitle)

  let name = ''

  let showLogin = !useSelector((state) => state.auth.isLoggedIn)
  const user = useSelector((state) => state.auth.user)

  const [timer, setTimer] = useState(false)

  setTimeout(() => {
    setTimer(true)
  }, 700)

  // console.log(`Timer: ${timer}, showLogin: ${showLogin}`)

  if (user) {
    name = user.name
  }

  const handleClicked = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }
  const handleLogout = () => {
    setAnchorEl(null)
    dispatch(logout())
  }

  return (
    <div className='head'>
      {showLogin && timer && <Login />}

      {/* {showLogin && <Redirect to='/login' />} */}
      <div className='left-head'>
        <button className='nav-toggle' onClick={(e) => handleClick(e)}>
          {bigNav ? <MenuIcon /> : <MenuOpenIcon />}
        </button>
        <Typography variant='h6' noWrap>
          {title}
        </Typography>
      </div>
      <div className='right-head'>
        <div>
          <IconButton size='large'>
            <Badge badgeContent={2} color='secondary'>
              <MailIcon />
            </Badge>
          </IconButton>
        </div>

        <div>
          <IconButton size='large'>
            <Badge badgeContent={4} color='secondary'>
              <NotificationsIcon />
            </Badge>
          </IconButton>
        </div>
        <div>
          <IconButton aria-controls='simple-menu' aria-haspopup='true' onClick={handleClicked} size='large'>
            {/* Todo Added avatar */}
            <AccountCircle />
          </IconButton>
          <Menu id='simple-menu' anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
            <MenuItem>{name}</MenuItem>
            <MenuItem onClick={handleClose}>My account</MenuItem>
            <MenuItem onClick={handleLogout}>Logout</MenuItem>
          </Menu>
        </div>
      </div>
    </div>
  )
}

export default Header
