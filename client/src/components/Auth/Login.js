import React, { useState } from 'react'
import { Paper, Button, TextField, Dialog, InputAdornment, IconButton } from '@mui/material'
import makeStyles from '@mui/styles/makeStyles'
import Notification from '../Notification/Notification'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import { useDispatch } from 'react-redux'
import { authUser } from '../../store/features/auth/authSlice'
import { Visibility, VisibilityOff } from '@mui/icons-material'

const useStyles = makeStyles((theme) => ({
  backDrop: {
    backdropFilter: 'blur(5px)',
    backgroundColor: 'rgba(0,0,30,0.4)',
  },
  paper: {
    // backgroundColor: theme.palette.grey[300],
    width: '500px',
    padding: theme.spacing(3),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    // margin: theme.spacing(1),
    elevation: 0,
  },
}))

function Login({ setIsLoggedIn }) {
  const classes = useStyles()
  const dispatch = useDispatch()

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [values, setValues] = useState()

  const handleClickShowPassword = () => {
    setValues(!values)
  }

  const handleMouseDownPassword = (event) => {
    event.preventDefault()
  }

  // console.log(`loadData: ${loadData}`)

  const handleSubmit = (e) => {
    e.preventDefault()

    const credentials = {
      email,
      password,
    }
    // Auth User and Load User
    dispatch(authUser(credentials))
  }

  return (
    <div>
      <Dialog
        aria-labelledby='customized-dialog-title'
        open={true}
        // disableBackdropClick
        BackdropProps={{
          classes: {
            root: classes.backDrop,
          },
        }}
      >
        <DialogTitle>Sign In</DialogTitle>
        <form onSubmit={(e) => handleSubmit(e)}>
          <DialogContent dividers>
            <Paper className={classes.paper}>
              <Notification />
              <TextField
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
                variant='filled'
                autoFocus
                margin='dense'
                id='email'
                label='Email'
                type='email'
                fullWidth
              />
              <TextField
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                type={values ? 'text' : 'password'}
                required
                variant='filled'
                autoFocus
                id='password'
                label='Password'
                fullWidth
                InputProps={{
                  endAdornment: (
                    <InputAdornment position='end'>
                      <IconButton
                        aria-label='toggle password visibility'
                        onClick={handleClickShowPassword}
                        onMouseDown={handleMouseDownPassword}
                        edge='end'
                        size='large'
                      >
                        {values ? <Visibility /> : <VisibilityOff />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </Paper>
          </DialogContent>
          <DialogActions>
            <Button type='submit' color='primary'>
              Login
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  )
}

export default Login
