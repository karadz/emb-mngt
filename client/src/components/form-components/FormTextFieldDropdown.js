import { MenuItem, TextField } from '@mui/material'
import { Controller } from 'react-hook-form'

function FormTextFieldDropdown({ control, name, label, dropdownList, noChangeData, readOnly = false }) {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <TextField
          sx={{ width: '266px' }}
          label={label}
          disabled={readOnly}
          InputLabelProps={{ shrink: true }}
          variant='filled'
          select
          size='small'
          value={value}
          InputProps={
            {
              // readOnly: readOnly,
            }
          }
          onChange={(e) => {
            // console.log('data dropdown: ', e.target)
            noChangeData({ [name]: e.target.value })
            return onChange(e.target.value)
          }}
          error={!!error}
          helperText={error ? error.message : null}
        >
          {dropdownList.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
      )}
    />
  )
}

export default FormTextFieldDropdown
