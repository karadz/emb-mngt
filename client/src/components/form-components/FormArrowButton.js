import { Button, ButtonGroup } from '@mui/material'
import React from 'react'

function FormArrowButton({ List = [], accountsStatus }) {
  return (
    <ButtonGroup aria-label='outlined primary button group'>
      {List.map((row) => {
        const { label, active = false, disabled = false } = row
        return <ArrowButton key={label} label={label} active={active} disabled={disabled} accountsStatus={accountsStatus} />
      })}
    </ButtonGroup>
  )
}

function ArrowButton({ accountsStatus = 'a', label = 'l', disabled, active }) {
  let buttonColor = '#1976d2'
  let hoverShade = '#1565c0'
  if (active) {
    buttonColor = '#9c27b0'
    hoverShade = '#7b1fa2'
  }
  // console.log('buttonColor', buttonColor)
  // console.log('hoverShade', hoverShade)
  // console.log('accountsStatus', props.accountsStatus)
  return (
    <Button
      variant='contained'
      disabled={disabled}
      sx={{
        display: 'inline-block',
        position: 'relative',
        zIndex: '1',
        border: '0px solid #000000',
        backgroundColor: 'transparent',
        color: '#ffffff',
        fontSize: '13px',
        padding: '4px 20px',
        marginLeft: '5px',
        boxShadow: 'none',

        '&:hover': {
          background: 'transparent',
          boxShadow: 'none',
        },

        '&:disabled': {
          background: 'transparent',
          boxShadow: 'none',
        },

        '&:before': {
          content: "''",
          color: 'transparent',
          position: 'absolute',
          width: '100%',
          height: '50%',
          left: '0px',
          top: '0px',
          zIndex: '-1',
          backgroundColor: buttonColor,
          transform: 'skew(40deg)',
          boxShadow: '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
        },

        '&:hover:before': {
          backgroundColor: hoverShade,
        },

        '&:disabled:before': {
          backgroundColor: '#d1d1d1',
        },

        '&:after': {
          content: "''",
          color: 'transparent',
          position: 'absolute',
          width: '100%',
          height: '50%',
          left: '0px',
          bottom: '0px',
          zIndex: '-1',
          backgroundColor: buttonColor,
          transform: 'skew(-40deg)',
          boxShadow: '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%)',
        },

        '&:hover:after': {
          backgroundColor: hoverShade,
        },

        '&:disabled:after': {
          backgroundColor: '#d1d1d1',
        },
      }}
    >
      {label}
    </Button>
  )
}

export default FormArrowButton
