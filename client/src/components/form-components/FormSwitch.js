import { FormControlLabel, Switch } from '@mui/material'
import { Controller } from 'react-hook-form'

function FormSwitch({ control, name, label, register, noChange, noChangeData, readOnly = false }) {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <FormControlLabel
          control={
            <Switch
              disabled={readOnly}
              checked={value}
              onChange={(e) => {
                // console.log('data radio: ', e.target.type)
                // console.log('data value: ', value)
                // noChangeData({ [name]: e.target.checked })
                return onChange(e.target.checked)
              }}
              color='primary'
              size='small'
            />
          }
          label={label}
          labelPlacement='top'
        />
      )}
    />
  )
}

export default FormSwitch
