import { FormControl, FormControlLabel, FormLabel, Radio, RadioGroup } from '@mui/material'
import { Controller } from 'react-hook-form'

function FormRadio({ control, name, label, List, radioLabel, readOnly = false }) {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <FormControl disabled={readOnly}>
          <FormLabel>{radioLabel}</FormLabel>
          <RadioGroup row name={name} value={value} onChange={onChange}>
            {List.map((option) => (
              <FormControlLabel key={option.value} value={option.value} control={<Radio />} label={option.label} />
            ))}
          </RadioGroup>
        </FormControl>
      )}
    />
  )
}

export default FormRadio
