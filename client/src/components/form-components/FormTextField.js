import { TextField } from '@mui/material'
import { Controller } from 'react-hook-form'

function FormTextField({ control, name, label, noChangeData, multi = false, row = 2, readOnly = false }) {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <TextField
          label={label}
          variant='filled'
          size='small'
          disabled={readOnly}
          multiline={multi}
          rows={row}
          value={value}
          InputProps={{
            // readOnly: readOnly,
            disableUnderline: readOnly,
          }}
          onFocus={(e) => e.target.select()}
          onChange={(e) => {
            // console.log('data dropdown: ', e.target)
            noChangeData({ [name]: e.target.value })
            return onChange(e.target.value)
          }}
          error={!!error}
          helperText={error ? error.message : null}
        />
      )}
    />
  )
}

export default FormTextField
