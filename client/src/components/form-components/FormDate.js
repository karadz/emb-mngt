import { TextField } from '@mui/material'
import { Controller } from 'react-hook-form'
// import AdapterDateFns from '@mui/lab/AdapterDateFns'
import AdapterDateFns from '@mui/lab/AdapterDayjs'
import LocalizationProvider from '@mui/lab/LocalizationProvider'
import DateTimePicker from '@mui/lab/DateTimePicker'

function FromDate({ control, name, label, noChangeData, readOnly = false }) {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <LocalizationProvider dateAdapter={AdapterDateFns}>
          <DateTimePicker
            label={label}
            disabled={readOnly}
            value={value}
            onChange={(data) => {
              // console.log(data.$d)
              noChangeData({ [name]: data.$d })
              return onChange(data)
            }}
            renderInput={(params) => (
              <TextField
                variant='filled'
                size='small'
                {...params}
                // InputProps={{
                //   readOnly: readOnly,
                // }}
              />
            )}
          />
        </LocalizationProvider>
      )}
    />
  )
}

export default FromDate
