import React, { useState } from 'react'
import logo from '../../assets/logo.png'
import { NavLink } from 'react-router-dom'
import { links } from './nav-data'
import ExpandLessIcon from '@mui/icons-material/ExpandLess'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { useLocalStorage } from '../../utils/useLocalStorage'
import { Tooltip } from '@mui/material'

const debug = false
let menuItem = []

const Nav = ({ bigNav }) => {
  const defaultValues = { activeUrl: ['Dashboard'], toggleMenu: true }

  const STORAGE_NAME = 'sideNavData'

  const [storage, setStorage] = useLocalStorage(STORAGE_NAME, defaultValues)

  const [activeUrl, setActiveUrl] = useState(storage.activeUrl)
  const [toggleMenu, setToggleMenu] = useState(storage.toggleMenu)

  if (debug) console.log('activeUrl', activeUrl)
  if (debug) console.log('toggleMenu', toggleMenu)

  menuItem.unshift(activeUrl)

  if (menuItem.length > 2) {
    menuItem.pop()
  }

  if (debug) console.log('menuItem', menuItem)

  if (menuItem[0] !== menuItem[1]) {
    setToggleMenu(true)
    setStorage({ ...storage, toggleMenu: true })
  }

  return (
    <div className='navbar-menu'>
      <div className={bigNav ? `nav-brand_big` : `nav-brand_small`}>
        <img src={logo} alt='The Embroidery Shop' />
      </div>
      <div className={bigNav ? `nav-list_menu_big` : `nav-list_menu_small`}>
        <nav>
          <ul>
            {links.map((link) => {
              const { id, icon, url, text, children } = link
              const parentText = text
              if (children.length === 0) {
                return (
                  <li key={id}>
                    <NavLink
                      exact
                      to={url}
                      onClick={() => {
                        setActiveUrl(parentText)
                        setStorage({ ...storage, activeUrl: parentText })
                      }}
                    >
                      <Tooltip title={bigNav ? '' : text} placement='right'>
                        <span>{icon}</span>
                      </Tooltip>
                      <span className={bigNav ? `nav--text` : 'd-none'}>{text}</span>
                    </NavLink>
                  </li>
                )
              }
              return (
                <li key={id}>
                  <NavLink
                    exact
                    to={url}
                    onClick={() => {
                      setActiveUrl(parentText)
                      setToggleMenu(!toggleMenu)
                      setStorage({ ...storage, activeUrl: parentText, toggleMenu: !toggleMenu })
                    }}
                    // className='d-flex align-items-center'
                  >
                    <Tooltip title={bigNav ? '' : text} placement='right'>
                      <span>{icon}</span>
                    </Tooltip>
                    <span className={bigNav ? `nav--text` : 'd-none'}>{text}</span>
                    <span className={bigNav ? `nav--endSpan` : `d-none`}>
                      {activeUrl === text && toggleMenu ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                    </span>
                  </NavLink>
                  <ul className={activeUrl === text && toggleMenu ? `child` : `child d-none`}>
                    {children.map((child) => {
                      const { id, url, icon, text } = child
                      return (
                        <li key={id}>
                          <NavLink
                            exact
                            to={url}
                            onClick={() => {
                              setActiveUrl(parentText)
                              setStorage({ ...storage, activeUrl: parentText })
                            }}
                          >
                            <Tooltip title={bigNav ? '' : text} placement='right'>
                              <span>{icon}</span>
                            </Tooltip>
                            <span className={bigNav ? `nav--text` : 'd-none'}>{text}</span>
                          </NavLink>
                        </li>
                      )
                    })}
                  </ul>
                </li>
              )
            })}
          </ul>
        </nav>
      </div>
    </div>
  )
}

export default Nav
