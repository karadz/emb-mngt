import ContactPhoneIcon from '@mui/icons-material/ContactPhone'
import DashboardIcon from '@mui/icons-material/Dashboard'
import GraphicEqIcon from '@mui/icons-material/GraphicEq'
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn'
import PeopleIcon from '@mui/icons-material/People'
import PlaylistAddCheckIcon from '@mui/icons-material/PlaylistAddCheck'
import SettingsIcon from '@mui/icons-material/Settings'
import MessageIcon from '@mui/icons-material/Message'
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart'
import RequestQuoteIcon from '@mui/icons-material/RequestQuote'
import AttachMoneyIcon from '@mui/icons-material/AttachMoney'
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile'
import PrecisionManufacturingIcon from '@mui/icons-material/PrecisionManufacturing'
import PriceCheckIcon from '@mui/icons-material/PriceCheck'

export const links = [
  {
    id: 1,
    icon: <DashboardIcon />,
    url: '/',
    text: 'Dashboard',
    children: [],
  },
  {
    id: 2,
    icon: <MessageIcon />,
    url: '/messages',
    text: 'Messages',
    children: [],
  },
  {
    id: 3,
    icon: <ContactPhoneIcon />,
    url: '/customers',
    text: 'Customers',
    children: [],
  },
  {
    id: 4,
    icon: <MonetizationOnIcon />,
    url: '/sales',
    text: 'Sales',
    children: [
      {
        id: 40,
        icon: <RequestQuoteIcon />,
        url: '/sales/quotation',
        text: 'Quotation',
        children: [],
      },
      {
        id: 41,
        icon: <AttachMoneyIcon />,
        url: '/sales/sales_order',
        text: 'Sales Order',
        children: [],
      },
      {
        id: 42,
        icon: <InsertDriveFileIcon />,
        url: '/sales/invoices',
        text: 'Invoices',
        children: [],
      },
    ],
  },
  {
    id: 5,
    icon: <ShoppingCartIcon />,
    url: '/products',
    text: 'Products',
    children: [],
  },

  {
    id: 6,
    icon: <PrecisionManufacturingIcon />,
    url: '/production',
    text: 'Production',
    children: [
      {
        id: 60,
        icon: <PlaylistAddCheckIcon />,
        url: '/production/list',
        text: 'Prod List',
        children: [],
      },
    ],
  },
  {
    id: 7,
    icon: <SettingsIcon />,
    url: '/settings',
    text: 'settings',
    children: [
      {
        id: 70,
        icon: <PeopleIcon />,
        url: '/settings/users',
        text: 'Users',
        children: [],
      },
      {
        id: 71,
        icon: <PriceCheckIcon />,
        url: '/settings/pricelists',
        text: 'Pricelist',
        children: [],
      },
      {
        id: 72,
        icon: <GraphicEqIcon />,
        url: '/settings/controls',
        text: 'Control',
        children: [],
      },
    ],
  },
]
