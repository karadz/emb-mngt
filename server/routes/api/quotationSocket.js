const { Quotations, getCurrentOrderID, incOrderID, calcPrice, calcOrderLinePrices } = require('../../models/Quotations')
const io = require('../../services/socket').get()

io.on('connection', (socket) => {
  console.log(`quotation connection: ${socket.id}`)
  socket.on('quotation:calculations', async (orderLineValues) => {
    console.log('Prices Before calc: ', orderLineValues)
    const orderLineCalculations = await calcOrderLinePrices(orderLineValues)
    console.log('Prices quotationSocket: ', orderLineCalculations)
    socket.emit('quotation:calculations', orderLineCalculations)
  })
})
