const express = require('express')
const router = express.Router()
const R = require('ramda')
const { Quotations } = require('../../models/Quotations')
const allAccountsStatus = require('../../services/accountsStatus')

let pages = 0

const addPageNumbers = (array) => {
  let finalPageArray = []
  pages = array.length
  let page = 1
  array.forEach((currentPage) => {
    finalPageArray.push({ [`page${page}`]: currentPage, page })
    page = page + 1
  })
  return finalPageArray
}

const splitOrderLineToPages = (order_line) => {
  let page = 1
  let pages = 5
  let newOrderLine = []
  const orderLineClone = [...order_line]

  // console.log('orderLineClone: ', orderLineClone)

  const firstPage = R.splitEvery(15, orderLineClone)
  const splitFirstPage = firstPage[0]

  newOrderLine.push(splitFirstPage)

  if (firstPage.length > 1) {
    const otherPages = R.drop(15, orderLineClone)
    const otherPagesArray = R.splitEvery(23, otherPages)
    newOrderLine = [...newOrderLine, ...otherPagesArray]
    // if array of second (last) page is greater than 20, add balance page to page to the following
    if (otherPagesArray[otherPagesArray.length - 1].length > 20) {
      newOrderLine.push([])
    }
  }
  if (firstPage.length === 1) {
    // if array of first page is greater than 12, add balance page to page 2
    if (splitFirstPage.length > 12) {
      newOrderLine.push([])
    }
  }

  return addPageNumbers(newOrderLine)
}

/*
    @route  GET api/invoice/:id
    @desc   Get generated invoice
    @access public
*/
router.get('/:id', async (req, res) => {
  console.log('req', req.body)
  try {
    const quotation = await Quotations.findById(req.params.id)
    const newOrderLine = splitOrderLineToPages(quotation.order_line)
    console.log('Generate accountsStatus', allAccountsStatus)
    console.log('newOrderLine: ', newOrderLine)
    const accountsStatus = allAccountsStatus.latest
    console.log('Get accountsStatus', accountsStatus)
    res.render('invoice/index', { quotation, pages, newOrderLine, accountsStatus })
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

module.exports = router
