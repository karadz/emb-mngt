const express = require('express')
const router = express.Router()

const puppeteer = require('puppeteer')
const accountsStatus = require('../../services/accountsStatus')

const generateInvoicePDF = async (id = '', accountsStatus) => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()

  // console.log('generateInvoicePDF accountsStatus', accountsStatus)

  await page.goto(`http://localhost:4000/invoice/${id}`, { waitUntil: 'networkidle2' })

  const pdfBuffer = await page.pdf({ format: 'a4', printBackground: true })

  await page.close()
  await browser.close()

  return pdfBuffer
}

const generateReceiptPDF = async (id = '') => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()

  await page.goto(`http://localhost:4000/receipt/${id}`, { waitUntil: 'networkidle2' })

  const dimensions = await page.evaluate(() => {
    return {
      // width: document.documentElement.clientWidth,
      width: document.getElementById('invoice-POS').offsetWidth,
      // height: document.documentElement.clientHeight,
      height: document.getElementById('invoice-POS').offsetHeight,
      deviceScaleFactor: window.devicePixelRatio,
    }
  })

  console.log('Dimensions:', dimensions)

  const pdfBuffer = await page.pdf({ width: dimensions.width, height: dimensions.height, pageRanges: '1', printBackground: true })

  await page.close()
  await browser.close()

  return pdfBuffer
}

router.post('/invoice/pdf', async (req, res) => {
  console.time('Page')
  // console.log('id', req.body.id)
  accountsStatus.current = req.body.accountsStatus
  // console.log('accountsStatus', accountsStatus.latest)
  const pdf = await generateInvoicePDF(req.body.id, req.body.accountsStatus)
  res.set({ 'Content-Type': 'application/pdf', 'Content-Length': pdf.length })
  console.timeEnd('Page')
  res.send(pdf)
})

router.post('/receipt/pdf', async (req, res) => {
  console.time('Page')
  console.log(req.body.id)
  const pdf = await generateReceiptPDF(req.body.id, req.body.accountsStatus)
  res.set({ 'Content-Type': 'application/pdf', 'Content-Length': pdf.length })
  console.timeEnd('Page')
  res.send(pdf)
})

module.exports = router
// module.exports = accountsStatus
