const express = require('express')
const router = express.Router()
const Customers = require('../../models/Customers')
const csv = require('csvtojson')
const R = require('ramda')
const validateRequestSchema = require('../../middleware/validateRequestSchema')
const { customerValidator } = require('../../schema/customersSchema')

// File upload
const multer = require('multer')
let storage = multer.memoryStorage()
var upload = multer({ storage: storage })
const auth = require('../../middleware/auth')

/**
 * @route   GET api/customers
 * @desc    Get all customers
 * @access  Private
 */
router.get('/', auth, async (req, res) => {
  try {
    const allCustomers = await Customers.find({ isDeleted: false }).sort({ name: 1 })

    res.status(200).json(allCustomers)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 * @route   GET api/customers/:id
 * @desc    Get one customer by id
 * @access  Private
 */
router.get('/:id', auth, async (req, res) => {
  try {
    const oneCustomers = await Customers.findById(req.params.id)
    res.status(200).json(oneCustomers)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 * @route   POST api/customers
 * @desc    Create customer
 * @access  Private
 */
router.post('/', auth, customerValidator, validateRequestSchema, async (req, res) => {
  try {
    const customer = new Customers(
      R.reject(R.anyPass([R.isEmpty, R.isNil]))({
        ...req.body,
        user_id: req.user.id,
      })
    )

    const post = await customer.save()
    // console.log(post)
    res.status(200).json(post)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 * @route   PUT api/customers/:id
 * @desc    Edit customer
 * @access  Private
 */
router.put('/:id', auth, customerValidator, validateRequestSchema, async (req, res) => {
  try {
    const updateCustomer = R.reject(R.anyPass([R.isEmpty, R.isNil]))({
      ...req.body,
      user_id: req.user.id,
    })

    const update = await Customers.findByIdAndUpdate(req.params.id, updateCustomer, { new: true })

    res.status(200).json(update)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *   @route   DELETE api/customers/:id
 *   @desc    Delete customer
 *   @access  Private
 */
router.delete('/:id', auth, async (req, res) => {
  try {
    const customerUpdate = {
      isDeleted: true,
    }

    const customer = await Customers.findByIdAndUpdate(req.params.id, customerUpdate, { new: true })

    res.status(200).json(customer)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   POST api/customers/upload
 *    @desc    Create many customers
 *    @access  Private
 */
router.post('/upload', [auth, upload.single('csv_file')], async (req, res) => {
  try {
    let csvString = req.file.buffer.toString()
    // console.log('csvString: ', csvString)
    const jsonArray = await csv()
      .preFileLine((fileLine, idx) => {
        if (idx === 0) {
          return fileLine.toLowerCase()
        }
        return fileLine
      })
      .fromString(csvString)
    // console.log('jsonArray', jsonArray)

    const newArray = jsonArray.map((array) => {
      const arrayValues = R.reject(R.anyPass([R.isEmpty, R.isNil]))(array)
      return { ...arrayValues, user_id: req.user.id }
    })
    console.log('newArray', newArray)
    const resp = await Customers.insertMany(newArray)
    res.status(200).send(resp)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

module.exports = router
