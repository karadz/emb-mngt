const express = require('express')
const router = express.Router()
const R = require('ramda')
const { Quotations } = require('../../models/Quotations')

/*
    @route  GET api/receipt/:id
    @desc   Get generated receipt
    @access public
*/
router.get('/:id', async (req, res) => {
  try {
    const quotation = await Quotations.findById(req.params.id)

    res.render('receipt/index', { quotation })
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

module.exports = router
