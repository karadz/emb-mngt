const express = require('express')
const router = express.Router()
const auth = require('../../middleware/auth')
const Users = require('../../models/Users')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('config')
const validateRequestSchema = require('../../middleware/validateRequestSchema')
const { authValidator } = require('../../schema/authSchema')

/**
 * @route   POST api/auth
 * @desc    Authenticate user & Get token
 * @access  Public
 */
router.post('/', authValidator, validateRequestSchema, async (req, res) => {
  try {
    // Destructor req.body
    const { email, password } = req.body
    // console.log(req.body)

    // Check if user exist
    const currentUser = await Users.findOne({ email })
    // console.log(currentUser)

    // Email does not exist
    if (!currentUser) {
      return res.status(400).json({ errors: [{ msg: 'Invalid Credentials' }] })
    }

    // If user is deleted
    if (currentUser.isDeleted === true) {
      return res.status(401).json({ errors: [{ msg: 'User is not authorized' }] })
    }

    // Match password
    const isMatch = await bcrypt.compare(password, currentUser.password)

    // No match
    if (!isMatch) {
      return res.status(400).json({ errors: [{ msg: 'Invalid Credentials' }] })
    }

    // Return jsonwebtoken
    const payload = {
      user: {
        id: currentUser.id,
        role: currentUser.role,
        isDeleted: currentUser.isDeleted,
      },
    }

    // TODO Change expiration time in production
    jwt.sign(payload, config.get('JWT_SECRET'), { expiresIn: config.get('EXPIRATION_TIME') }, (err, token) => {
      if (err) throw err
      return res.json({ token, user: currentUser })
    })
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 * @route   GET api/users
 * @desc    Get Auth user
 * @access  Private
 */
router.get('/', auth, async (req, res) => {
  try {
    const user = await Users.findById(req.user.id).select('-password')
    res.json(user)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

module.exports = router
