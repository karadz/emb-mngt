const express = require('express')
const router = express.Router()
const { Products, incProductID, getCurrentProductID } = require('../../models/Products')
const csv = require('csvtojson')
const { body, validationResult } = require('express-validator')
const R = require('ramda')
const validateRequestSchema = require('../../middleware/validateRequestSchema')
const { productsValidation } = require('../../schema/productsSchema')
const { findEnumValue, constants } = require('../../models/CustomEnum')

// File upload
const multer = require('multer')

let storage = multer.memoryStorage()

var upload = multer({ storage: storage })

const auth = require('../../middleware/auth')

/**
 *    @route   POST api/products
 *    @desc    Create product
 *    @access  Private
 */
router.post('/', auth, productsValidation, validateRequestSchema, async (req, res) => {
  try {
    let productID = await getCurrentProductID()
    productID = incProductID(productID)

    // Product creation
    const product = new Products(
      R.reject(R.anyPass([R.isEmpty, R.isNil]))({
        user_id: req.user.id,
        ...req.body,
        productID,
      })
    )

    const post = await product.save()

    res.status(200).json(post)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   PUT api/products
 *    @desc    Edit product by id
 *    @access  Private
 */
router.put('/:id', auth, productsValidation, validateRequestSchema, async (req, res) => {
  try {
    //  Creation product object
    const productUpdate = R.reject(R.anyPass([R.isEmpty, R.isNil]))({
      user_id: req.user.id,
      ...req.body,
    })

    // console.log(req.params.id)

    const post = await Products.findByIdAndUpdate(req.params.id, productUpdate, { new: true })

    res.status(200).json(post)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   GET api/products?page=1&pageSize=10
 *    @desc    Get all products with pagination as params
 *    @access  Private
 */
router.get('/', auth, async (req, res) => {
  try {
    const products = await Products.find({ isDeleted: false })

    res.status(200).json(products)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   GET api/products/:id
 *    @desc    Get product by id
 *    @access  Private
 */
router.get('/:id', auth, async (req, res) => {
  try {
    const products = await Products.findById(req.params.id)

    res.status(200).json(products)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   DELETE api/products/:id
 *    @desc    Delete product by id
 *    @access  Private
 */
router.delete('/:id', auth, async (req, res) => {
  try {
    const userUpdate = {
      isDeleted: true,
    }

    const products = await Products.findByIdAndUpdate(req.params.id, userUpdate, { new: true })

    res.status(200).json(products)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   POST api/products/upload
 *    @desc    Create many product
 *    @access  Private
 */
router.post('/upload', [auth, upload.single('csv_file')], async (req, res) => {
  let productID = await getCurrentProductID()
  // console.log('productID', productID)

  try {
    // console.log('getCurrentProductID', getCurrentProductID)
    // console.log('incProductID', incProductID)

    let csvString = req.file.buffer.toString()
    const jsonArray = await csv().fromString(csvString)

    const newArray = jsonArray.map((array) => {
      const { Name, Title, Stitches } = array
      productID = incProductID(productID)
      return { user_id: req.user.id, name: Name, title: Title, stitches: parseInt(Stitches), category: constants.DEF_PRODUCT_CATEGORY, productID }
    })
    const resp = await Products.insertMany(newArray)
    res.status(200).send(resp)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   PUT api/products/update
 *    @desc    Edit multiple product
 *    @access  Private
 *    TODO:    Test this route
 */
router.put('/update', [auth, upload.single('csv_file')], async (req, res) => {
  try {
    let csvString = req.file.buffer.toString()
    const jsonArray = await csv().fromString(csvString)

    const newArray = jsonArray.map(async (array) => {
      const { Name, Title, Stitches } = array
      productID = incProductID(productID)
      const resp = await Products.Update({ name: Name }, { $set: { user: req.user.id, name: Name, title: Title, stitches: parseInt(Stitches) } })
      return resp
    })

    res.status(200).send(newArray)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

module.exports = router
