const express = require('express')
const router = express.Router()
const Users = require('../../models/Users')
const auth = require('../../middleware/auth')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const R = require('ramda')
const validateRequestSchema = require('../../middleware/validateRequestSchema')
const { usersValidation } = require('../../schema/usersSchema')
const { CustomEnum } = require('../../models/CustomEnum')

const debug = false

// @route   POST api/users
// @desc    Create user
// @access  Public
// TODO make the route protected and access buy admin only
router.post('/', usersValidation, validateRequestSchema, async (req, res) => {
  try {
    // Destructor req.body
    const { email, password, mobile } = req.body

    // Check if user exist
    const oldUser = await Users.findOne({ email })
    if (oldUser) {
      return res.status(400).json({ error: [{ msg: 'User already exists' }] })
    }

    // Create an array and trim
    let mobileArr = mobile.split(',').map((phone) => phone.trim())

    // Encrypt the password
    const salt = await bcrypt.genSalt(10)
    encryptPassword = await bcrypt.hash(password, salt)

    const user = new Users(
      R.reject(R.anyPass([R.isEmpty, R.isNil]))({
        ...req.body,
        mobile: mobileArr,
        password: encryptPassword,
      })
    )

    // console.log(user)

    await user.save()

    res.json(user)

    if (debug) console.log('User registered')
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

// @route   PUT api/users/:id
// @desc    Edit user by id
// @access  Private
router.put('/:id', auth, usersValidation, validateRequestSchema, async (req, res) => {
  try {
    // Destructor req.body
    const { email, password, mobile } = req.body

    // Create an array and trim
    let mobileArr = mobile.split(',').map((phone) => phone.trim())

    // Encrypt the password
    const salt = await bcrypt.genSalt(10)
    encryptPassword = await bcrypt.hash(password, salt)

    const userUpdate = R.reject(R.anyPass([R.isEmpty, R.isNil]))({
      ...req.body,
      mobile: mobileArr,
      password: encryptPassword,
    })

    // console.log(Users)

    const update = await Users.findByIdAndUpdate(req.params.id, userUpdate, { new: true })

    // console.log(update)

    res.status(200).json(update)
  } catch (err) {
    console.error(err)
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

// @route   DELETE api/users/:id
// @desc    Edit user by id
// @access  Private
router.delete('/:id', [auth], async (req, res) => {
  // Validation

  try {
    const userUpdate = {
      isDeleted: true,
    }

    // console.log(req.params.id)

    const update = await Users.findByIdAndUpdate(req.params.id, userUpdate, { new: true })

    // console.log(update)

    res.status(200).json(update)
  } catch (err) {
    console.error(err)
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

// @route   GET api/users/
// @desc    GET all users
// @access  Private
router.get('/', auth, async (req, res) => {
  try {
    /**
     * Check if users are more than one,
     * if true check if current user is not admin,
     * if true block access to create users for non Admin
     * TODO: remove passwords when fetching data
     */
    const allUsers = await Users.find().select('-password')
    if (allUsers.length >= 1 && req.user.role !== 'admin' && req.user.isDeleted !== true) {
      const currentUser = await Users.findOne({ _id: req.user.id }).select('-password')
      // console.log(currentUser)
      return res.status(200).json(currentUser)
    }

    res.status(200).json(allUsers)
  } catch (err) {
    console.error(err.message)
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

module.exports = router
