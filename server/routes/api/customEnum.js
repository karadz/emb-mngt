const express = require('express')
const router = express.Router()
const { CustomEnum } = require('../../models/CustomEnum')
const auth = require('../../middleware/auth')
const validateRequestSchema = require('../../middleware/validateRequestSchema')
const { body } = require('express-validator')
const { customEnumValidator } = require('../../schema/customEnumSchema')

/*
    @route  GET api/settings/custom_enum
    @desc   Get all custom enum of a given type
    @access private
*/
router.get('/', auth, async (req, res) => {
  try {
    const customEnum = await CustomEnum.find({ isDeleted: false })
    res.status(200).json(customEnum)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})
/*
    @route  GET api/settings/custom_enum/:id
    @desc   Get all custom enum
    @access private
*/
router.get('/:id', auth, async (req, res) => {
  try {
    const customEnum = await CustomEnum.findOne(req.params.id)
    res.status(200).json(customEnum)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})
/*
    @route  POST api/settings/custom_enum
    @desc   Create custom enum
    @access private
*/
router.post('/', auth,  customEnumValidator, validateRequestSchema, async (req, res) => {
  try {
    const { isDefault, type } = req.body
    if (isDefault) {
      await CustomEnum.updateMany(
        { type: type },
        {
          $set: {
            isDefault: false,
          },
        },
        {
          multi: true,
          upsert: false,
        }
      )
    }
    const newCustomEnum = new CustomEnum({
      ...req.body,
    })
    const post = await newCustomEnum.save()
    res.status(200).json(post)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/*
    @route  PUT api/settings/custom_enum
    @desc   Edit custom enum
    @access private
*/
router.put('/:id', auth, customEnumValidator, validateRequestSchema, async (req, res) => {
  // console.log(`In the Backend: ${req.body} Req Body: ${req.params.id}`)
  try {
    const { isDefault, type } = req.body
    if (isDefault) {
      await CustomEnum.updateMany(
        { type: type },
        {
          $set: {
            isDefault: false,
          },
        },
        {
          multi: true,
          upsert: false,
        }
      )
    }
    const updateCustomEnum = await CustomEnum.findByIdAndUpdate(req.params.id, req.body, { new: true })
    res.status(200).json(updateCustomEnum)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})
/*
    @route  DELETE api/settings/custom_enum
    @desc   Delete custom enum
    @access private
*/
router.delete('/:id', auth, async (req, res) => {
  try {
    const customEnumToDelete = await CustomEnum.findById(req.params.id)
    const { isDefault, type } = customEnumToDelete
    if (isDefault) {
      return res.status(403).send({ errors: [{ msg: 'Record is default, can not be deleted if another default is not set' }] })
    }
    const deleteCustomEnum = {
      isDeleted: true,
    }
    const updateCustomEnum = await CustomEnum.findByIdAndUpdate(req.params.id, deleteCustomEnum, { new: true })
    res.status(200).json(updateCustomEnum)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

module.exports = router
