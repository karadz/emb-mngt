const express = require('express')
const router = express.Router()
const Pricelists = require('../../models/Pricelists')
const { Quotations, getCurrentOrderID, incOrderID, calcOrderLinePrices } = require('../../models/Quotations')
const R = require('ramda')

const auth = require('../../middleware/auth')
const validateRequestSchema = require('../../middleware/validateRequestSchema')
const { quotationsValidation } = require('../../schema/quotationsSchema')

const { calculateQuotation } = require('./quotationSocket')

/**
 *    @route   POST api/sales/quotations
 *    @desc    Create quotation
 *    @access  Private
 */
router.post('/', auth, quotationsValidation, validateRequestSchema, async (req, res) => {
  try {
    let getOrderID = await getCurrentOrderID()

    let orderID = incOrderID(getOrderID)

    const ordersLinesClone = R.clone(req.body.order_line)

    const priceList = await Pricelists.findById(req.body.pricelist_id)

    const tax_rate = req.body.tax_rate
    const discount_rate = req.body.discount_rate
    console.log('RQ body: ', req.body)
    console.log('priceList: ', priceList)

    const prices = await calcOrderLinePrices({ pricelistData: priceList, order_line: ordersLinesClone, tax_rate, discount_rate })

    console.log('prices: ', prices)

    // Product creation
    const quotation = new Quotations({
      ...req.body,
      ...prices,
      user_id: req.user.id,
      orderID,
    })

    console.log('### @ Quotation', quotation)

    const post = await quotation.save()

    res.status(200).json(post)
  } catch (err) {
    console.log(err.message)
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   PUT api/sales/quotations/:id
 *    @desc    Edit a quotation by id
 *    @access  Private
 */
router.put('/:id', auth, quotationsValidation, validateRequestSchema, async (req, res) => {
  try {
    // Product creation
    const updateQuotation = R.reject(R.anyPass([R.isEmpty, R.isNil]))({
      user_id: req.user.id,
      ...req.body,
    })

    const update = await Quotations.findByIdAndUpdate(req.params.id, updateQuotation, { new: true })

    res.status(200).json(update)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   GET api/sales/quotations
 *    @desc    GET all quotation
 *    @access  Private
 */
router.get('/', auth, async (req, res) => {
  try {
    const post = await Quotations.find({ isDeleted: false })

    res.status(200).json(post)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   GET api/sales/quotations/:id
 *    @desc    GET a quotation by id
 *    @access  Private
 */
router.get('/:id', auth, async (req, res) => {
  try {
    const post = await Quotations.findById(req.params.id)

    res.status(200).json(post)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *    @route   DELETE api/sales/quotations/:id
 *    @desc    DELETE a quotation by id
 *    @access  Private
 */
router.delete('/:id', auth, async (req, res) => {
  try {
    const quotationUpdate = {
      isDeleted: true,
    }
    const quotation = await Quotations.findByIdAndUpdate(req.params.id, quotationUpdate, { new: true })
    res.status(200).json(quotation)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

module.exports = router
