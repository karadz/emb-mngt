const express = require('express')
const router = express.Router()
const Pricelists = require('../../models/Pricelists')
const auth = require('../../middleware/auth')
const R = require('ramda')
const validateRequestSchema = require('../../middleware/validateRequestSchema')
const { pricelistsValidation } = require('../../schema/pricelistsSchema')

/*
    @route  POST api/settings/pricelists
    @desc   Create Pricelist
    @access private
*/
router.post('/', auth, pricelistsValidation, validateRequestSchema, async (req, res) => {
  try {
    // Create pricelist
    const pricelist = new Pricelists(
      R.reject(R.anyPass([R.isEmpty, R.isNil]))({
        user_id: req.user.id,
        ...req.body,
      })
    )

    // console.log(pricelist)

    const post = await pricelist.save()

    res.status(200).json(post)
  } catch (err) {
    // console.log(err.message)
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 *  @route  GET api/settings/pricelists
 *  @desc   Get all pricelists
 *  @access private
 */
router.get('/', auth, async (req, res) => {
  try {
    const pricelists = await Pricelists.find({ isDeleted: false })

    res.status(200).json(pricelists)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 * @route  GET api/settings/pricelists
 * @desc   GET pricelist by id
 * @access private
 */
router.get('/:id', auth, async (req, res) => {
  try {
    const pricelists = await Pricelists.findById(req.params.id)

    res.status(200).json(pricelists)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 * @route  DELETE api/settings/pricelists
 * @desc   DELETE pricelist by id
 * @access private
 */
router.delete('/:id', auth, async (req, res) => {
  try {
    const pricelistUpdate = {
      isDeleted: true,
    }
    const pricelist = await Pricelists.findByIdAndUpdate(req.params.id, pricelistUpdate, { new: true })
    res.status(200).json(pricelist)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

/**
 * @route  PUT api/settings/pricelists
 * @desc   EDIT pricelist by id
 * @access private
 */
router.put('/:id', auth, pricelistsValidation, validateRequestSchema, async (req, res) => {
  try {
    //  Creation pricelist object
    const pricelistUpdate = R.reject(R.anyPass([R.isEmpty, R.isNil]))({
      user_id: req.user.id,
      ...req.body,
    })

    const pricelist = await Pricelists.findByIdAndUpdate(req.params.id, pricelistUpdate, { new: true })
    res.status(200).json(pricelist)
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
})

module.exports = router
