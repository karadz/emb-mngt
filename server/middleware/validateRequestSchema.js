const { validationResult } = require('express-validator')

module.exports = function (req, res, next) {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    console.log(errors.array({ onlyFirstError: true }))
    return res.status(400).json({ errors: errors.array({ onlyFirstError: true }) })
  }

  next()
}
