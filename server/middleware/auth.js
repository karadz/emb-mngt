const jwt = require('jsonwebtoken')
const config = require('config')
const Users = require('../models/Users')

module.exports = async function (req, res, next) {
  // Get token from header
  const token = req.header('x-auth-token')

  // Check if token exists
  if (!token) {
    return res.status(401).json({ error: [{ msg: 'No token, authorization denied' }] })
  }

  // Verify token
  try {
    const decoded = jwt.verify(token, config.get('JWT_SECRET'))
    req.user = decoded.user
    next()
  } catch (err) {
    res.status(401).json({ error: [{ msg: 'Token not valid' }] })
  }
}
