const { body } = require('express-validator')
const ObjectId = require('mongoose').Types.ObjectId
const { constants, findEnumValue } = require('../models/CustomEnum')

const quotationsValidation = [
  body('customer').notEmpty().withMessage('Customer can not be empty').bail().isObject().withMessage('Customer line must be an object').bail(),
  body('customer._id')
    .notEmpty()
    .withMessage('Customer id can not be empty')
    .bail()
    .custom((id) => {
      if (!ObjectId.isValid(id)) {
        throw new Error('Please enter a valid customer id')
      }
      return true
    }),
  body('customer.name')
    .notEmpty()
    .withMessage('Customer name can not be empty')
    .bail()
    .isString()
    .withMessage('Please enter a valid customer name')
    .bail(),
  body('customer.vatOrBpNo').optional({ checkFalsy: true }).isString().withMessage('Please enter a valid Vat or Bp No').bail(),
  body('customer.isCompany')
    .optional({ checkFalsy: true })
    .custom(async (value) => {
      const results = await findEnumValue(constants.CUSTOMER_TYPES, value)
      if (!results) {
        console.log('Customer enum does not exist')
        return Promise.reject()
      }
    })
    .withMessage('Please enter a either "individual" or "company"')
    .bail(),
  body('customer.email').optional({ checkFalsy: true }).isEmail().normalizeEmail().withMessage('Please enter a valid email').bail(),
  body('customer.phone').optional({ checkFalsy: true }).isArray().withMessage('Please enter a valid phone number').bail(),
  body('customer.address').optional({ checkFalsy: true }).isString().withMessage('Please enter a valid address').bail(),
  body('customer.organization').optional({ checkFalsy: true }).isString().withMessage('Please enter a valid organization').bail(),

  body('pricelist_id')
    .notEmpty()
    .withMessage('Pricelist id can not be empty')
    .bail()
    .custom((id) => {
      if (!ObjectId.isValid(id)) {
        throw new Error('Please enter a valid pricelist id')
      }
      return true
    }),
  // body('orderID')
  //   .optional()
  //   .matches(/SO\s[0-9]{7}$/)
  //   .withMessage('Please enter a valid Order number')
  //   .bail(),
  body('comments').optional().isString().withMessage('Please enter a valid comment').bail(),
  body('order_date').optional().isISO8601().withMessage('Please enter a valid order date').bail(),
  body('quote_expiry_date').optional().isISO8601().withMessage('Please enter a valid quotation expiry date').bail(),
  body('required_date').optional().isISO8601().withMessage('Please enter a valid collection date').bail(),
  body('isDeleted').optional().isBoolean().withMessage('Is deleted must be boolean').bail(),
  body('total').optional().isNumeric().withMessage('Please enter a valid total').bail(),
  body('order_line').isArray().withMessage('Order line must be an array').bail(),
  body('order_line.*._id')
    .notEmpty()
    .withMessage('Product id can not be empty')
    .custom((id) => {
      if (!ObjectId.isValid(id)) {
        throw new Error('Please enter a valid product id')
      }
      // Indicates the success of this synchronous custom validator
      return true
    }),
  body('order_line.*.productID')
    .notEmpty()
    .withMessage('Product ID can not be empty')
    .bail()
    .isString()
    .withMessage('Please enter a valid Product ID')
    .bail(),
  body('order_line.*.name')
    .notEmpty()
    .withMessage('Order Line Name can not be empty')
    .bail()
    .isString()
    .withMessage('Please enter a valid product name')
    .bail(),
  body('order_line.*.quantity')
    .notEmpty()
    .withMessage('Quantity can not be empty')
    .bail()
    .isNumeric()
    .withMessage('Please enter a valid quantity')
    .bail(),
  body('order_line.*.category')
    .notEmpty()
    .withMessage('Category can not be empty')
    .bail()
    .isString()
    .withMessage('Please enter a valid category')
    .bail(),
  body('order_line.*.unit_price')
    .if(body('order_line.*.category').not().equals(constants.DEF_PRODUCT_CATEGORY))
    .notEmpty()
    .withMessage('Sales price name must not be empty')
    .bail()
    .isNumeric()
    .withMessage('Sales price must be a number')
    .bail(),
  body('order_line.*.emb_type')
    .if(body('order_line.*.category').equals(constants.DEF_PRODUCT_CATEGORY))
    .notEmpty()
    .withMessage('Embroidery type must not be empty')
    .bail()
    .isString()
    .withMessage('Embroidery type must be a string')
    .bail(),
  body('order_line.*.garment_positions')
    .if(body('order_line.*.category').equals(constants.DEF_PRODUCT_CATEGORY))
    .notEmpty()
    .withMessage('Embroidery position type must not be empty')
    .bail()
    .isString()
    .withMessage('Embroidery position type must be a string')
    .bail(),
  body('order_line.*.stitches')
    .if(body('order_line.*.category').equals(constants.DEF_PRODUCT_CATEGORY))
    .notEmpty()
    .withMessage('Embroidery stitches type must not be empty')
    .bail()
    .isNumeric()
    .withMessage('Embroidery stitches type must be a number')
    .bail(),
]

module.exports = { quotationsValidation }
