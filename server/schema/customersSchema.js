const { body } = require('express-validator')
const { findEnumValue, constants } = require('../models/CustomEnum')

const customerValidator = [
  body('isCompany')
    .notEmpty()
    .withMessage('Company or Individual can not be empty')
    .bail()
    .custom(async (value) => {
      const results = await findEnumValue(constants.CUSTOMER_TYPES, value)
      if (!results) {
        console.log('Customer enum does not exist')
        return Promise.reject()
      }
    })
    .withMessage("Please enter category: 'individual' or 'company'")
    .bail(),
  body('rating').optional().isNumeric().withMessage('Please enter a valid rating').bail(),
  body('balance').optional().isNumeric().withMessage('Please enter a valid balance').bail(),
  body('name').notEmpty().withMessage('Customer name can not be empty').bail().isString().withMessage('Please enter a valid name').bail(),
  body('vatBpNo').optional().isString().withMessage('Please enter a valid Vat or BP No').bail(),
  body('email').optional({ nullable: true, checkFalsy: true }).isEmail().withMessage('Please include a valid email').bail(),
  body('phone').notEmpty().withMessage('Phone number can not be empty').bail().isString().withMessage('Please enter a valid phone number').bail(),
  body('notes').optional().isString().withMessage('Please enter a valid notes').bail(),
  body('address').optional().isString().withMessage('Please enter a valid address').bail(),
]

module.exports = { customerValidator }
