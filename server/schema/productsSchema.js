const { body } = require('express-validator')
const { constants, findEnumValue } = require('../models/CustomEnum')

const productsValidation = [
  body('name').notEmpty().withMessage('Product name must not be empty').bail().isString().withMessage('Please enter a valid product name').bail(),
  body('productID').optional().isString().withMessage('Please enter a valid product ID').bail(), // TODO: Use optional
  body('title').optional().isString().withMessage('Please enter a valid title').bail(),
  body('description').optional().isString().withMessage('Please enter a valid description').bail(),
  body('image').optional().isString().withMessage('Please enter a valid image').bail(),
  body('rating').optional().isNumeric().withMessage('Please enter a valid rating').bail(),
  body('category')
    .notEmpty()
    .withMessage('Category can not be empty')
    .bail()
    .custom(async (value) => {
      const results = await findEnumValue(constants.PRODUCT_CATEGORIES, value)
      if (!results) {
        console.log('Product enum does not exist')
        return Promise.reject()
      }
    })
    .withMessage("Please enter category: constants.DEF_PRODUCT_CATEGORY, 'shirt', 'threads', 'backing' or 'other'")
    .bail(),
  body('unit_price')
    .if(body('category').not().equals(constants.DEF_PRODUCT_CATEGORY))
    .notEmpty()
    .withMessage('Sales price name must not be empty')
    .bail()
    .isNumeric()
    .withMessage('Sales price must be a number')
    .bail(),
  body('quantity')
    .if(body('category').not().equals(constants.DEF_PRODUCT_CATEGORY))
    .notEmpty()
    .withMessage('Quantity price name must not be empty')
    .bail()
    .isNumeric()
    .withMessage('Quantity price must be a number')
    .bail(),
  body('stitches')
    .if(body('category').equals(constants.DEF_PRODUCT_CATEGORY))
    .notEmpty()
    .withMessage('Stitches name must not be empty')
    .bail()
    .isNumeric()
    .withMessage('Stitches must be a number')
    .bail(),
]

module.exports = { productsValidation }
