const { body } = require('express-validator')

const pricelistsValidation = [
  body('name').notEmpty().withMessage('Pricelist name can not be empty').bail().isString().withMessage('Please enter a valid name').bail(),
  body('isDefault').optional().isBoolean().withMessage('Please enter a valid default').bail(),
  body('pricelist').notEmpty().withMessage('Pricelist can not be empty').bail().isArray().withMessage('Pricelist must be an array').bail(),
  body('pricelist.*.embroidery_type')
    .notEmpty()
    .withMessage('Embroidery type can not be empty')
    .bail()
    .isString()
    .withMessage('Please enter a valid embroidery type')
    .bail(),
  body('pricelist.*.max_qty')
    .notEmpty()
    .withMessage('Maximum quantity can not be empty')
    .bail()
    .isNumeric()
    .withMessage('Please enter a valid Maximum quantity')
    .bail(),
  body('pricelist.*.price_per_thus_stitches')
    .notEmpty()
    .withMessage('Price per thousand stitches can not be empty')
    .bail()
    .isNumeric()
    .withMessage('Please enter a valid price per thousand stitches')
    .bail(),
  body('pricelist.*.min_price')
    .notEmpty()
    .withMessage('Minimum price can not be empty')
    .bail()
    .isNumeric()
    .withMessage('Please enter a valid minimum price')
    .bail(),
]

module.exports = { pricelistsValidation }
