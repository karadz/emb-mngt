const { body } = require('express-validator')
const { findEnumValue, constants } = require('../models/CustomEnum')

const usersValidation = [
  body('name').notEmpty().withMessage('User name can not be empty').bail().isString().withMessage('Please enter a valid name').bail(),
  body('role')
    .notEmpty()
    .withMessage('Role can not be empty')
    .bail()
    .custom(async (value) => {
      const results = await findEnumValue(constants.USER_ROLES, value)
      if (!results) {
        console.log('enum does not exist')
        return Promise.reject()
      }
    })
    .withMessage('Please enter correct role')
    .bail(),
  body('email').notEmpty().withMessage('Email can not be empty').bail().isEmail().withMessage('Please include a valid email').bail(),
  body('mobile').notEmpty().withMessage('Mobile number can not be empty').bail().isString().withMessage('Please enter a valid phone number').bail(),
  body('image').optional().isString().withMessage('Please enter a valid image').bail(),
  body('password')
    .notEmpty()
    .withMessage('Password can not be empty')
    .bail()
    .isLength({ min: 6 })
    .withMessage('The password must have at least 6 characters')
    .bail()
    .matches(/\d/)
    .withMessage('The password must contain a number')
    .bail()
    .isString()
    .withMessage('Please enter a valid password')
    .bail(),
  body('passwordConfirmation').custom((value, { req }) => {
    if (value !== req.body.password) {
      throw new Error('Password confirmation does not match password')
    }
    // Indicates the success of this synchronous custom validator
    return true
  }),
]

module.exports = { usersValidation }
