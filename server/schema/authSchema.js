const { body } = require('express-validator')

const authValidator = [
  body('email').notEmpty().withMessage('Email can not be empty').bail().isEmail().withMessage('Please include a valid email').bail(),
  body('password')
    .notEmpty()
    .withMessage('Password can not be empty')
    .bail()
    .isLength({ min: 6 })
    .withMessage('The password must have at least 6 characters')
    .bail()
    .matches(/\d/)
    .withMessage('The password contain a number')
    .bail()
    .isString()
    .withMessage('Please enter a valid password')
    .bail(),
]

module.exports = { authValidator }
