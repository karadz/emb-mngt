const { body } = require('express-validator')

const customEnumValidator = [
  body('type').notEmpty().withMessage('Type must not be empty').bail().isString().withMessage('Please include valid type').bail(),
  body('isDeleted').notEmpty().withMessage('Deleted must not be empty').bail().isBoolean().withMessage('Deleted must be true or false').bail(),
  body('isDefault').notEmpty().withMessage('Default must not be empty').bail().isBoolean().withMessage('Default must be true or false').bail(),
  body('value').notEmpty().withMessage('Value must not be empty').bail().isString().withMessage('Please include valid value').bail(),
]

module.exports = { customEnumValidator }
