const express = require('express')
const app = express()
const httpServer = require('http').createServer(app)

const options = { cors: { origin: 'https://localhost:3000' } }

// Export io using singleton
const io = require('./services/socket').init(httpServer)

const cors = require('cors')
const helmet = require('helmet')
const connectDB = require('./services/db')
const config = require('config') 

// Initial values in the Database
const seed = require('./deploy/seed')

const debug = false

if (debug) console.log(`NODE_ENV: ${process.env.NODE_ENV}`)
// console.log(`app: ${app.get('env')}`)
if (debug) console.log(`Application name: ${config.get('name')}`)
if (debug) console.log(`Application secrets: ${config.get('JWT_SECRET')}`)

// Connect Database
connectDB()

// Init Middleware
// app.use(express.json({ limit: '50mb' }))
// app.use(express.urlencoded({ limit: '50mb' }))
app.use(express.json())
app.use(cors())
app.use(helmet())
app.set('view engine', 'ejs')
app.use(express.static(__dirname + '/public'))

// Define Routes
app.use('/api/customers', require('./routes/api/customers'))
// app.use('/api/orders', require('./routes/api/orders'))
app.use('/api/users', require('./routes/api/users'))
app.use('/api/auth', require('./routes/api/auth'))
app.use('/api/products', require('./routes/api/products'))
app.use('/api/settings/pricelists', require('./routes/api/pricelists'))
app.use('/api/settings/custom_enum', require('./routes/api/customEnum'))
app.use('/api/sales/quotations', require('./routes/api/quotations'))
app.use('/api/services', require('./routes/api/generatePDF'))
// app.use('/api/services', require('./routes/api/generatePDF'))
app.use('/invoice', require('./routes/api/generateInvoice'))
app.use('/receipt', require('./routes/api/generateReceipt'))
// app.get('/invoice', (req, res) => {
//   res.render('invoice/index', { foo: 'FOO' })
// })

/**
 * Socket.io server
 */

io.on('connection', (socket) => {
  console.log(`Server connection: ${socket.id}`)
  socket.on('test message', (message) => {
    console.log('message: ', message)
    setTimeout(() => {
      socket.emit('test message', message)
    }, 5000)
  })
})

// Test the creation of default Data
seed.startup()

// Define Ports
const port = process.env.PORT || 4000

const server = httpServer.listen(port, () => console.log(`Listening to localhost port: ${port}`))

module.exports = server

// const express = require('express')
// const app = express()
// const http = require('http')
// const server = http.createServer(app)
// const { Server } = require('socket.io')
// const io = new Server(server)
// app.get('/', (req, res) => {
//   res.sendFile(__dirname + '/index.html')
// })
// io.on('connection', (socket) => {
//   console.log('a user connected')
// })
// server.listen(3000, () => {
//   console.log('listening on *:3000')
// })
