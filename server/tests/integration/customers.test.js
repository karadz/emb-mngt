const request = require('supertest')
const { Quotations } = require('../../models/Quotations')
const Pricelists = require('../../models/Pricelists')
const Customers = require('../../models/Customers')
const { Products } = require('../../models/Products')
const Users = require('../../models/Users')

let server

xdescribe('/api/customers', () => {
  beforeEach(() => {
    server = require('../../server')
  })
  afterEach(async () => {
    server.close()
    await Quotations.deleteMany({})
    await Pricelists.deleteMany({})
    await Customers.deleteMany({})
    await Users.deleteMany({})
    await Products.deleteMany({})
  })
  describe('GET /', () => {
    test('should all customers not deleted', async () => {
      let resUser = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })

      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      token = resToken.body.token

      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })

      resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Noah Johnson',
        email: 'noahjohnson@gmail.com',
        phone: '0748 159 753',
        isCompany: 'individual',
      })

      resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Andular Trading',
        email: 'andular@gmail.com',
        phone: '0736 324 126',
        isCompany: 'company',
      })

      const res = await request(server).get('/api/customers').set({ 'x-auth-token': token })

      // console.log(res.body)

      expect(res.status).toBe(200)
      expect(res.body.length).toBe(3)
    })
    test('should all customer by id', async () => {
      let resUser = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })

      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      token = resToken.body.token

      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })

      resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Noah Johnson',
        email: 'noahjohnson@gmail.com',
        phone: '0748 159 753',
        isCompany: 'individual',
      })

      resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Andular Trading',
        email: 'andular@gmail.com',
        phone: '0736 324 126',
        isCompany: 'company',
      })

      const res = await request(server).get(`/api/customers/${resCustomer.body._id}`).set({ 'x-auth-token': token })

      // console.log(res.body)

      expect(res.status).toBe(200)
      expect(res.body.name).toBe('Andular Trading')
    })
  })
  describe('POST /', () => {
    test('should add a customer', async () => {
      let resUser = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })

      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      token = resToken.body.token

      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })

      const res = await request(server).get(`/api/customers/${resCustomer.body._id}`).set({ 'x-auth-token': token })

      // console.log(res.body)
      expect(res.status).toBe(200)
      expect(res.body.name).toBe('Jane Smith')
    })
    test('should give an error if customer name is added twice', async () => {
      let resUser = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })

      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      token = resToken.body.token

      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })
      resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        phone: '0734 123 281',
        isCompany: 'individual',
      })

      const res = await request(server).get(`/api/customers/${resCustomer.body._id}`).set({ 'x-auth-token': token })

      // console.log(res.body)
      expect(res.status).toBe(500)
      expect(res.body.errors[0].msg).toBe('Sever Error: Cast to ObjectId failed for value "undefined" at path "_id" for model "Customers"')
    })
  })
  describe('PUT /', () => {
    test('should edit a customer', async () => {
      let resUser = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })

      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      token = resToken.body.token

      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })

      let resEdit = await request(server).put(`/api/customers/${resCustomer.body._id}`).set({ 'x-auth-token': token }).send({
        name: 'Jane Hannibal',
        email: 'janehannibal@gmail.com',
        phone: '0734 111 222',
        isCompany: 'individual',
      })

      const res = await request(server).get(`/api/customers/${resCustomer.body._id}`).set({ 'x-auth-token': token })

      // console.log(res.body)
      expect(res.status).toBe(200)
      expect(res.body.name).toBe('Jane Hannibal')
    })
  })
  describe('DELETE /', () => {
    test('should delete a customer by id', async () => {
      let resUser = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })

      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      token = resToken.body.token

      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })

      resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Noah Johnson',
        email: 'noahjohnson@gmail.com',
        phone: '0748 159 753',
        isCompany: 'individual',
      })

      resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Andular Trading',
        email: 'andular@gmail.com',
        phone: '0736 324 126',
        isCompany: 'company',
      })
      // Delete customer
      const resDelete = await request(server).delete(`/api/customers/${resCustomer.body._id}`).set({ 'x-auth-token': token })

      const res = await request(server).get('/api/customers').set({ 'x-auth-token': token })

      // console.log(res.body)

      expect(res.status).toBe(200)
      expect(res.body.length).toBe(2)
    })
  })
})
