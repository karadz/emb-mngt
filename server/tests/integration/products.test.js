const request = require('supertest')
const { Quotations } = require('../../models/Quotations')
const Pricelists = require('../../models/Pricelists')
const Customers = require('../../models/Customers')
const { Products } = require('../../models/Products')
const Users = require('../../models/Users')

let server

xdescribe('/api/products', () => {
  beforeEach(() => {
    server = require('../../server')
  })
  afterEach(async () => {
    server.close()
    await Quotations.deleteMany({})
    await Pricelists.deleteMany({})
    await Customers.deleteMany({})
    await Users.deleteMany({})
    await Products.deleteMany({})
  })
  describe('GET /', () => {
    test('should get all products', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add products
      let resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })
      resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo2',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1245',
      })
      resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo3',
        productID: '000-000-0003',
        category: 'emb_logo',
        stitches: '1278',
      })

      // get products
      const res = await request(server).get('/api/products').set({ 'x-auth-token': token })

      // console.log(res.body)
      expect(res.status).toBe(200)
      expect(res.body.length).toBe(3)
    })
    test('should get a product by id', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add products
      let resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })
      resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo2',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1245',
      })
      resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo3',
        productID: '000-000-0003',
        category: 'emb_logo',
        stitches: '1278',
      })

      // get products
      const res = await request(server).get(`/api/products/${resAddProduct.body._id}`).set({ 'x-auth-token': token })

      // console.log(res.body)
      expect(res.status).toBe(200)
      expect(res.body.name).toBe('logo3')
    })
    test('should get all products not deleted', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add products
      let resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })
      resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo2',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1245',
      })
      resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo3',
        productID: '000-000-0003',
        category: 'emb_logo',
        stitches: '1278',
      })

      // console.log(resAddProduct.body._id)

      // Delete one product
      const resDeleted = await request(server).delete(`/api/products/${resAddProduct.body._id}`).set({ 'x-auth-token': token })

      // get products
      const res = await request(server).get('/api/products').set({ 'x-auth-token': token })

      // console.log(res.body)
      expect(res.status).toBe(200)
      expect(res.body.length).toBe(2)
    })
  })
  describe('POST /', () => {
    test('should add a product', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add products
      let res = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })
      expect(res.status).toBe(200)
      expect(res.body.name).toContain('logo1')
    })
    test('should give an error if category is emb_logo and stitches are not added when adding a product', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add products
      let res = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
      })
      // console.log(res.body)
      expect(res.status).toBe(400)
      expect(res.body.errors[0].msg).toContain('Stitches name must not be empty')
    })
    test('should give an error if category is NOT emb_logo and (unit_price & quantity) is not set when adding a product', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add products
      let res = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'other',
      })
      // console.log(res.body)
      expect(res.status).toBe(400)
      expect(res.body.errors.length).toBe(2)
      expect(res.body.errors[0].msg).toContain('Sales price name must not be empty')
      expect(res.body.errors[1].msg).toContain('Quantity price name must not be empty')
    })
    test('should give an error if you add 2 products with the same name', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add products
      let res = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })
      res = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1234',
      })
      // console.log(res.body)
      expect(res.status).toBe(500)
      expect(res.body.errors[0].msg).toContain(
        'Sever Error: E11000 duplicate key error collection: tests.products index: name_1 dup key: { name: "logo1" }'
      )
    })
  })
  describe('DELETE /', () => {
    test('should delete a product by id', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add products
      let resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })
      resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo2',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1245',
      })
      resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo3',
        productID: '000-000-0003',
        category: 'emb_logo',
        stitches: '1278',
      })

      // get products
      const res = await request(server).delete(`/api/products/${resAddProduct.body._id}`).set({ 'x-auth-token': token })

      // console.log(res.body)
      expect(res.status).toBe(200)
      expect(res.body.isDeleted).toBe(true)
    })
  })
  describe('PUT /', () => {
    test('should update a product when edited', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add products
      let resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })
      resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo2',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1245',
      })
      resAddProduct = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo3',
        productID: '000-000-0003',
        category: 'emb_logo',
        stitches: '1278',
      })

      // console.log(resAddProduct.body._id)

      // get products
      const res = await request(server).put(`/api/products/${resAddProduct.body._id}`).set({ 'x-auth-token': token }).send({
        name: 'logo4',
        productID: '000-000-0004',
        category: 'emb_logo',
        stitches: '1278',
      })

      // console.log(res.body)
      expect(res.status).toBe(200)
      expect(res.body.name).toBe('logo4')
    })
  })
})
