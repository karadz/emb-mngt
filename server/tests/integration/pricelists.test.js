const request = require('supertest')
const { Quotations } = require('../../models/Quotations')
const Pricelists = require('../../models/Pricelists')
const Customers = require('../../models/Customers')
const { Products } = require('../../models/Products')
const Users = require('../../models/Users')
let server

xdescribe('/api/settings/pricelists', () => {
  beforeEach(() => {
    server = require('../../server')
  })
  afterEach(async () => {
    server.close()
    await Quotations.deleteMany({})
    await Pricelists.deleteMany({})
    await Customers.deleteMany({})
    await Users.deleteMany({})
    await Products.deleteMany({})
  })
  describe('POST /', () => {
    test('should add a pricelist', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add pricelists

      let res = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist1',
          isDefault: true,
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })
      expect(res.status).toBe(200)
      expect(res.body.name).toContain('pricelist1')
    })
    test('should produce an error if a pricelist is not included', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add pricelists

      let res = await request(server).post('/api/settings/pricelists').set({ 'x-auth-token': token }).send({
        name: 'pricelist1',
        isDefault: true,
      })
      // console.log(res.body)
      expect(res.status).toBe(400)
      expect(res.body.errors[0].msg).toBe('Pricelist can not be empty')
    })
    test('should produce an error if a pricelist array is empty', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add pricelists

      let res = await request(server).post('/api/settings/pricelists').set({ 'x-auth-token': token }).send({
        name: 'pricelist1',
        isDefault: true,
        pricelist: [],
      })
      // console.log(res.body)
      expect(res.status).toBe(400)
      expect(res.body.errors[0].msg).toBe('Pricelist can not be empty')
    })
    test('should produce an error if a pricelist is not a array', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add pricelists

      let res = await request(server).post('/api/settings/pricelists').set({ 'x-auth-token': token }).send({
        name: 'pricelist1',
        isDefault: true,
        pricelist: 'not a array',
      })
      // console.log(res.body)
      expect(res.status).toBe(400)
      expect(res.body.errors[0].msg).toBe('Pricelist must be an array')
    })
  })
  describe('GET /', () => {
    test('should get all pricelists', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add pricelists

      let resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist1',
          isDefault: true,
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })
      resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist2',
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })
      resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist3',
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })

      // get pricelists
      const res = await request(server).get('/api/settings/pricelists').set({ 'x-auth-token': token })

      // console.log(res.body)

      expect(res.status).toBe(200)
      expect(res.body.length).toBe(3)
    })
    test('should get pricelist by id', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add pricelists

      let resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist1',
          isDefault: true,
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })
      resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist2',
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })
      resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist3',
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })

      // get pricelists
      const res = await request(server).get(`/api/settings/pricelists/${resPricelist.body._id}`).set({ 'x-auth-token': token })

      // console.log(res.body)

      expect(res.status).toBe(200)
      expect(res.body.name).toBe('pricelist3')
    })
  })
  describe('DELETE /', () => {
    test('should delete a pricelist by id', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add pricelists

      let resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist1',
          isDefault: true,
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })
      resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist2',
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })
      resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist3',
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })

      // delete a pricelist
      const resDelete = await request(server).delete(`/api/settings/pricelists/${resPricelist.body._id}`).set({ 'x-auth-token': token })

      // get pricelists
      const res = await request(server).get('/api/settings/pricelists').set({ 'x-auth-token': token })

      expect(res.status).toBe(200)
      expect(res.body.length).toBe(2)
    })
  })
  describe('PUT /', () => {
    test('should update a pricelist when edited', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      let token = resToken.body.token

      // add pricelists

      let resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist1',
          isDefault: true,
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })
      resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist2',
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })
      resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist3',
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })

      // delete a pricelist
      const resDelete = await request(server)
        .put(`/api/settings/pricelists/${resPricelist.body._id}`)
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist4',
          pricelist: [
            {
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })

      // get pricelists
      const res = await request(server).get(`/api/settings/pricelists/${resPricelist.body._id}`).set({ 'x-auth-token': token })

      expect(res.status).toBe(200)
      expect(res.body.name).toBe('pricelist4')
    })
  })
})
