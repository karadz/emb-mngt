const request = require('supertest')
const { Quotations } = require('../../models/Quotations')
const Pricelists = require('../../models/Pricelists')
const Customers = require('../../models/Customers')
const { Products } = require('../../models/Products')
const Users = require('../../models/Users')
const bcrypt = require('bcryptjs')
let server

describe('/api/settings/pricelists', () => {
  beforeEach(() => {
    server = require('../../server')
  })
  afterEach(async () => {
    server.close()
    await Quotations.deleteMany({})
    await Pricelists.deleteMany({})
    await Customers.deleteMany({})
    await Users.deleteMany({})
    await Products.deleteMany({})
  })
  describe('POST /', () => {
    test('should add a quotation', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      let token = resToken.body.token

      // add products
      let resProduct1 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })

      let resProduct2 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo2',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1234',
      })

      let resProduct3 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo3',
        productID: '000-000-0003',
        category: 'emb_logo',
        stitches: '1234',
      })

      // add customer
      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })
      // add pricelist
      let resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist1',
          isDefault: true,
          pricelist: [
            {
              id: '1',
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              id: '2',
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              id: '3',
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              id: '4',
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              id: '5',
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              id: '6',
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              id: '7',
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              id: '8',
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })
      // console.log('resPricelist', resPricelist.body)

      // add quotation
      let resQuotation = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000001',
          comments: 'First attempt at creation of a quotation',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })

      // console.log('resQuotation', resQuotation.body)
      expect(resQuotation.status).toBe(200)
      expect(resQuotation.body.orderID).toContain('SO 0000001')
    })
  })
  xdescribe('GET /', () => {
    test('should get all quotations', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      let token = resToken.body.token

      // add products
      let resProduct1 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })

      let resProduct2 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo2',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1234',
      })

      let resProduct3 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo3',
        productID: '000-000-0003',
        category: 'emb_logo',
        stitches: '1234',
      })

      // add customer
      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })
      // add pricelist
      let resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist1',
          isDefault: true,
          pricelist: [
            {
              id: '1',
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              id: '2',
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              id: '3',
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              id: '4',
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              id: '5',
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              id: '6',
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              id: '7',
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              id: '8',
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })

      // add quotation
      let resQuotation1 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000001',
          comments: 'First attempt at creation of a quotation',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })
      let resQuotation2 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000003',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })

      let resQuotation3 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000002',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })
      // console.log('resQuotation1', resQuotation1.body)
      // console.log('resQuotation2', resQuotation2.body)
      // console.log('resQuotation3', resQuotation3.body)

      let getQuotation = await request(server).get('/api/sales/quotations').set({ 'x-auth-token': token })

      // console.log('getQuotation', getQuotation.body)
      expect(getQuotation.status).toBe(200)
      expect(getQuotation.body.length).toBe(3)
    })
    test('should get a quotations by id', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      let token = resToken.body.token

      // add products
      let resProduct1 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })

      let resProduct2 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo2',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1234',
      })

      let resProduct3 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo3',
        productID: '000-000-0003',
        category: 'emb_logo',
        stitches: '1234',
      })

      // add customer
      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })
      // add pricelist
      let resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist1',
          isDefault: true,
          pricelist: [
            {
              id: '1',
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              id: '2',
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              id: '3',
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              id: '4',
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              id: '5',
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              id: '6',
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              id: '7',
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              id: '8',
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })

      // add quotation
      let resQuotation1 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000001',
          comments: 'First attempt at creation of a quotation',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })
      let resQuotation2 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000002',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })

      let resQuotation3 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000003',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })

      let getQuotation = await request(server).get(`/api/sales/quotations/${resQuotation3.body._id}`).set({ 'x-auth-token': token })

      // console.log('getQuotation', getQuotation.body)
      expect(getQuotation.status).toBe(200)
      expect(getQuotation.body.orderID).toBe('SO 0000003')
    })
  })
  xdescribe('DELETE /', () => {
    test('should delete a quotation by id', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      let token = resToken.body.token

      // add products
      let resProduct1 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })

      let resProduct2 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo2',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1234',
      })

      let resProduct3 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo3',
        productID: '000-000-0003',
        category: 'emb_logo',
        stitches: '1234',
      })

      // add customer
      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })
      // add pricelist
      let resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist1',
          isDefault: true,
          pricelist: [
            {
              id: '1',
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              id: '2',
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              id: '3',
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              id: '4',
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              id: '5',
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              id: '6',
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              id: '7',
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              id: '8',
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })

      // add quotation
      let resQuotation1 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000001',
          comments: 'First attempt at creation of a quotation',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })
      let resQuotation2 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000002',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })

      let resQuotation3 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000003',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })

      // delete quotation
      let deleteQuotation = await request(server).delete(`/api/sales/quotations/${resQuotation3.body._id}`).set({ 'x-auth-token': token })
      // get all quotations
      let getQuotation = await request(server).get('/api/sales/quotations').set({ 'x-auth-token': token })

      // console.log('getQuotation', getQuotation.body)
      expect(getQuotation.status).toBe(200)
      expect(getQuotation.body.length).toBe(2)
    })
  })
  xdescribe('PUT /', () => {
    test('should edit a quotation by id', async () => {
      // add user
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      let token = resToken.body.token

      // add products
      let resProduct1 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo1',
        productID: '000-000-0001',
        category: 'emb_logo',
        stitches: '1234',
      })

      let resProduct2 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo2',
        productID: '000-000-0002',
        category: 'emb_logo',
        stitches: '1234',
      })

      let resProduct3 = await request(server).post('/api/products').set({ 'x-auth-token': token }).send({
        name: 'logo3',
        productID: '000-000-0003',
        category: 'emb_logo',
        stitches: '1234',
      })

      // add customer
      let resCustomer = await request(server).post('/api/customers').set({ 'x-auth-token': token }).send({
        name: 'Jane Smith',
        email: 'janesmith@gmail.com',
        phone: '0734 123 281',
        isCompany: 'individual',
      })
      // add pricelist
      let resPricelist = await request(server)
        .post('/api/settings/pricelists')
        .set({ 'x-auth-token': token })
        .send({
          name: 'pricelist1',
          isDefault: true,
          pricelist: [
            {
              id: '1',
              embroidery_type: 'flat',
              max_qty: 3,
              price_per_thus_stitches: 0.2,
              min_price: 1,
            },
            {
              id: '2',
              embroidery_type: 'flat',
              max_qty: 30,
              price_per_thus_stitches: 0.16,
              min_price: 0.75,
            },
            {
              id: '3',
              embroidery_type: 'flat',
              max_qty: 50,
              price_per_thus_stitches: 0.15,
              min_price: 0.675,
            },
            {
              id: '4',
              embroidery_type: 'flat',
              max_qty: 100000,
              price_per_thus_stitches: 0.14,
              min_price: 0.6,
            },
            {
              id: '5',
              embroidery_type: 'cap',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.5,
            },
            {
              id: '6',
              embroidery_type: 'cap',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
            {
              id: '7',
              embroidery_type: 'applique',
              max_qty: 50,
              price_per_thus_stitches: 0.2,
              min_price: 1.25,
            },
            {
              id: '8',
              embroidery_type: 'applique',
              max_qty: 100000,
              price_per_thus_stitches: 0.175,
              min_price: 1,
            },
          ],
        })

      // add quotation
      let resQuotation1 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000001',
          comments: 'First attempt at creation of a quotation',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })
      let resQuotation2 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000002',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })

      let resQuotation3 = await request(server)
        .post('/api/sales/quotations')
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000003',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })

      // edit a quotation
      let resEdit = await request(server)
        .put(`/api/sales/quotations/${resQuotation3.body._id}`)
        .set({ 'x-auth-token': token })
        .send({
          user_id: resCreate.body._id,
          customer: resCustomer.body._id,
          pricelist_id: resPricelist.body._id,
          orderID: 'SO 0000004',
          total: 123456,
          order_line: [
            { ...resProduct1.body, product_id: resProduct1.body._id, quantity: 5, emb_type: 'flat', garment_positions: 'front_left', unit_price: 10 },
            { ...resProduct2.body, product_id: resProduct2.body._id, quantity: 8, emb_type: 'flat', garment_positions: 'front_left', unit_price: 11 },
            {
              ...resProduct3.body,
              product_id: resProduct3.body._id,
              quantity: 10,
              emb_type: 'flat',
              garment_positions: 'front_left',
              unit_price: 12,
            },
          ],
        })

      // console.log('resEdit:', resEdit.body)

      let getQuotation = await request(server).get(`/api/sales/quotations/${resQuotation3.body._id}`).set({ 'x-auth-token': token })

      // console.log('getQuotation', getQuotation.body)
      expect(getQuotation.status).toBe(200)
      expect(getQuotation.body.orderID).toBe('SO 0000004')
    })
  })
})
