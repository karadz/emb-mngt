const request = require('supertest')
const Users = require('../../models/Users')
const bcrypt = require('bcryptjs')
let server

xdescribe('/api/users', () => {
  beforeEach(() => {
    server = require('../../server')
  })
  afterEach(async () => {
    server.close()
    await Users.deleteMany({})
  })
  /**
   * Does not need Auth ????
   * Maybe you can change the Auth middleware to to allow first admin
   * TODO: To make a middleware for access control
   */
  describe('GET /', () => {
    let token
    test('should return all user if user is admin', async () => {
      let resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })

      resCreate = await request(server).post('/api/users').send({
        name: 'Jane Doe',
        email: 'janedoe@gmail.com',
        role: 'sales',
        mobile: '0789123456',
        password: 'jane123',
        passwordConfirmation: 'jane123',
      })

      resCreate = await request(server).post('/api/users').send({
        name: 'john juba',
        email: 'johnjuba@gmail.com',
        role: 'sales',
        mobile: '0789123456',
        password: 'juba123',
        passwordConfirmation: 'juba123',
      })

      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      token = resToken.body.token

      const res = await request(server).get('/api/users').set({ 'x-auth-token': token })

      expect(res.status).toBe(200)
      expect(res.body.length).toBe(3)
    })
    test('should return user profile if not admin', async () => {
      let resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })

      resCreate = await request(server).post('/api/users').send({
        name: 'Jane Doe',
        email: 'janedoe@gmail.com',
        role: 'sales',
        mobile: '0789123456',
        password: 'jane123',
        passwordConfirmation: 'jane123',
      })

      resCreate = await request(server).post('/api/users').send({
        name: 'john juba',
        email: 'johnjuba@gmail.com',
        role: 'sales',
        mobile: '0789123456',
        password: 'juba123',
        passwordConfirmation: 'juba123',
      })

      const resToken = await request(server).post('/api/auth').send({ email: 'janedoe@gmail.com', password: 'jane123' })

      // console.log(resToken.body)
      token = resToken.body.token

      const res = await request(server).get('/api/users').set({ 'x-auth-token': token })

      // console.log(res.body)
      expect(res.status).toBe(200)
      // expect(res.body.length).toBe(3)
      expect(res.body.email).toContain('janedoe@gmail.com')
    })
    test('should return an error message if token does not exist', async () => {
      const res = await request(server).get('/api/users').set({ 'x-auth-token': '' })
      expect(res.status).toBe(401)
      expect(res.body.error[0].msg).toContain('No token, authorization denied')
    })
    test('should return an error message if token is wrong or expired', async () => {
      const res = await request(server)
        .get('/api/users')
        .set({ 'x-auth-token': token + 'f' })
      expect(res.status).toBe(401)
      expect(res.body.error[0].msg).toContain('Token not valid')
    })
  })
  describe('POST /', () => {
    let token
    test('should add users if you are an administrator', async () => {
      const resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })

      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      token = resToken.body.token

      const resUser = await request(server).post('/api/users').set({ 'x-auth-token': token }).send({
        name: 'john juba',
        email: 'johnjuba@gmail.com',
        role: 'sales',
        mobile: '0789123456',
        password: 'juba123',
        passwordConfirmation: 'juba123',
      })

      const res = await request(server).get(`/api/users`).set({ 'x-auth-token': token })

      // console.log(res.body)
      expect(res.status).toBe(200)
      expect(res.body.length).toBe(2)
      expect(res.body[1].email).toContain('johnjuba@gmail.com')
    })
  })
  describe('PUT /', () => {
    let token
    test('should edit a users', async () => {
      let resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      resCreate = await request(server).post('/api/users').send({
        name: 'Jane Doe',
        email: 'janedoe@gmail.com',
        role: 'sales',
        mobile: '0789123456',
        password: 'jane123',
        passwordConfirmation: 'jane123',
      })

      resCreate = await request(server).post('/api/users').send({
        name: 'john juba',
        email: 'johnjuba@gmail.com',
        role: 'sales',
        mobile: '0789123456',
        password: 'juba123',
        passwordConfirmation: 'juba123',
      })

      // console.log(resCreate.body._id)

      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      token = resToken.body.token

      const res = await request(server).put(`/api/users/${resCreate.body._id}`).set({ 'x-auth-token': token }).send({
        name: 'john Smith',
        email: 'johnsmith@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'smith123',
        passwordConfirmation: 'smith123',
      })

      // console.log(res.body)
      expect(res.status).toBe(200)
      expect(res.body.email).toContain('johnsmith@gmail.com')
    })
  })
  describe('DELETE /', () => {
    let token
    test('delete a user by id', async () => {
      let resCreate = await request(server).post('/api/users').send({
        name: 'John Doe',
        email: 'johndoe@gmail.com',
        role: 'admin',
        mobile: '0789123456',
        password: 'john123',
        passwordConfirmation: 'john123',
      })
      resCreate = await request(server).post('/api/users').send({
        name: 'Jane Doe',
        email: 'janedoe@gmail.com',
        role: 'sales',
        mobile: '0789123456',
        password: 'jane123',
        passwordConfirmation: 'jane123',
      })

      resCreate = await request(server).post('/api/users').send({
        name: 'john juba',
        email: 'johnjuba@gmail.com',
        role: 'sales',
        mobile: '0789123456',
        password: 'juba123',
        passwordConfirmation: 'juba123',
      })

      // console.log(resCreate.body._id)

      const resToken = await request(server).post('/api/auth').send({ email: 'johndoe@gmail.com', password: 'john123' })

      // console.log(resToken.body)
      token = resToken.body.token

      const res = await request(server).delete(`/api/users/${resCreate.body._id}`).set({ 'x-auth-token': token })

      // console.log(res.body)
      expect(res.status).toBe(200)
      expect(res.body.isDeleted).toBe(true)
    })
  })
})
