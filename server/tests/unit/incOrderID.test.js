const { incOrderID } = require('../../models/Quotations')

xdescribe('incOrderID', () => {
  let orderID
  test('should increase the SO ID by 1', () => {
    orderID = 'SO 0000000'
    orderID = incOrderID(orderID)
    expect(orderID).toEqual('SO 0000001')
  })
  test('should next increase SO ID by 1', () => {
    orderID = incOrderID(orderID)
    expect(orderID).toEqual('SO 0000002')
  })
  test('should next increase SO ID by 1', () => {
    orderID = incOrderID(orderID)
    expect(orderID).toEqual('SO 0000003')
  })
  test('should next increase SO ID by 1', () => {
    orderID = incOrderID(orderID)
    expect(orderID).toEqual('SO 0000004')
  })
  test('should next increase SO ID by 1', () => {
    orderID = incOrderID(orderID)
    expect(orderID).toEqual('SO 0000005')
  })
  test('should next increase SO ID by 1', () => {
    orderID = incOrderID(orderID)
    expect(orderID).toEqual('SO 0000006')
  })
})
