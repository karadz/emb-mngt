const fs = require('fs')
const path = require('path')
const bcrypt = require('bcryptjs')
const Users = require('../models/Users')
const { CustomEnum } = require('../models/CustomEnum')

const startup = () => {
  const userPath = path.resolve(__dirname, 'users.json')
  const enumPath = path.resolve(__dirname, 'enum.json')

  fs.readFile(enumPath, (err, data) => {
    if (err) return console.error(err)
    const enumConf = JSON.parse(data)
    console.log('CustomEnum', CustomEnum)
    enumConf.customEnum.forEach(async (cEnum) => {
      await CustomEnum.create(cEnum)
      fs.readFile(userPath, (err, data) => {
        if (err) return console.error(err)
        const usersConf = JSON.parse(data)

        usersConf.users.forEach(async (user) => {
          // Encrypt the password
          const salt = await bcrypt.genSalt(10)
          encryptPassword = await bcrypt.hash(user.password, salt)
          user.password = encryptPassword
          await Users.create(user)
        })
        deletePath(userPath)
      })
    })
    deletePath(enumPath)
  })
}

const deletePath = (path) => {
  fs.unlink(path, (err) => {
    if (err) console.error(err)
  })
}

exports.startup = startup
