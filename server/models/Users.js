const mongoose = require('mongoose')
const { findEnumValue, constants } = require('./CustomEnum')
const dayjs = require('dayjs')
dayjs().format()

const oneWeek = dayjs().add(7, 'day').toISOString().slice(0, 16)
const date = dayjs().toISOString().slice(0, 16)

var UsersSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  image: {
    type: String,
  },
  role: {
    type: String,
    required: true,
    validate: (value) => {
      return findEnumValue(constants.USER_ROLES, value)
    },
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  mobile: [
    {
      type: String,
      required: true,
    },
  ],
  password: {
    type: String,
    required: true,
  },
  isDeleted: {
    type: Boolean,
    required: true,
    default: false,
  },
  date: {
    type: Date,
    required: true,
    default: date,
  },
})

//Export the model
module.exports = mongoose.model('Users', UsersSchema)
