const mongoose = require('mongoose') // Erase if already required
const { findEnumValue, constants } = require('./CustomEnum')
const dayjs = require('dayjs')
dayjs().format()

const oneWeek = dayjs().add(7, 'day').toISOString().slice(0, 16)
const date = dayjs().toISOString().slice(0, 16)

var customersSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Users',
  },
  name: {
    type: String,
    required: true,
    unique: true,
    index: true,
  },
  notes: {
    type: String,
    default: '',
  },
  vatOrBpNo: {
    type: String,
    default: '',
  },
  isCompany: {
    type: String,
    required: true,
    default: constants.DEF_CUSTOMER_TYPE,
    validate: (value) => {
      return findEnumValue(constants.CUSTOMER_TYPES, value)
    },
  },
  email: {
    type: String,
    unique: true,
  },
  phone: [
    {
      type: String,
      required: true,
    },
  ],
  address: {
    type: String,
    default: '',
  },
  balance: {
    type: Number, // TODO: fix for currency
    required: true,
    default: 0,
  },
  rating: {
    type: Number,
    required: true,
    default: 0,
  },
  isDeleted: {
    type: Boolean,
    required: true,
    default: false,
  },
  date: {
    type: Date,
    required: true,
    default: date,
  },
})

//Export the model
module.exports = mongoose.model('Customers', customersSchema)
