const mongoose = require('mongoose') // Erase if already required
const Pricelists = require('./Pricelists')
const R = require('ramda')
const { findEnumValue, constants } = require('./CustomEnum')

const dayjs = require('dayjs')
dayjs().format()

const oneWeek = dayjs().add(7, 'day').toISOString().slice(0, 16)
const date = dayjs().toISOString().slice(0, 16)

var customersSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Customers',
  },
  name: {
    type: String,
    required: true,
  },
  vatOrBpNo: {
    type: String,
    default: '',
  },
  isCompany: {
    type: String,
    required: true,
    default: constants.DEF_CUSTOMER_TYPE,
    validate: (value) => {
      return findEnumValue(constants.CUSTOMER_TYPES, value)
    },
  },
  email: {
    type: String,
  },
  phone: [
    {
      type: String,
      required: true,
    },
  ],
  address: {
    type: String,
  },
  organization: {
    type: String,
  },
})

var productsSchema = new mongoose.Schema({
  _id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Products',
  },
  productID: {
    // of the form xxx-xxx-xxxx /^([0-9]{3}-){2}[0-9]{4}$/gm
    type: String,
    match: /^([0-9]{3}-){2}[0-9]{4}$/,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  unit_price: {
    type: Number,
  },
  total: {
    type: Number,
  },
  category: {
    type: String,
    default: constants.DEF_PRODUCT_CATEGORY,
    required: true,
    validate: (value) => {
      return findEnumValue(constants.PRODUCT_CATEGORIES, value)
    },
  },
  emb_type: {
    type: String,
    default: 'flat',
    validate: (value) => {
      return findEnumValue(constants.EMBROIDERY_TYPES, value)
    },
    required: function () {
      return this.category === constants.DEF_PRODUCT_CATEGORY
    },
  },
  garment_positions: {
    type: String,
    default: 'front_left',
    validate: (value) => {
      return findEnumValue(constants.GARMENT_POSITIONS, value)
    },
    required: function () {
      return this.category === constants.DEF_PRODUCT_CATEGORY
    },
  },
  stitches: {
    type: Number,
    required: function () {
      return this.category === constants.DEF_PRODUCT_CATEGORY
    },
  },
  manufacturingStatus: {
    type: String,
    default: 'Receiving',
    validate: (value) => {
      return findEnumValue(constants.MANUFACTURING_STATUS, value)
    },
  },
})

var quotationsSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Users',
  },
  customer: { type: customersSchema, required: true },

  pricelist_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Pricelists',
  },
  orderID: {
    // of the form SO0000001 /SO\s[0-9]{7}$/gm
    type: String,
    match: /SO\s[0-9]{7}$/,
    required: true,
    unique: true,
    index: true,
  },
  comments: {
    type: String,
  },
  order_date: {
    type: Date,
    required: true,
    default: date,
  },
  quote_expiry_date: {
    type: Date,
    required: true,
    default: oneWeek,
  },
  required_date: {
    type: Date,
    required: true,
    default: oneWeek,
  },
  sub_total: {
    type: Number,
    required: true,
  },
  tax: {
    type: Number,
    required: true,
  },
  tax_rate: {
    type: Number,
    required: true,
  },
  discount: {
    type: Number,
    required: true,
  },
  discount_rate: {
    type: Number,
    required: true,
  },
  balance: {
    type: Number,
    required: true,
  },
  isDeleted: {
    type: Boolean,
    required: true,
    default: false,
  },
  order_line: { type: [productsSchema], required: true },
  accountsStatus: {
    type: String,
    default: 'quotation',
    validate: (value) => {
      return findEnumValue(constants.ACCOUNTS_STATUS, value)
    },
  },
})

const incOrderID = (orderID) => {
  let numOrderID = orderID.slice(2) // remove characters SO from string
  let strOrderID = (parseInt(numOrderID) + 1).toString() // convert to int and add one then covert to string
  let zeroBuffer = '0000000'
  orderID = `SO ${zeroBuffer.slice(strOrderID.length)}${strOrderID}`
  return orderID
}

const getCurrentOrderID = async () => {
  try {
    const quotation = await Quotations.find({}).sort({ _id: -1 }).limit(1).select('orderID')
    // console.log('products', products)
    let orderID = ''
    if (quotation.length === 0) {
      // set the orderID to the initial Value so xxxxxxx
      orderID = 'SO 0000000'
    } else {
      orderID = quotation[0].orderID
    }
    return orderID
  } catch (err) {
    return { errors: [{ msg: `Sever Error: ${err.message}` }] }
  }
}

const calcOrderLinePrices = async (orderLineValues) => {
  const { pricelistData, order_line, tax_rate, discount_rate } = orderLineValues
  console.log('pricelistData: ', pricelistData)
  const { pricelist } = pricelistData
  let newOrderLine = []
  let newSubtotal = 0
  let newTax = 0
  let newDiscount = 0
  let newBalance = 0

  console.log('orderLineValues', orderLineValues)

  order_line.map((product) => {
    let newTotal = 0
    let newUnitPrice = 0
    let stitch_price = 0
    let min_price = 0

    let { category, stitches, quantity, emb_type, unit_price, total } = product

    const pricelistFiltered = R.filter(R.where({ embroidery_type: R.includes(emb_type) }))(pricelist)
    console.log('pricelistFiltered', pricelistFiltered)
    const pricelistSorted = R.sort(R.ascend(R.compose(R.prop('max_qty'))), pricelistFiltered)
    console.log('pricelistSorted', pricelistSorted)
    const pricelistToApply = R.filter(R.where({ max_qty: R.lte(quantity) }))(pricelistSorted)[0]
    console.log('last', emb_type, quantity, pricelistToApply)
    if (category === constants.DEF_PRODUCT_CATEGORY) {
      min_price = parseFloat(pricelistToApply.min_price)
      stitch_price = (parseFloat(stitches) / 1000) * parseFloat(pricelistToApply.price_per_thus_stitches)
      newUnitPrice = min_price > stitch_price ? min_price : stitch_price
      newTotal = newUnitPrice * parseFloat(quantity)

      newSubtotal = newSubtotal + newTotal
      newOrderLine.push({ ...product, unit_price: newUnitPrice, total: newTotal })
    } else {
      newTotal = unit_price * parseFloat(quantity)
      newSubtotal = newSubtotal + newTotal
      newTotal = unit_price * parseFloat(quantity)
      newSubtotal = newSubtotal + newTotal
      newOrderLine.push({ ...product, unit_price, total: newTotal })
    }
  })
  newTax = (parseInt(tax_rate) / 100) * newSubtotal
  console.log(`newTax: ${newTax}, tax_rate: ${tax_rate}, newSubtotal: ${newSubtotal}`)
  newDiscount = (parseInt(discount_rate) / 100) * newSubtotal
  newBalance = newSubtotal - newTax - newDiscount
  return { order_line: newOrderLine, balance: newBalance, tax: newTax, discount: newDiscount, sub_total: newSubtotal }
}

const Quotations = mongoose.model('Quotations', quotationsSchema)

//Export the model
module.exports.Quotations = Quotations
module.exports.getCurrentOrderID = getCurrentOrderID
module.exports.incOrderID = incOrderID
module.exports.calcOrderLinePrices = calcOrderLinePrices
