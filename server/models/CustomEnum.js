const mongoose = require('mongoose')
const Schema = mongoose.Schema
const R = require('ramda')

// 
const customEnumSchema = new Schema({
  type: {
    type: String,
    required: true,
  },
  isDeleted: {
    type: Boolean,
    required: true,
    default: false,
  },
  isDefault: {
    type: Boolean,
    required: true,
    default: false,
  },
  value: {
    type: String,
    required: true,
    unique: true,
  },
})

const CustomEnum = mongoose.model('CustomEnum', customEnumSchema)

const findEnumValue = async (type, value) => {
  return !!(await CustomEnum.findOne({ type, isDeleted: false, value }))
}

const getEnumsArray = async (type) => {
  return R.pluck('value')(await CustomEnum.find({ type, isDeleted: false }))
}

module.exports.CustomEnum = CustomEnum
module.exports.findEnumValue = findEnumValue
module.exports.getEnumsArray = getEnumsArray
// type
module.exports.constants = {
  USER_ROLES: 'userRoles',
  PRODUCT_CATEGORIES: 'productCategories',
  EMBROIDERY_TYPES: 'embTypes',
  GARMENT_POSITIONS: 'garmentPositions',
  CUSTOMER_TYPES: 'customerTypes',
  ACCOUNTS_STATUS: 'accountsStatus',
  MANUFACTURING_STATUS: 'manufacturingStatus',
  // Default values
  DEF_USER_ROLE: 'user',
  DEF_PRODUCT_CATEGORY: 'emb_logo',
  DEF_EMBROIDERY_TYPE: 'Flat',
  DEF_GARMENT_POSITION: 'Front Left',
  DEF_CUSTOMER_TYPE: 'Individual',
  DEF_ACCOUNTS_STATUS: 'quotation',
  DEF_MANUFACTURING_STATUS: 'awaitingEmbroidery',
}
