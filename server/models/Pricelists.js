const mongoose = require('mongoose')
const Schema = mongoose.Schema

const pricelistsSchema = new Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Users',
  },
  name: {
    type: String,
    required: true,
    unique: true,
  },
  isDeleted: {
    type: Boolean,
    default: false,
    required: true,
  },
  isDefault: {
    type: Boolean,
    default: false,
    required: true,
  },
  pricelist: [
    {
      embroidery_type: {
        type: String, //TODO: to refactor to user enum and user one source for pricelist and quotation
        required: true,
      },
      max_qty: {
        type: Number,
        required: true,
      },
      price_per_thus_stitches: {
        type: Number,
        required: true,
      },
      min_price: {
        type: Number,
        required: true,
      },
    },
  ],
})

module.exports = mongoose.model('Pricelists', pricelistsSchema)
