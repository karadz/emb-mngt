const mongoose = require('mongoose')
const { findEnumValue, constants } = require('./CustomEnum')
const dayjs = require('dayjs')
dayjs().format()

const date = dayjs().toISOString().slice(0, 16)

var productsSchema = new mongoose.Schema({
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Users',
  },
  name: {
    type: String,
    required: true,
    unique: true,
    index: true,
  },
  productID: {
    // of the form xxx-xxx-xxxx /^([0-9]{3}-){2}[0-9]{4}$/
    type: String,
    match: /^([0-9]{3}-){2}[0-9]{4}$/,
    required: true,
    unique: true,
    index: true,
  },
  title: {
    type: String,
  },
  rating: {
    type: Number,
    required: true,
    default: 0,
  },
  description: {
    type: String,
  },
  image: {
    type: String,
  },
  unit_price: {
    type: Number,
    required: function () {
      return this.category !== constants.DEF_PRODUCT_CATEGORY
    },
  },
  category: {
    type: String,
    default: constants.DEF_PRODUCT_CATEGORY,
    required: true,
    validate: (value) => {
      return findEnumValue(constants.PRODUCT_CATEGORIES, value)
    },
  },
  stitches: {
    type: Number,
    required: function () {
      return this.category === constants.DEF_PRODUCT_CATEGORY
    },
  },
  quantity: {
    type: Number,
    required: function () {
      return this.category !== constants.DEF_PRODUCT_CATEGORY
    },
  },
  isDeleted: {
    type: Boolean,
    required: true,
    default: false,
  },
  date: {
    type: Date,
    default: date,
  },
})

const incProductID = (productID) => {
  oldProductID = productID.replace(/-/g, '') // remove - from string
  strProductID = (parseInt(oldProductID) + 1).toString() // convert to int and add one then covert to string
  productID = `${strProductID.slice(0, 3)}-${strProductID.slice(3, 6)}-${strProductID.slice(6)}`
  return productID
}

const getCurrentProductID = async () => {
  try {
    const products = await Products.find({}).sort({ _id: -1 }).limit(1).select('productID')
    // console.log('products', products)
    let productID = ''
    if (products.length === 0) {
      // set the productID to the initial Value xxx-xxx-xxxx (xxyxxyxxxx)
      productID = '100-000-0000'
    } else {
      productID = products[0].productID
    }
    return productID
  } catch (err) {
    res.status(500).send({ errors: [{ msg: `Sever Error: ${err.message}` }] })
  }
}

const Products = mongoose.model('Products', productsSchema)

//Export the model
module.exports.Products = Products
module.exports.incProductID = incProductID
module.exports.getCurrentProductID = getCurrentProductID
