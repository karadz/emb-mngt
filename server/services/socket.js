let io

module.exports = {
  init: (httpServer) => {
    const options = { cors: { origin: 'https://localhost:3000' }, transports: ['websocket'] }
    // io = require('socket.io').listen(server)
    io = require('socket.io')(httpServer, options)
    return io
  },
  get: () => {
    if (!io) {
      throw new Error('socket is not initialized')
    }
    return io
  },
}

// import { Server } from 'socket.io'
// let io

// export function init(httpServer) {
//   const options = { cors: { origin: 'https://localhost:3000' } }
//   // io = require('socket.io').listen(server)
//   // io = require('socket.io')(httpServer, options)
//   io = new Server(httpServer, options)
//   return io
// }
// export function get() {
//   if (!io) {
//     throw new Error('socket is not initialized')
//   }
//   return io
// }

// export default io
