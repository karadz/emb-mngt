const mongoose = require('mongoose')
const config = require('config')
// import config from 'config'
// import mongoose from 'mongoose'

const connectDB = async () => {
  try {
    const db = config.get('DB_LINK')
    const options = {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      autoIndex: true,
      connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
      socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
      family: 4, // IP version 4
      maxPoolSize: 10, // Maintain up to 10 socket connections
    }
    await mongoose.connect(db, options)
    console.log('Database connected.....')
  } catch (err) {
    console.error(`Could not connect to MongoDB, ${err}`)
    process.exit(1)
  }
}

module.exports = connectDB
